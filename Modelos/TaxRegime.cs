﻿using AppINTROIT.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AppINTROIT.Modelos
{
    public class TaxRegime
    {
        public DataTable all(bool active = false)
        {
            var ClsDatos = new Conexion();
            var DT = new DataTable();
            string query = (active == true) ? "SELECT * FROM RegimenFiscal" : "SELECT * FROM RegimenFiscal";
            //if (ClsDatos.CargaTabla(query, ref DT)) { }
            //return DT;
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }
    }
}
