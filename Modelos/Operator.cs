﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AppINTROIT.Modelos
{
    public class Operator
    {
        public bool Login()
        {
            var ClsDatos = new Conexion();
            var DT = new DataTable();
            bool _result = false;
            ActiveOperator._Password = Helper.Encripta(ActiveOperator._Password);
            string query = "SELECT o.*, e.Es_Descripcion as Estado FROM Operadores o INNER JOIN Estado e ON o.Es_Cve_Estado = e.Es_Cve_Estado " +
                " WHERE Login ='" + ActiveOperator._Login + "' AND Password ='" + ActiveOperator._Password + "' AND o.Es_Cve_Estado ='AC'";

            if (ClsDatos.CargaTabla(query, ref DT))
            {
                if (DT.Rows.Count > 0)
                {
                    ActiveOperator._Id = Convert.ToInt32(DT.Rows[0][0]);
                    ActiveOperator._Nombre = DT.Rows[0][1].ToString();
                    ActiveOperator.Correo = DT.Rows[0][6].ToString();
                    ActiveOperator._Rol = DT.Rows[0][7].ToString();
                    ActiveOperator._Es_Cve_Estado = DT.Rows[0][9].ToString();
                    _result = true;
                }
            }
            else
            {
                ActiveOperator._Message = ClsDatos.MsjError;
            }

            return _result;
        }

        public bool OperadoresIU()
        {
            bool _result = false;
            try
            {
                var ClsDatos = new Conexion();
                string query = "";

                switch (ActiveOperator._Action)
                {
                    case 1:
                        ActiveOperator._Password = Helper.Encripta(ActiveOperator._Password);
                        query = "INSERT INTO Operadores (Nombre_completo, Login, Password, Correo, Rol) VALUES ('" + ActiveOperator._Nombre + "', " +
                         " '" + ActiveOperator._Login + "', '" + ActiveOperator._Password + "', '" + ActiveOperator.Correo + "', '" + ActiveOperator._Rol + "');";
                        ActiveOperator._Message = "Operador guardado correctamente";
                        break;
                    case 2:
                        ActiveOperator._Password = Helper.Encripta(ActiveOperator._Password);
                        query = "UPDATE Operadores SET Nombre_completo= '" + ActiveOperator._Nombre + "', login='" + ActiveOperator._Login + "', password='" + ActiveOperator._Password + "', " +
                            " correo='" + ActiveOperator.Correo + "', rol='" + ActiveOperator._Rol + "' WHERE Id=" + ActiveOperator._Id;
                        ActiveOperator._Message = "Operador guardado correctamente";
                        break;
                    case 3:
                        query = "UPDATE Operadores SET Es_Cve_Estado='BA' WHERE Id=" + ActiveOperator._Id;
                        ActiveOperator._Message = "Operador inactivado correctamente";
                        break;
                    case 4:
                        query = "UPDATE Operadores SET Es_Cve_Estado='AC' WHERE Id=" + ActiveOperator._Id;
                        ActiveOperator._Message = "Operador activado correctamente";
                        break;
                }

                ClsDatos.CargaComando(query);
                if (ClsDatos.Execute())
                {
                    if (ActiveOperator._Action == 1)
                    {
                        var DT = new DataTable();
                        if (ClsDatos.CargaTabla("SELECT @@IDENTITY", ref DT))
                        {
                            ActiveOperator._Id = Convert.ToInt32(DT.Rows[0][0]);
                        }
                    }

                    _result = true;

                }
                else
                {
                    ActiveOperator._Message = ClsDatos.MsjError;
                }
            }
            catch (Exception ex)
            {
                ActiveOperator._Message = ex.Message.ToString();
            }
            return _result;
        }

        /// <summary>
        /// Listar todos los usuarios
        /// </summary>
        /// <returns></returns>
        public DataTable All(bool image = false)
        {
            string query = "";
            if (image)
            {
                query = "SELECT " +
                "Id AS Clave, " +
                "Nombre_completo AS Nombre, " +
                "[Login] AS Usuario, " +
                "[Password], " +
                "Correo, " +
                "Rol AS Perfil, " +
                "Es_Cve_Estado AS Estado, " +
                "Icono " +
                "FROM Operadores ORDER BY Id DESC";
            }
            else
            {
                query = "SELECT " +
               "Id AS Clave, " +
               "Nombre_completo AS Nombre, " +
               "[Login] AS Usuario, " +
               "[Password], " +
               "Correo, " +
               "Rol AS Perfil, " +
               "Es_Cve_Estado AS Estado " +
               "FROM Operadores ORDER BY Id DESC";
            }

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }
    }
}
