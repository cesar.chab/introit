﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT.Modelos
{
    public class Concept
    {
        //metodo para cargar la coleccion de datos para el autocomplete
        public static AutoCompleteStringCollection Autocomplete()
        {
            DataTable dt = All(true);

            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            //recorrer y cargar los items para el autocompletado
            foreach (DataRow row in dt.Rows)
            {
                coleccion.Add(Convert.ToString(row["Descripcion"]));
            }

            return coleccion;
        }

        public bool Concepto_IU()
        {
            bool _result = false;
            try
            {
                var ClsDatos = new Conexion();
                string query = "";

                switch (ActiveConcept._Action)
                {
                    case 1:
                        query = "INSERT INTO Conceptos (descripcion, precio_gasolina, precio_diesel, Es_Cve_Estado, Oper_Alta, Oper_Ult_Modif) VALUES ('" + ActiveConcept._Descripcion + "', " +
                         " " + ActiveConcept._PrecioGasolina + ", " + ActiveConcept._PrecioDiesel + ", 'AC', '" + ActiveOperator._Login + "', '" + ActiveOperator._Login + "')";
                        ActiveConcept._Message = "Concepto guardado correctamente";
                        break;
                    case 2:
                        query = "UPDATE Conceptos SET descripcion = '" + ActiveConcept._Descripcion + "', precio_gasolina=" + ActiveConcept._PrecioGasolina + ", precio_diesel=" + ActiveConcept._PrecioDiesel + ", " +
                            " Fecha_Ult_Modif=GETDATE(), Oper_Ult_Modif='" + ActiveOperator._Login + "' WHERE Id=" + ActiveConcept._Id;
                        ActiveConcept._Message = "Concepto actualizado correctamente";
                        break;
                    case 3:
                        query = "UPDATE Conceptos SET Es_Cve_Estado='BA', Fecha_Baja=GETDATE(), Oper_Baja='" + ActiveOperator._Login + "' WHERE Id=" + ActiveConcept._Id;
                        ActiveConcept._Message = "Concepto dado de baja correctamente";
                        break;
                    case 4:
                        query = "UPDATE Conceptos SET Es_Cve_Estado='AC', Fecha_Baja=NULL, Oper_Baja='' WHERE Id=" + ActiveConcept._Id;
                        ActiveConcept._Message = "Concepto activado correctamente";
                        break;
                }

                ClsDatos.CargaComando(query);
                if (ClsDatos.Execute())
                {
                    if (ActiveConcept._Action == 1)
                    {
                        var DT = new DataTable();
                        if (ClsDatos.CargaTabla("SELECT @@IDENTITY", ref DT))
                        {
                            ActiveConcept._Id = Convert.ToInt32(DT.Rows[0][0]);
                        }
                    }

                    _result = true;

                }
                else
                {
                    ActiveConcept._Message = ClsDatos.MsjError;
                }
            }
            catch (Exception ex)
            {
                ActiveConcept._Message = ex.Message.ToString();
            }
            return _result;
        }

        /// <summary>
        /// Listar todos los usuarios
        /// </summary>
        /// <returns></returns>
        public static DataTable All(bool active = false)
        {
            string query = "";
            if (active)
            {
                query = "SELECT " +
                "id AS Clave, " +
                "descripcion AS Descripcion, " +
                "precio_gasolina as [Precio Gasolina], " + 
                "precio_diesel as [Precio Diesel], " +
                "Fecha_Ult_Modif, " +
                "Oper_Ult_Modif, " +
                "Es_Cve_Estado AS Estado " +
                "FROM Conceptos WHERE Es_Cve_Estado ='AC' ORDER BY id DESC";
            }
            else
            {
                query = "SELECT " +
                "id AS Clave, " +
                "descripcion AS Descripcion, " +
                "precio_gasolina as [Precio Gasolina], " +
                "precio_diesel as [Precio Diesel], " +
                "Fecha_Ult_Modif, " +
                "Oper_Ult_Modif, " +
                "Es_Cve_Estado AS Estado " +
                "FROM Conceptos ORDER BY id DESC";
            }

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }

        public DataTable findByConcept (string concept)
        {
           string query = "SELECT * FROM Conceptos WHERE Es_Cve_Estado ='AC' AND descripcion = '" + concept + "'";

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }

        public bool findExistsByConcept(string concet)
        {
            string query = "SELECT TOP 1 * FROM Conceptos WHERE Es_Cve_Estado ='AC' AND descripcion = '" + concet + "'";
            bool exists = false;
            var DT = new DataTable();
            var ClsDatos = new Conexion();
            if (ClsDatos.CargaTabla(query, ref DT))
            {
                if (DT.Rows.Count > 0)
                {
                    ActiveConcept._Id = int.Parse(DT.Rows[0][0].ToString());
                    exists = true;
                }
            }

            return exists;
        }



        public DataTable searchConcept (string concept)
        {
            string query = "SELECT TOP 100 id AS Clave, descripcion AS Descripcion, precio_gasolina as [Precio Gasolina], " +
                "precio_diesel as [Precio Diesel] FROM Conceptos WHERE Es_Cve_Estado ='AC' AND descripcion LIKE '%" + concept + "%'";

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }
    }
}
