﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT.Modelos
{
    public class Customer
    {
        //metodo para cargar la coleccion de datos para el autocomplete
        public static AutoCompleteStringCollection Autocomplete()
        {
            DataTable dt = All(true);

            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            //recorrer y cargar los items para el autocompletado
            foreach (DataRow row in dt.Rows)
            {
                coleccion.Add(Convert.ToString(row["Nombre"]));
            }

            return coleccion;
        }

        public bool Customer_IU()
        {
            bool _result = false;
            try
            {
                var ClsDatos = new Conexion();
                string query = "";

                switch (ActiveCustomer._Action)
                {
                    case 1:
                        var next = "SELECT dbo.CIntToChar(CASE WHEN Folio IS NULL THEN 0 ELSE Folio END + 1,10) AS FOLIO FROM Folios WHERE Tipo='Clientes'";
                        DataTable DTFolio = new DataTable();
                        if (ClsDatos.CargaTabla(next, ref DTFolio))
                        {
                            if (DTFolio.Rows.Count > 0)
                            {
                                ActiveCustomer._Cl_Cve_Cliente = DTFolio.Rows[0][0].ToString();
                            }

                        }
                        query = "INSERT INTO Clientes (Cl_Cve_Cliente, Cl_Nombre, Oper_Alta) VALUES ('" + ActiveCustomer._Cl_Cve_Cliente + "', " +
                         " '" + ActiveCustomer._Cl_Nombre + "', '" + ActiveOperator._Login + "')";
                        ActiveCustomer._Message = "Cliente guardado correctamente";
                        break;
                    case 2:
                        query = "UPDATE Clientes SET Cl_Nombre = '" + ActiveCustomer._Cl_Nombre + "' WHERE Cl_Cve_Cliente='" + ActiveCustomer._Cl_Cve_Cliente + "'";
                        ActiveCustomer._Message = "Cliente actualizado correctamente";
                        break;
                    case 3:
                        query = "UPDATE Clientes SET Es_Cve_Estado='BA' WHERE Cl_Cve_Cliente='" + ActiveCustomer._Cl_Cve_Cliente + "'";
                        ActiveCustomer._Message = "Cliente dado de baja correctamente";
                        break;
                    case 4:
                        query = "UPDATE Clientes SET Es_Cve_Estado='AC' WHERE Cl_Cve_Cliente='" + ActiveCustomer._Cl_Cve_Cliente + "'";
                        ActiveCustomer._Message = "Cliente activado correctamente";
                        break;
                }

                ClsDatos.CargaComando(query);
                if (ClsDatos.Execute())
                {
                    string upNext = "UPDATE Folios SET Folio = [dbo].[Get_ValueOf]('" + ActiveCustomer._Cl_Cve_Cliente + "') WHERE Tipo = 'Clientes'";
                    ClsDatos.CargaComando(upNext);
                    ClsDatos.Execute();
                    _result = true;
                }
                else
                {
                    ActiveConcept._Message = ClsDatos.MsjError;
                }
            }
            catch (Exception ex)
            {
                ActiveConcept._Message = ex.Message.ToString();
            }
            return _result;
        }

        /// <summary>
        /// Listar todos los usuarios
        /// </summary>
        /// <returns></returns>
        public static DataTable All(bool active = false)
        {
            string query = "";
            if (active)
            {
                query = "SELECT Cl_Cve_Cliente as Clave, Cl_Nombre as Nombre FROM Clientes WHERE Es_Cve_Estado ='AC' ORDER BY Cl_Nombre DESC";
            }
            else
            {
                query = "SELECT Cl_Cve_Cliente as Clave, Cl_Nombre as Nombre FROM Clientes Order By Cl_Nombre DESC";
            }

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }

        public bool findExistsByName(string name)
        {
            string query = "SELECT TOP 1 * FROM Clientes WHERE Es_Cve_Estado ='AC' AND Cl_Nombre = '" + name + "'";
            bool exists = false;
            var DT = new DataTable();
            var ClsDatos = new Conexion();
            if (ClsDatos.CargaTabla(query, ref DT))
            {
                if (DT.Rows.Count > 0)
                {
                    ActiveCustomer._Cl_Cve_Cliente = DT.Rows[0][0].ToString();
                    exists = true;
                }
            }

            return exists;
        }

        public DataTable searchCustomer(string customer)
        {
            string query = "SELECT TOP 100 Cl_Cve_Cliente as Clave, Cl_Nombre as Nombre FROM Clientes WHERE Es_Cve_Estado ='AC' AND Cl_Nombre LIKE '%" + customer + "%'";

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }
    }
}
