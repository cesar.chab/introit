﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AppINTROIT.Modelos
{
    public class Referral
    {
        /// <summary>
        /// Listar todos los usuarios
        /// </summary>
        /// <returns></returns>
        public static DataTable All ()
        {
            string query = "";

            string condicion = "";
            string condicionadicional = "";

            if (!String.IsNullOrEmpty( ActiveReferral._Cl_Nombre))
            {
                if (ActiveReferral._Cl_Nombre.Length > 0) condicion += " AND c.Cl_Nombre LIKE '%" + ActiveReferral._Cl_Nombre + "%'";
            }

            if (!String.IsNullOrEmpty(ActiveReferral._Fecha_Inicio.ToShortDateString()) && !String.IsNullOrEmpty(ActiveReferral._Fecha_Final.ToShortDateString()))
            {
                 condicion += " AND CONVERT(DATE, rme.Rm_Fecha) >= '" + ActiveReferral._Fecha_Inicio.ToString("yyyyMMdd") + "' AND CONVERT(DATE, rme.Rm_Fecha) <= '" + ActiveReferral._Fecha_Final.ToString("yyyyMMdd") + "'";
            }

            if (condicionadicional.Length > 0) condicion += " AND " + condicionadicional;
            if (condicion != "")
                condicion = String.Format(" WHERE {0}", condicion.Substring(5));

            query = "SELECT " +
                " rme.Rm_Folio as Folio, " +
                " c.Cl_Nombre as Cliente, " +
                " em.Em_Nombre as Empresa, " +
                " rme.Rm_Fecha as Fecha, " +
                " rme.Rm_Comentario as Condiciones, " +
                " rme.Rm_Importe as Total, " +
                " e.Es_Descripcion as Estado " +
                " FROM Remision_Encabezado rme " +
                " INNER JOIN Clientes c ON rme.Cl_Cve_Cliente = c.Cl_Cve_Cliente " +
                " INNER JOIN Estado e ON rme.Es_Cve_Estado = e.Es_Cve_Estado " +
                " INNER JOIN Empresa em ON rme.Em_Cve_Empresa = em.Em_Cve_Empresa " + condicion +  " ORDER BY rme.Rm_Folio DESC";

            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }

        public static DateTime FechaAlta()
        {
            string query = "SELECT Rm_Fecha FROM Remision_Encabezado WHERE Rm_Folio='" + ActiveReferral._Rm_Folio + "'";
            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            if (DT.Rows.Count > 0)
            {
                return Convert.ToDateTime(DT.Rows[0][0].ToString());
            } else
            {
                return DateTime.Now;
            }
        }
       

        public static DataTable Detail()
        {
            string query = "SELECT " +
                    " Rm_ID AS Clave, " +
                    " Rm_Concepto as Concepto, " +
                    " Rm_Cantidad as Cantidad, " +
                    " Rm_Precio as Precio, " +
                    " Rm_Importe as Importe " +
                    " FROM Remision_Detalle " +
                    " WHERE Rm_Folio='" + ActiveReferral._Rm_Folio + "'";
            var DT = new DataTable();
            var ClsDatos = new Conexion();
            ClsDatos.CargaTabla(query, ref DT);
            return DT;
        }

        public static bool destroy()
        {
            bool cancelled = false;
            var ClsDatos = new Conexion();

            string query = "UPDATE [dbo].[Remision_Encabezado] " +
                       " SET " +
                       "  [Oper_Ult_Modif] = '" + ActiveOperator._Login + "' " +
                       "   ,[Fecha_Ult_Modif] = GETDATE() " +
                       "   ,[Oper_Baja] = '" + ActiveOperator._Login + "' " +
                       "   ,[Fecha_Baja] = GETDATE() " +
                       "   ,[Es_Cve_Estado] = 'BA' " +
                     " WHERE Rm_Folio = '" + ActiveReferral._Rm_Folio + "'";
            string queryDetail = "UPDATE [dbo].[Remision_Detalle] " +
                           " SET " +
                           "   [Oper_Ult_Modif] = '" + ActiveOperator._Login + "' " +
                           "   ,[Fecha_Ult_Modif] = GETDATE() " +
                           "   ,[Oper_Baja] = '" + ActiveOperator._Login + "' " +
                           "   ,[Fecha_Baja] = GETDATE() " +
                           "   ,[Es_Cve_Estado] = 'BA' " +
                         " WHERE[Rm_Folio] = '" + ActiveReferral._Rm_Folio + "'";

            ClsDatos.CargaComando(query);
            if (ClsDatos.Execute())
            {
                cancelled = true;
                ClsDatos.CargaComando(queryDetail);
                ClsDatos.Execute();
                ActiveReferral._Message = "Remisión cancelado correctamente";
            } else
            {
                ActiveReferral._Message = ClsDatos.MsjError;
            }

            return cancelled;
        }
    }
}
