﻿
namespace AppINTROIT.Configurations
{
    partial class RegisterCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterCompany));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.txtNumInterior = new System.Windows.Forms.TextBox();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.txtNumExterior = new System.Windows.Forms.TextBox();
            this.txtCiudad = new System.Windows.Forms.TextBox();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCruzamientos = new System.Windows.Forms.TextBox();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.btnNext1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRuta = new System.Windows.Forms.Button();
            this.txtRutaBackup = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.PanelLogo = new System.Windows.Forms.Panel();
            this.cboRegimen = new System.Windows.Forms.ComboBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.PanelBorder = new System.Windows.Forms.Panel();
            this.txtNombreEmpresa = new System.Windows.Forms.TextBox();
            this.lblempresa = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblChangeImage = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtConfirmarPassword = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtNombreOperador = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.btnBackDirs = new System.Windows.Forms.Button();
            this.btnSaveFull = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Label3 = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.FolderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ofdUser = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.PanelLogo.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 62);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(754, 584);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.btnNext1);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.PanelLogo);
            this.tabPage1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(746, 558);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.panel12);
            this.panel3.Controls.Add(this.panel13);
            this.panel3.Controls.Add(this.panel17);
            this.panel3.Controls.Add(this.panel26);
            this.panel3.Controls.Add(this.panel27);
            this.panel3.Controls.Add(this.panel29);
            this.panel3.Controls.Add(this.panel30);
            this.panel3.Controls.Add(this.panel33);
            this.panel3.Controls.Add(this.panel34);
            this.panel3.Controls.Add(this.panel35);
            this.panel3.Controls.Add(this.panel36);
            this.panel3.Controls.Add(this.txtNumInterior);
            this.panel3.Controls.Add(this.txtEstado);
            this.panel3.Controls.Add(this.txtLocalidad);
            this.panel3.Controls.Add(this.txtNumExterior);
            this.panel3.Controls.Add(this.txtCiudad);
            this.panel3.Controls.Add(this.txtColonia);
            this.panel3.Controls.Add(this.txtCruzamientos);
            this.panel3.Controls.Add(this.txtPais);
            this.panel3.Controls.Add(this.txtMunicipio);
            this.panel3.Controls.Add(this.txtCodigoPostal);
            this.panel3.Controls.Add(this.txtCalle);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Location = new System.Drawing.Point(9, 174);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(734, 213);
            this.panel3.TabIndex = 596;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel12.Location = new System.Drawing.Point(574, 57);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(95, 2);
            this.panel12.TabIndex = 554;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel13.Location = new System.Drawing.Point(470, 149);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(199, 2);
            this.panel13.TabIndex = 554;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel17.Location = new System.Drawing.Point(470, 103);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(199, 2);
            this.panel17.TabIndex = 554;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel26.Location = new System.Drawing.Point(470, 57);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(95, 2);
            this.panel26.TabIndex = 554;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel27.Location = new System.Drawing.Point(258, 149);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(201, 2);
            this.panel27.TabIndex = 554;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel29.Location = new System.Drawing.Point(258, 103);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(201, 2);
            this.panel29.TabIndex = 554;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel30.Location = new System.Drawing.Point(258, 57);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(201, 2);
            this.panel30.TabIndex = 554;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel33.Location = new System.Drawing.Point(51, 196);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(201, 2);
            this.panel33.TabIndex = 554;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel34.Location = new System.Drawing.Point(48, 149);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(201, 2);
            this.panel34.TabIndex = 554;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel35.Location = new System.Drawing.Point(48, 103);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(201, 2);
            this.panel35.TabIndex = 554;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel36.Location = new System.Drawing.Point(48, 57);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(201, 2);
            this.panel36.TabIndex = 554;
            // 
            // txtNumInterior
            // 
            this.txtNumInterior.BackColor = System.Drawing.Color.White;
            this.txtNumInterior.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumInterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNumInterior.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNumInterior.ForeColor = System.Drawing.Color.Black;
            this.txtNumInterior.Location = new System.Drawing.Point(574, 34);
            this.txtNumInterior.Name = "txtNumInterior";
            this.txtNumInterior.Size = new System.Drawing.Size(95, 21);
            this.txtNumInterior.TabIndex = 4;
            // 
            // txtEstado
            // 
            this.txtEstado.BackColor = System.Drawing.Color.White;
            this.txtEstado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEstado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtEstado.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtEstado.ForeColor = System.Drawing.Color.Black;
            this.txtEstado.Location = new System.Drawing.Point(470, 126);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(199, 21);
            this.txtEstado.TabIndex = 10;
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.Color.White;
            this.txtLocalidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLocalidad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtLocalidad.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtLocalidad.ForeColor = System.Drawing.Color.Black;
            this.txtLocalidad.Location = new System.Drawing.Point(470, 80);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(199, 21);
            this.txtLocalidad.TabIndex = 7;
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.Color.White;
            this.txtNumExterior.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumExterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNumExterior.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNumExterior.ForeColor = System.Drawing.Color.Black;
            this.txtNumExterior.Location = new System.Drawing.Point(470, 34);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.Size = new System.Drawing.Size(95, 21);
            this.txtNumExterior.TabIndex = 3;
            // 
            // txtCiudad
            // 
            this.txtCiudad.BackColor = System.Drawing.Color.White;
            this.txtCiudad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCiudad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCiudad.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCiudad.ForeColor = System.Drawing.Color.Black;
            this.txtCiudad.Location = new System.Drawing.Point(258, 126);
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.Size = new System.Drawing.Size(201, 21);
            this.txtCiudad.TabIndex = 9;
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.Color.White;
            this.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtColonia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtColonia.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtColonia.ForeColor = System.Drawing.Color.Black;
            this.txtColonia.Location = new System.Drawing.Point(258, 80);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(201, 21);
            this.txtColonia.TabIndex = 6;
            // 
            // txtCruzamientos
            // 
            this.txtCruzamientos.BackColor = System.Drawing.Color.White;
            this.txtCruzamientos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCruzamientos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCruzamientos.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCruzamientos.ForeColor = System.Drawing.Color.Black;
            this.txtCruzamientos.Location = new System.Drawing.Point(258, 34);
            this.txtCruzamientos.Name = "txtCruzamientos";
            this.txtCruzamientos.Size = new System.Drawing.Size(201, 21);
            this.txtCruzamientos.TabIndex = 2;
            // 
            // txtPais
            // 
            this.txtPais.BackColor = System.Drawing.Color.White;
            this.txtPais.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPais.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtPais.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtPais.ForeColor = System.Drawing.Color.Black;
            this.txtPais.Location = new System.Drawing.Point(48, 172);
            this.txtPais.Name = "txtPais";
            this.txtPais.Size = new System.Drawing.Size(201, 21);
            this.txtPais.TabIndex = 11;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.Color.White;
            this.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMunicipio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtMunicipio.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtMunicipio.ForeColor = System.Drawing.Color.Black;
            this.txtMunicipio.Location = new System.Drawing.Point(48, 126);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(201, 21);
            this.txtMunicipio.TabIndex = 8;
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.BackColor = System.Drawing.Color.White;
            this.txtCodigoPostal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCodigoPostal.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCodigoPostal.ForeColor = System.Drawing.Color.Black;
            this.txtCodigoPostal.Location = new System.Drawing.Point(48, 80);
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.Size = new System.Drawing.Size(201, 21);
            this.txtCodigoPostal.TabIndex = 5;
            // 
            // txtCalle
            // 
            this.txtCalle.BackColor = System.Drawing.Color.White;
            this.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCalle.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCalle.ForeColor = System.Drawing.Color.Black;
            this.txtCalle.Location = new System.Drawing.Point(48, 34);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(201, 21);
            this.txtCalle.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(571, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 342;
            this.label1.Text = "Num. Interior";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(467, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 15);
            this.label8.TabIndex = 342;
            this.label8.Text = "Estado";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(467, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 15);
            this.label14.TabIndex = 342;
            this.label14.Text = "Localidad";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(467, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 15);
            this.label15.TabIndex = 342;
            this.label15.Text = "Num. Exterior";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(255, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 15);
            this.label16.TabIndex = 342;
            this.label16.Text = "Ciudad";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(255, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 15);
            this.label17.TabIndex = 342;
            this.label17.Text = "Colonia";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(255, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 15);
            this.label18.TabIndex = 342;
            this.label18.Text = "Cruzamientos";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label19.Location = new System.Drawing.Point(45, 154);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 15);
            this.label19.TabIndex = 342;
            this.label19.Text = "País";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(45, 108);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 15);
            this.label22.TabIndex = 342;
            this.label22.Text = "Municipio";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label32.Location = new System.Drawing.Point(45, 62);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 15);
            this.label32.TabIndex = 342;
            this.label32.Text = "Código Postal";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label33.Location = new System.Drawing.Point(45, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(33, 15);
            this.label33.TabIndex = 342;
            this.label33.Text = "Calle";
            // 
            // btnNext1
            // 
            this.btnNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext1.BackColor = System.Drawing.Color.SeaGreen;
            this.btnNext1.FlatAppearance.BorderSize = 0;
            this.btnNext1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext1.ForeColor = System.Drawing.Color.White;
            this.btnNext1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext1.Location = new System.Drawing.Point(610, 514);
            this.btnNext1.Name = "btnNext1";
            this.btnNext1.Size = new System.Drawing.Size(130, 38);
            this.btnNext1.TabIndex = 14;
            this.btnNext1.Text = "Siguiente";
            this.btnNext1.UseVisualStyleBackColor = false;
            this.btnNext1.Click += new System.EventHandler(this.btnNext1_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.btnRuta);
            this.panel2.Controls.Add(this.txtRutaBackup);
            this.panel2.Controls.Add(this.Label9);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.panel2.Location = new System.Drawing.Point(9, 396);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(734, 95);
            this.panel2.TabIndex = 594;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel4.Location = new System.Drawing.Point(87, 65);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(493, 2);
            this.panel4.TabIndex = 559;
            // 
            // btnRuta
            // 
            this.btnRuta.BackColor = System.Drawing.Color.Transparent;
            this.btnRuta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRuta.BackgroundImage")));
            this.btnRuta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRuta.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRuta.FlatAppearance.BorderSize = 2;
            this.btnRuta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRuta.ForeColor = System.Drawing.Color.White;
            this.btnRuta.Location = new System.Drawing.Point(48, 41);
            this.btnRuta.Name = "btnRuta";
            this.btnRuta.Size = new System.Drawing.Size(36, 29);
            this.btnRuta.TabIndex = 13;
            this.btnRuta.UseVisualStyleBackColor = false;
            this.btnRuta.Click += new System.EventHandler(this.btnRuta_Click);
            // 
            // txtRutaBackup
            // 
            this.txtRutaBackup.BackColor = System.Drawing.Color.White;
            this.txtRutaBackup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRutaBackup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtRutaBackup.Enabled = false;
            this.txtRutaBackup.Font = new System.Drawing.Font("Ebrima", 12F);
            this.txtRutaBackup.Location = new System.Drawing.Point(87, 43);
            this.txtRutaBackup.Name = "txtRutaBackup";
            this.txtRutaBackup.Size = new System.Drawing.Size(490, 22);
            this.txtRutaBackup.TabIndex = 14;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.White;
            this.Label9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label9.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.Label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label9.Location = new System.Drawing.Point(44, 17);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(494, 21);
            this.Label9.TabIndex = 599;
            this.Label9.Text = "Seleccione una Carpeta donde Guardar Las Copias de Seguridad\r\n";
            // 
            // PanelLogo
            // 
            this.PanelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelLogo.BackColor = System.Drawing.Color.White;
            this.PanelLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelLogo.BackgroundImage")));
            this.PanelLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelLogo.Controls.Add(this.cboRegimen);
            this.PanelLogo.Controls.Add(this.panel28);
            this.PanelLogo.Controls.Add(this.panel31);
            this.PanelLogo.Controls.Add(this.panel32);
            this.PanelLogo.Controls.Add(this.txtTelefono);
            this.PanelLogo.Controls.Add(this.txtRFC);
            this.PanelLogo.Controls.Add(this.label26);
            this.PanelLogo.Controls.Add(this.label29);
            this.PanelLogo.Controls.Add(this.label30);
            this.PanelLogo.Controls.Add(this.PanelBorder);
            this.PanelLogo.Controls.Add(this.txtNombreEmpresa);
            this.PanelLogo.Controls.Add(this.lblempresa);
            this.PanelLogo.Location = new System.Drawing.Point(9, 6);
            this.PanelLogo.Name = "PanelLogo";
            this.PanelLogo.Size = new System.Drawing.Size(734, 147);
            this.PanelLogo.TabIndex = 590;
            // 
            // cboRegimen
            // 
            this.cboRegimen.DropDownHeight = 100;
            this.cboRegimen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRegimen.DropDownWidth = 230;
            this.cboRegimen.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRegimen.Font = new System.Drawing.Font("Ebrima", 12F);
            this.cboRegimen.FormattingEnabled = true;
            this.cboRegimen.IntegralHeight = false;
            this.cboRegimen.Location = new System.Drawing.Point(48, 89);
            this.cboRegimen.Name = "cboRegimen";
            this.cboRegimen.Size = new System.Drawing.Size(400, 29);
            this.cboRegimen.TabIndex = 556;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel28.Location = new System.Drawing.Point(48, 118);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(400, 2);
            this.panel28.TabIndex = 561;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel31.Location = new System.Drawing.Point(459, 120);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(201, 2);
            this.panel31.TabIndex = 562;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel32.Location = new System.Drawing.Point(461, 63);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(201, 2);
            this.panel32.TabIndex = 563;
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.White;
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtTelefono.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtTelefono.ForeColor = System.Drawing.Color.Black;
            this.txtTelefono.Location = new System.Drawing.Point(457, 97);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(201, 21);
            this.txtTelefono.TabIndex = 557;
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.Color.White;
            this.txtRFC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRFC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtRFC.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtRFC.ForeColor = System.Drawing.Color.Black;
            this.txtRFC.Location = new System.Drawing.Point(461, 45);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(201, 21);
            this.txtRFC.TabIndex = 555;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label26.Location = new System.Drawing.Point(45, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 15);
            this.label26.TabIndex = 558;
            this.label26.Text = "Regimen Fiscal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label29.Location = new System.Drawing.Point(454, 78);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(57, 15);
            this.label29.TabIndex = 559;
            this.label29.Text = "Teléfono";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label30.Location = new System.Drawing.Point(458, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 15);
            this.label30.TabIndex = 560;
            this.label30.Text = "R.F.C";
            // 
            // PanelBorder
            // 
            this.PanelBorder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.PanelBorder.Location = new System.Drawing.Point(48, 64);
            this.PanelBorder.Name = "PanelBorder";
            this.PanelBorder.Size = new System.Drawing.Size(400, 2);
            this.PanelBorder.TabIndex = 554;
            // 
            // txtNombreEmpresa
            // 
            this.txtNombreEmpresa.BackColor = System.Drawing.Color.White;
            this.txtNombreEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombreEmpresa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNombreEmpresa.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNombreEmpresa.ForeColor = System.Drawing.Color.Black;
            this.txtNombreEmpresa.Location = new System.Drawing.Point(48, 41);
            this.txtNombreEmpresa.Name = "txtNombreEmpresa";
            this.txtNombreEmpresa.Size = new System.Drawing.Size(400, 21);
            this.txtNombreEmpresa.TabIndex = 2;
            // 
            // lblempresa
            // 
            this.lblempresa.AutoSize = true;
            this.lblempresa.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.lblempresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblempresa.Location = new System.Drawing.Point(45, 23);
            this.lblempresa.Name = "lblempresa";
            this.lblempresa.Size = new System.Drawing.Size(135, 15);
            this.lblempresa.TabIndex = 342;
            this.lblempresa.Text = "Nombre de tu Empresa";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblChangeImage);
            this.tabPage3.Controls.Add(this.panel23);
            this.tabPage3.Controls.Add(this.panel25);
            this.tabPage3.Controls.Add(this.panel24);
            this.tabPage3.Controls.Add(this.panel19);
            this.tabPage3.Controls.Add(this.txtUsuario);
            this.tabPage3.Controls.Add(this.txtConfirmarPassword);
            this.tabPage3.Controls.Add(this.txtPassword);
            this.tabPage3.Controls.Add(this.txtNombreOperador);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.label28);
            this.tabPage3.Controls.Add(this.btnBackDirs);
            this.tabPage3.Controls.Add(this.btnSaveFull);
            this.tabPage3.Controls.Add(this.label27);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.pictureBox1);
            this.tabPage3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(746, 558);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lblChangeImage
            // 
            this.lblChangeImage.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblChangeImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblChangeImage.Font = new System.Drawing.Font("Ebrima", 10F, System.Drawing.FontStyle.Bold);
            this.lblChangeImage.ForeColor = System.Drawing.Color.White;
            this.lblChangeImage.Location = new System.Drawing.Point(446, 276);
            this.lblChangeImage.Name = "lblChangeImage";
            this.lblChangeImage.Size = new System.Drawing.Size(125, 32);
            this.lblChangeImage.TabIndex = 602;
            this.lblChangeImage.Text = "Cambiar";
            this.lblChangeImage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChangeImage.Click += new System.EventHandler(this.lblChangeImage_Click);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel23.Location = new System.Drawing.Point(38, 201);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(302, 2);
            this.panel23.TabIndex = 599;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel25.Location = new System.Drawing.Point(38, 307);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(302, 2);
            this.panel25.TabIndex = 599;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel24.Location = new System.Drawing.Point(38, 258);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(302, 2);
            this.panel24.TabIndex = 599;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel19.Location = new System.Drawing.Point(38, 155);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(302, 2);
            this.panel19.TabIndex = 599;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.White;
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtUsuario.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtUsuario.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Location = new System.Drawing.Point(38, 178);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(302, 21);
            this.txtUsuario.TabIndex = 2;
            this.txtUsuario.Text = "admin";
            // 
            // txtConfirmarPassword
            // 
            this.txtConfirmarPassword.BackColor = System.Drawing.Color.White;
            this.txtConfirmarPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConfirmarPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfirmarPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtConfirmarPassword.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtConfirmarPassword.ForeColor = System.Drawing.Color.Black;
            this.txtConfirmarPassword.Location = new System.Drawing.Point(38, 281);
            this.txtConfirmarPassword.MaxLength = 6;
            this.txtConfirmarPassword.Name = "txtConfirmarPassword";
            this.txtConfirmarPassword.PasswordChar = '*';
            this.txtConfirmarPassword.Size = new System.Drawing.Size(302, 21);
            this.txtConfirmarPassword.TabIndex = 4;
            this.txtConfirmarPassword.UseSystemPasswordChar = true;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.White;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtPassword.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtPassword.ForeColor = System.Drawing.Color.Black;
            this.txtPassword.Location = new System.Drawing.Point(38, 235);
            this.txtPassword.MaxLength = 6;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(302, 21);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtNombreOperador
            // 
            this.txtNombreOperador.BackColor = System.Drawing.Color.White;
            this.txtNombreOperador.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombreOperador.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNombreOperador.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNombreOperador.ForeColor = System.Drawing.Color.Black;
            this.txtNombreOperador.Location = new System.Drawing.Point(38, 132);
            this.txtNombreOperador.Name = "txtNombreOperador";
            this.txtNombreOperador.Size = new System.Drawing.Size(302, 21);
            this.txtNombreOperador.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(35, 160);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(49, 15);
            this.label24.TabIndex = 598;
            this.label24.Text = "Usuario";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label31.Location = new System.Drawing.Point(35, 263);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(128, 15);
            this.label31.TabIndex = 598;
            this.label31.Text = "Confirmar Contraseña";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(35, 217);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 15);
            this.label23.TabIndex = 598;
            this.label23.Text = "Contraseña";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label28.Location = new System.Drawing.Point(35, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(116, 15);
            this.label28.TabIndex = 598;
            this.label28.Text = "Nombre del usuario";
            // 
            // btnBackDirs
            // 
            this.btnBackDirs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackDirs.BackColor = System.Drawing.Color.DarkGray;
            this.btnBackDirs.FlatAppearance.BorderSize = 0;
            this.btnBackDirs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackDirs.ForeColor = System.Drawing.Color.White;
            this.btnBackDirs.Location = new System.Drawing.Point(458, 497);
            this.btnBackDirs.Name = "btnBackDirs";
            this.btnBackDirs.Size = new System.Drawing.Size(130, 38);
            this.btnBackDirs.TabIndex = 17;
            this.btnBackDirs.Text = "Atras";
            this.btnBackDirs.UseVisualStyleBackColor = false;
            this.btnBackDirs.Click += new System.EventHandler(this.btnBackDirs_Click);
            // 
            // btnSaveFull
            // 
            this.btnSaveFull.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveFull.BackColor = System.Drawing.Color.SeaGreen;
            this.btnSaveFull.FlatAppearance.BorderSize = 0;
            this.btnSaveFull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveFull.ForeColor = System.Drawing.Color.White;
            this.btnSaveFull.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveFull.Location = new System.Drawing.Point(597, 497);
            this.btnSaveFull.Name = "btnSaveFull";
            this.btnSaveFull.Size = new System.Drawing.Size(130, 38);
            this.btnSaveFull.TabIndex = 18;
            this.btnSaveFull.Text = "Guardar";
            this.btnSaveFull.UseVisualStyleBackColor = false;
            this.btnSaveFull.Click += new System.EventHandler(this.btnSaveFull_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label27.Location = new System.Drawing.Point(19, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(330, 21);
            this.label27.TabIndex = 2;
            this.label27.Text = "¿Que usuario principal usara el programa?";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label25.Location = new System.Drawing.Point(384, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(315, 73);
            this.label25.TabIndex = 2;
            this.label25.Text = "El administrador siempre tendra acceso a todas las funciones del sistema";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AppINTROIT.Properties.Resources.user1281;
            this.pictureBox1.Location = new System.Drawing.Point(446, 149);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 118);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 600;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.panel1.Controls.Add(this.Label3);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(778, 227);
            this.panel1.TabIndex = 2;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Ebrima", 20F);
            this.Label3.ForeColor = System.Drawing.Color.White;
            this.Label3.Location = new System.Drawing.Point(11, 11);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(194, 37);
            this.Label3.TabIndex = 63;
            this.Label3.Text = "Datos Empresa";
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Ebrima", 12F);
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCerrar.Location = new System.Drawing.Point(731, 17);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(35, 35);
            this.btnCerrar.TabIndex = 62;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Text = "X";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // ofdUser
            // 
            this.ofdUser.FileName = "openFileDialog1";
            // 
            // RegisterCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(778, 671);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Ebrima", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RegisterCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegisterCompany";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RegisterCompany_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.PanelLogo.ResumeLayout(false);
            this.PanelLogo.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Button btnNext1;
        internal System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.Button btnRuta;
        internal System.Windows.Forms.TextBox txtRutaBackup;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel PanelLogo;
        internal System.Windows.Forms.Panel PanelBorder;
        internal System.Windows.Forms.TextBox txtNombreEmpresa;
        internal System.Windows.Forms.Label lblempresa;
        private System.Windows.Forms.TabPage tabPage3;
        internal System.Windows.Forms.Button btnBackDirs;
        internal System.Windows.Forms.Button btnSaveFull;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button btnCerrar;
        internal System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Panel panel12;
        internal System.Windows.Forms.Panel panel13;
        internal System.Windows.Forms.Panel panel17;
        internal System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Panel panel27;
        internal System.Windows.Forms.Panel panel29;
        internal System.Windows.Forms.Panel panel30;
        internal System.Windows.Forms.Panel panel33;
        internal System.Windows.Forms.Panel panel34;
        internal System.Windows.Forms.Panel panel35;
        internal System.Windows.Forms.Panel panel36;
        internal System.Windows.Forms.TextBox txtNumInterior;
        internal System.Windows.Forms.TextBox txtEstado;
        internal System.Windows.Forms.TextBox txtLocalidad;
        internal System.Windows.Forms.TextBox txtNumExterior;
        internal System.Windows.Forms.TextBox txtCiudad;
        internal System.Windows.Forms.TextBox txtColonia;
        internal System.Windows.Forms.TextBox txtCruzamientos;
        internal System.Windows.Forms.TextBox txtPais;
        internal System.Windows.Forms.TextBox txtMunicipio;
        internal System.Windows.Forms.TextBox txtCodigoPostal;
        internal System.Windows.Forms.TextBox txtCalle;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label label33;
        internal System.Windows.Forms.ComboBox cboRegimen;
        internal System.Windows.Forms.Panel panel28;
        internal System.Windows.Forms.Panel panel31;
        internal System.Windows.Forms.Panel panel32;
        internal System.Windows.Forms.TextBox txtTelefono;
        internal System.Windows.Forms.TextBox txtRFC;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Label lblChangeImage;
        private System.Windows.Forms.PictureBox pictureBox1;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Panel panel25;
        internal System.Windows.Forms.Panel panel24;
        internal System.Windows.Forms.Panel panel19;
        internal System.Windows.Forms.TextBox txtUsuario;
        internal System.Windows.Forms.TextBox txtConfirmarPassword;
        internal System.Windows.Forms.TextBox txtPassword;
        internal System.Windows.Forms.TextBox txtNombreOperador;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label label27;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.OpenFileDialog ofdUser;
    }
}