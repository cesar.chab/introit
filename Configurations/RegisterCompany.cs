﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using AppINTROIT.Modelos;

namespace AppINTROIT.Configurations
{
    public partial class RegisterCompany : Form
    {
        private static RegisterCompany _DefaultInstance;
        private System.IO.MemoryStream ms = new System.IO.MemoryStream();

        public static RegisterCompany DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new RegisterCompany();

                return _DefaultInstance;
            }
        }

        public RegisterCompany()
        {
            InitializeComponent();
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;

            foreach (TabPage tab in tabControl1.TabPages)
            {
                tab.Text = "";
            }

            //Regimen Fiscal
            var DT = new TaxRegime().all();
            cboRegimen.DataSource = DT;
            cboRegimen.ValueMember = "Rf_Codigo";
            cboRegimen.DisplayMember = "Rf_Descripcion";
            
        }

        private void RegisterCompany_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-MX");
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ",";
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ",";
            txtNombreEmpresa.Focus();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Es necesario configurar los datos de la empresa para poder continuar...", "Parametros Iniciales", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Cancel)
                if (result == DialogResult.Cancel)
                {
                    System.Environment.Exit(1);
                }
        }

        private void btnNext1_Click(object sender, EventArgs e)
        {
            ActiveCompany._Em_Nombre = txtNombreEmpresa.Text;
            ActiveCompany._Ruta_Backup = txtRutaBackup.Text;
            ActiveCompany._Em_Calle = txtCalle.Text;
            ActiveCompany._Em_Cruzamiento = txtCruzamientos.Text;
            ActiveCompany._Em_Numext = txtNumExterior.Text;
            ActiveCompany._Em_Numint = txtNumInterior.Text;
            ActiveCompany._Em_CodPost = txtCodigoPostal.Text;
            ActiveCompany._Em_Colonia = txtColonia.Text;
            ActiveCompany._Em_Localidad = txtLocalidad.Text;
            ActiveCompany._Em_Municipio = txtMunicipio.Text;
            ActiveCompany._Em_Ciudad = txtCiudad.Text;
            ActiveCompany._Em_Pais = txtPais.Text;
            ActiveCompany._Em_Estado = txtEstado.Text;
            ActiveCompany._Em_RFC = txtRFC.Text;
            ActiveCompany._Em_CURP = "";
            ActiveCompany._Em_Telefono = txtTelefono.Text;
            if (!String.IsNullOrEmpty(cboRegimen.Text))
            {
                ActiveCompany._Em_Cve_Regimen_Fiscal = cboRegimen.SelectedValue.ToString();
                ActiveCompany._Em_Regimen_Fiscal = cboRegimen.Text;
            }
            else
            {
                ActiveCompany._Em_Cve_Regimen_Fiscal = "";
                ActiveCompany._Em_Regimen_Fiscal = "";
            }
            tabControl1.SelectTab(1);

        }

        private void btnBackDirs_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
        }

        private void btnRuta_Click(object sender, EventArgs e)
        {
            if (FolderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRutaBackup.Text = FolderBrowserDialog1.SelectedPath;
            }
        }

        private void lblChangeImage_Click(object sender, EventArgs e)
        {
            ofdUser.InitialDirectory = "";
            ofdUser.Filter = "Imagenes|*.jpg;*.png";
            ofdUser.FilterIndex = 2;
            ofdUser.Title = "Imagen Usuario";

            if (ofdUser.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.BackgroundImage = null;
                pictureBox1.Image = new Bitmap(ofdUser.FileName);
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        /// <summary>
        /// Funcion para validar el nombre de la empresa
        /// </summary>
        /// <returns></returns>
        private bool valEmpresa()
        {
            bool status = true;
            if (String.IsNullOrEmpty(txtNombreEmpresa.Text.Trim()))
            {
                errorProvider1.SetError(txtNombreEmpresa, "Nombre empresa requerido");
                status = false;
            }
            else
            {
                errorProvider1.SetError(txtNombreEmpresa, "");
            }

            return status;
        }

        /// <summary>
        /// Funcion para validar el nombre del cajero
        /// </summary>
        /// <returns></returns>
        private bool valUser()
        {
            bool status = true;

            if (String.IsNullOrEmpty(txtUsuario.Text.Trim()))
            {
                errorProvider1.SetError(txtUsuario, "Debe ingresar el nombre del usuario");
                status = false;
            }

            return status;
        }

        /// <summary>
        /// Funcion para validar el nombre del cajero
        /// </summary>
        /// <returns></returns>
        private bool valPassword()
        {
            bool status = true;

            if (String.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                errorProvider1.SetError(txtPassword, "Debe ingresar una contraseña");
                status = false;
            }
            else if (txtPassword.Text != txtConfirmarPassword.Text)
            {
                errorProvider1.SetError(txtConfirmarPassword, "La");
                status = false;
            }

            return status;
        }

        private void btnSaveFull_Click(object sender, EventArgs e)
        {
            if (valUser() && valPassword())
            {
                DateTime _TODAY = DateTime.Now;

                ActiveOperator._Id = 0;
                ActiveOperator._Nombre = txtNombreOperador.Text;
                ActiveOperator._Login = txtUsuario.Text.Trim();
                ActiveOperator._Password = Helper.Encripta(txtConfirmarPassword.Text.Trim());

                //Generate Lic
                ActiveLicence._Id = 0;
                ActiveLicence._Descripcion = "Licencia de prueba gratuita (30 días)";
                ActiveLicence._FI = Cifrado.Encriptar(_TODAY.ToString());
                ActiveLicence._FF = Cifrado.Encriptar(_TODAY.AddDays(30).ToString());
                ActiveLicence._FA = Cifrado.Encriptar(_TODAY.ToString());
                ActiveLicence._SerialPC = Cifrado.Encriptar(Helpers.Helper.serialDD());

                //Crear empresa
                var ClsDatos = new Conexion();
                String _sql = "EXEC [dbo].[Empresas_IU] '', '" + ActiveCompany._Em_Nombre + "', '" + ActiveCompany._Em_Calle + "', " +
                    "'" + ActiveCompany._Em_Cruzamiento + "', '" + ActiveCompany._Em_Numext + "', '" + ActiveCompany._Em_Numint + "', '" + ActiveCompany._Em_Colonia + "', " +
                    "'" + ActiveCompany._Em_Localidad + "', '" + ActiveCompany._Em_Municipio + "', '" + ActiveCompany._Em_Ciudad + "', '" + ActiveCompany._Em_Estado + "', " +
                    "'" + ActiveCompany._Em_Pais + "', '" + ActiveCompany._Em_CodPost + "', '" + ActiveCompany._Em_Telefono + "', '" + ActiveCompany._Em_RFC + "', " +
                    "'" + ActiveCompany._Em_Email + "', '" + ActiveCompany._Em_Regimen_Fiscal + "', '" + ActiveCompany._Em_Cve_Regimen_Fiscal + "',null, '" + ActiveCompany._Em_Maneja_Impuesto + "', " +
                    "'" + ActiveCompany._Em_Impuesto + "', " + ActiveCompany._Em_Impuesto_porcentaje + ", '" + ActiveCompany._Em_Moneda + "', '" + ActiveCompany._Modo_Busqueda + "', " +
                    "'" + ActiveCompany._Ruta_Backup + "', null, 1, '" + ActiveCompany._Redondeo_Total + "', 'CONFIG', 0, 1, " +
                    "'" + ActiveOperator._Nombre + "', '" + ActiveOperator._Login + "', '" + ActiveOperator._Password + "', '" + pictureBox1.Image + "', '" + ActiveCompany._Em_Email + "', 'Administrador', 1, " +
                    "'" + ActiveLicence._Descripcion + "', '" + ActiveLicence._FI + "', '" + ActiveLicence._FF + "', '" + ActiveLicence._FA + "', '" + ActiveLicence._SerialPC + "'";

                 var DT = new DataTable();
                if (ClsDatos.CargaTabla(_sql, ref DT))
                {
                    if (DT.Rows.Count > 0)
                    {
                        ////Guardamos el logo de la empresa
                        //Collection<SQLpar> Parametros = new Collection<SQLpar>();
                        //MemoryStream ms1 = new MemoryStream();
                        //var sqlpar = new SQLpar();
                        //sqlpar.StrNombreParametro = "@logo";
                        //sqlpar.DataType = SqlDbType.VarBinary;
                        //pictureBoxLogo.Image.Size.ToString();
                        //pictureBoxLogo.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //sqlpar.obj = ms1;
                        //Parametros.Add(sqlpar);
                        //string sqlUpdate = "UPDATE Empresa SET Em_Logo=@logo WHERE Em_Cve_Empresa='0001'";
                        //ClsDatos.CargaComando(sqlUpdate, Parametros);
                        //ClsDatos.Execute();

                        //Guardamos el logo del usuario
                        Collection<Helper.SQLpar> ParametrosUser = new Collection<Helper.SQLpar>();
                        MemoryStream msUser = new MemoryStream();
                        var sqlparUser = new Helper.SQLpar();
                        sqlparUser.StrNombreParametro = "@icono";
                        sqlparUser.DataType = SqlDbType.VarBinary;
                        pictureBox1.Image.Size.ToString();
                        pictureBox1.Image.Save(msUser, System.Drawing.Imaging.ImageFormat.Jpeg);
                        sqlparUser.obj = msUser;
                        ParametrosUser.Add(sqlparUser);
                        string sqlUpdateUser = "UPDATE Operadores SET Icono=@icono WHERE Id=1";
                        ClsDatos.CargaComando(sqlUpdateUser, ParametrosUser);
                        ClsDatos.Execute();
                        MessageBox.Show("!LISTO¡ RECUERDA que para iniciar sesión tu usuario es " + ActiveOperator._Login + " y tu contraseña es " + txtPassword.Text, "Registro Exitoso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Dispose();
                        Login.DefaultInstance.ShowDialog();
                    }
                }
                else
                {
                   MessageBox.Show(ClsDatos.MsjError, "Error generado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}