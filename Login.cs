﻿using AppINTROIT.Entidades;
using AppINTROIT.Forms;
using AppINTROIT.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT
{
    public partial class Login : Form
    {
        private static Login _DefaultInstance;

        public static Login DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new Login();

                return _DefaultInstance;
            }
        }
        public Login()
        {
            InitializeComponent();

            pictureBox1.Location = new Point((splitContainer1.Panel1.Width / 2) - (pictureBox1.Width / 2),
               (splitContainer1.Panel1.Height / 2) - (pictureBox1.Height / 2));

            panelControlsLogin.Location = new Point((splitContainer1.Panel2.Width / 2) - (panelControlsLogin.Width / 2),
                (splitContainer1.Panel2.Height / 2) - (panelControlsLogin.Height / 2));
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Focus();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ActiveOperator._Login = txtUser.Text;
            ActiveOperator._Password = txtPassword.Text;
            bool _valida = new Operator().Login();
            if (_valida)
            {
                this.Dispose();
                FormMain.DefaultInstance.ShowDialog();
            }
            else
            {
                MessageBox.Show("Usuario o contraseña incorrecta", "Acceso al sistema", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
