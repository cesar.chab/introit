﻿
namespace AppINTROIT
{
    partial class FormConexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConexion));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.PictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblestadoLicencia = new System.Windows.Forms.Label();
            this.panelControlsLogin = new System.Windows.Forms.Panel();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.cboServer = new System.Windows.Forms.ComboBox();
            this.panelAuth = new System.Windows.Forms.Panel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.PBLoading = new System.Windows.Forms.PictureBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnValidar = new System.Windows.Forms.Button();
            this.cboAutenticacion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBase = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).BeginInit();
            this.panelControlsLogin.SuspendLayout();
            this.panelAuth.SuspendLayout();
            this.panelButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.btnCerrar);
            this.splitContainer1.Panel2.Controls.Add(this.PictureBox4);
            this.splitContainer1.Panel2.Controls.Add(this.lblestadoLicencia);
            this.splitContainer1.Panel2.Controls.Add(this.panelControlsLogin);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.Label3);
            this.splitContainer1.Size = new System.Drawing.Size(811, 530);
            this.splitContainer1.SplitterDistance = 310;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AppINTROIT.Properties.Resources.paper;
            this.pictureBox1.Location = new System.Drawing.Point(81, 163);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(152, 152);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Ebrima", 12F);
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCerrar.Location = new System.Drawing.Point(455, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(39, 32);
            this.btnCerrar.TabIndex = 613;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Text = "X";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // PictureBox4
            // 
            this.PictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox4.Image")));
            this.PictureBox4.Location = new System.Drawing.Point(23, 501);
            this.PictureBox4.Name = "PictureBox4";
            this.PictureBox4.Size = new System.Drawing.Size(23, 17);
            this.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox4.TabIndex = 612;
            this.PictureBox4.TabStop = false;
            // 
            // lblestadoLicencia
            // 
            this.lblestadoLicencia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblestadoLicencia.AutoSize = true;
            this.lblestadoLicencia.Font = new System.Drawing.Font("Ebrima", 12F);
            this.lblestadoLicencia.ForeColor = System.Drawing.Color.Gray;
            this.lblestadoLicencia.Location = new System.Drawing.Point(52, 500);
            this.lblestadoLicencia.Name = "lblestadoLicencia";
            this.lblestadoLicencia.Size = new System.Drawing.Size(69, 21);
            this.lblestadoLicencia.TabIndex = 611;
            this.lblestadoLicencia.Text = "Licencia ";
            // 
            // panelControlsLogin
            // 
            this.panelControlsLogin.Controls.Add(this.lblEstatus);
            this.panelControlsLogin.Controls.Add(this.cboServer);
            this.panelControlsLogin.Controls.Add(this.panelAuth);
            this.panelControlsLogin.Controls.Add(this.panelButtons);
            this.panelControlsLogin.Controls.Add(this.cboAutenticacion);
            this.panelControlsLogin.Controls.Add(this.label4);
            this.panelControlsLogin.Controls.Add(this.txtBase);
            this.panelControlsLogin.Controls.Add(this.label6);
            this.panelControlsLogin.Controls.Add(this.label5);
            this.panelControlsLogin.Controls.Add(this.label2);
            this.panelControlsLogin.Location = new System.Drawing.Point(23, 109);
            this.panelControlsLogin.Name = "panelControlsLogin";
            this.panelControlsLogin.Size = new System.Drawing.Size(444, 386);
            this.panelControlsLogin.TabIndex = 59;
            // 
            // lblEstatus
            // 
            this.lblEstatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblEstatus.ForeColor = System.Drawing.Color.Black;
            this.lblEstatus.Location = new System.Drawing.Point(28, 369);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(76, 17);
            this.lblEstatus.TabIndex = 67;
            this.lblEstatus.Text = "Iniciando...";
            this.lblEstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblEstatus.Visible = false;
            // 
            // cboServer
            // 
            this.cboServer.BackColor = System.Drawing.SystemColors.Control;
            this.cboServer.Font = new System.Drawing.Font("Ebrima", 12F);
            this.cboServer.ForeColor = System.Drawing.Color.Gray;
            this.cboServer.FormattingEnabled = true;
            this.cboServer.Location = new System.Drawing.Point(28, 37);
            this.cboServer.Name = "cboServer";
            this.cboServer.Size = new System.Drawing.Size(350, 29);
            this.cboServer.TabIndex = 2;
            // 
            // panelAuth
            // 
            this.panelAuth.Controls.Add(this.txtPassword);
            this.panelAuth.Controls.Add(this.label8);
            this.panelAuth.Controls.Add(this.txtUsuario);
            this.panelAuth.Controls.Add(this.label7);
            this.panelAuth.Location = new System.Drawing.Point(28, 180);
            this.panelAuth.Name = "panelAuth";
            this.panelAuth.Size = new System.Drawing.Size(350, 130);
            this.panelAuth.TabIndex = 17;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Control;
            this.txtPassword.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.ForeColor = System.Drawing.Color.Gray;
            this.txtPassword.Location = new System.Drawing.Point(0, 84);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(350, 29);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(-1, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 21);
            this.label8.TabIndex = 15;
            this.label8.Text = "Contraseña";
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.SystemColors.Control;
            this.txtUsuario.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.ForeColor = System.Drawing.Color.Gray;
            this.txtUsuario.Location = new System.Drawing.Point(0, 27);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(350, 29);
            this.txtUsuario.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(1, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "Usuario";
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.PBLoading);
            this.panelButtons.Controls.Add(this.btnGuardar);
            this.panelButtons.Controls.Add(this.btnValidar);
            this.panelButtons.Location = new System.Drawing.Point(28, 320);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(350, 49);
            this.panelButtons.TabIndex = 16;
            // 
            // PBLoading
            // 
            this.PBLoading.Image = ((System.Drawing.Image)(resources.GetObject("PBLoading.Image")));
            this.PBLoading.Location = new System.Drawing.Point(5, -4);
            this.PBLoading.Name = "PBLoading";
            this.PBLoading.Size = new System.Drawing.Size(42, 42);
            this.PBLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PBLoading.TabIndex = 68;
            this.PBLoading.TabStop = false;
            this.PBLoading.Visible = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.SeaGreen;
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Ebrima", 14F);
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.Location = new System.Drawing.Point(193, 9);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(154, 37);
            this.btnGuardar.TabIndex = 8;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnValidar
            // 
            this.btnValidar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.btnValidar.FlatAppearance.BorderSize = 0;
            this.btnValidar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnValidar.Font = new System.Drawing.Font("Ebrima", 14F);
            this.btnValidar.ForeColor = System.Drawing.Color.White;
            this.btnValidar.Location = new System.Drawing.Point(3, 9);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(154, 37);
            this.btnValidar.TabIndex = 7;
            this.btnValidar.Text = "Validar";
            this.btnValidar.UseVisualStyleBackColor = false;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // cboAutenticacion
            // 
            this.cboAutenticacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAutenticacion.Font = new System.Drawing.Font("Ebrima", 12F);
            this.cboAutenticacion.ForeColor = System.Drawing.Color.Gray;
            this.cboAutenticacion.FormattingEnabled = true;
            this.cboAutenticacion.Items.AddRange(new object[] {
            "Windows Autenticación",
            "SQL Server Autenticación"});
            this.cboAutenticacion.Location = new System.Drawing.Point(28, 149);
            this.cboAutenticacion.Name = "cboAutenticacion";
            this.cboAutenticacion.Size = new System.Drawing.Size(350, 29);
            this.cboAutenticacion.TabIndex = 4;
            this.cboAutenticacion.SelectedIndexChanged += new System.EventHandler(this.cboAutenticacion_SelectedIndexChanged);
            this.cboAutenticacion.SelectedValueChanged += new System.EventHandler(this.cboAutenticacion_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(138, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "(Por default es INTROIT)";
            // 
            // txtBase
            // 
            this.txtBase.BackColor = System.Drawing.SystemColors.Control;
            this.txtBase.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBase.ForeColor = System.Drawing.Color.Gray;
            this.txtBase.Location = new System.Drawing.Point(28, 93);
            this.txtBase.Name = "txtBase";
            this.txtBase.Size = new System.Drawing.Size(350, 29);
            this.txtBase.TabIndex = 3;
            this.txtBase.Text = "INTROIT";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gray;
            this.label6.Location = new System.Drawing.Point(24, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "Autenticación";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(24, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 21);
            this.label5.TabIndex = 13;
            this.label5.Text = "Base de Datos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(24, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 21);
            this.label2.TabIndex = 13;
            this.label2.Text = "Servidor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ebrima", 14F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(18, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 25);
            this.label1.TabIndex = 58;
            this.label1.Text = "Parametros de conexión a la base de datos";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Ebrima", 32F);
            this.Label3.ForeColor = System.Drawing.Color.Gray;
            this.Label3.Location = new System.Drawing.Point(13, 9);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(289, 59);
            this.Label3.TabIndex = 57;
            this.Label3.Text = "Configuración";
            // 
            // FormConexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.ClientSize = new System.Drawing.Size(811, 530);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormConexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormConexion";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormConexion_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).EndInit();
            this.panelControlsLogin.ResumeLayout(false);
            this.panelControlsLogin.PerformLayout();
            this.panelAuth.ResumeLayout(false);
            this.panelAuth.PerformLayout();
            this.panelButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PBLoading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        internal System.Windows.Forms.Button btnCerrar;
        internal System.Windows.Forms.PictureBox PictureBox4;
        internal System.Windows.Forms.Label lblestadoLicencia;
        private System.Windows.Forms.Panel panelControlsLogin;
        private System.Windows.Forms.ComboBox cboServer;
        private System.Windows.Forms.Panel panelAuth;
        internal System.Windows.Forms.TextBox txtPassword;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox txtUsuario;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelButtons;
        internal System.Windows.Forms.Button btnGuardar;
        internal System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.ComboBox cboAutenticacion;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtBase;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label3;
        private System.Windows.Forms.PictureBox PBLoading;
        internal System.Windows.Forms.Label lblEstatus;
    }
}