USE [master]
GO
/****** Object:  Database [INTROIT]    Script Date: 02/12/2020 10:59:31 p. m. ******/
CREATE DATABASE [INTROIT]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Notas', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Notas.mdf' , SIZE = 5312KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Notas_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Notas_log.ldf' , SIZE = 1856KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [INTROIT].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [INTROIT] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [INTROIT] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [INTROIT] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [INTROIT] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [INTROIT] SET ARITHABORT OFF 
GO
ALTER DATABASE [INTROIT] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [INTROIT] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [INTROIT] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [INTROIT] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [INTROIT] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [INTROIT] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [INTROIT] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [INTROIT] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [INTROIT] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [INTROIT] SET  DISABLE_BROKER 
GO
ALTER DATABASE [INTROIT] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [INTROIT] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [INTROIT] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [INTROIT] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [INTROIT] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [INTROIT] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [INTROIT] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [INTROIT] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [INTROIT] SET  MULTI_USER 
GO
ALTER DATABASE [INTROIT] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [INTROIT] SET DB_CHAINING OFF 
GO
ALTER DATABASE [INTROIT] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [INTROIT] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'INTROIT', N'ON'
GO
USE [INTROIT]
GO
/****** Object:  UserDefinedFunction [dbo].[CIntToChar]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CIntToChar](@intVal BIGINT, @intLen Int) RETURNS nvarchar(20)
AS
BEGIN

    -- BIGINT = 2^63-1 (9,223,372,036,854,775,807) Max size number
    -- @intlen contains the string size to return
    IF @intlen > 20
       SET @intlen = 20

    RETURN REPLICATE('0',@intLen-LEN(RTRIM(CONVERT(nvarchar(20),@intVal)))) 
        + CONVERT(nvarchar(20),@intVal)
END	
GO
/****** Object:  UserDefinedFunction [dbo].[Get_Numbers]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------------------------
--RECIBE UNA CADENA Y REGRESA LOS NUMEROS CONTENIDOS EN ELLA
----------------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[Get_Numbers] (
	@Var AS nvarchar(3000) 
)  
RETURNS nvarchar(3000)
AS  
BEGIN 

DECLARE @Car as integer
DECLARE @Line as nvarchar(3000) 

SET @Line = ''
SET @Car = 0

WHILE @Car < LEN(@Var)
BEGIN
	SET @Car = @Car + 1
		
	IF  SUBSTRING(@Var,@Car,1) like '[0-9]'
	BEGIN				
		SET @Line = @Line +  SUBSTRING(@Var,@Car,1)  
	END 
END		

	RETURN  @Line 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Get_ValueOf]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------------------------
--RECIBE UNA CADENA Y QUITA LOS CEROS Y LETRAS A LA IZQUIERA 
--HASTA ENCONTRAR EL PRIMER NUMERO DIFERENTE DE CERO
----------------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[Get_ValueOf] (
	@Var as nvarchar(3000) 
)  
RETURNS nvarchar(3000)
AS  
BEGIN 

DECLARE @Car as integer
DECLARE @Line as nvarchar(3000) 
DECLARE @Caracter as nvarchar(2)

SET @Car = 0
SET @Caracter = ''
SET @Line = ''
SET @Var = dbo.Get_Numbers(@Var)

WHILE @Car < LEN(@Var)
BEGIN
	SET @Car = @Car + 1
	SET @Caracter = SUBSTRING(@Var,@Car,1)

	IF  @Caracter LIKE '[1-9]'
	BEGIN			
		
		SET @Line = RIGHT(@Var,LEN(@Var)-(@Car-1))  	

		BREAK
	END 

END		
	
IF  @Line = ''
BEGIN					
	SET @Line = '0'
END 

	RETURN @Line
END
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[Cl_Cve_Cliente] [nvarchar](10) NOT NULL,
	[Cl_Nombre] [nvarchar](150) NOT NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Alta] [nvarchar](35) NOT NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[Cl_Cve_Cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Conceptos]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conceptos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](150) NOT NULL,
	[precio_gasolina] [decimal](18, 3) NOT NULL,
	[precio_diesel] [decimal](18, 3) NOT NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Alta] [nvarchar](35) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](35) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Oper_Baja] [nvarchar](35) NOT NULL,
 CONSTRAINT [PK_Conceptos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresa](
	[Em_Cve_Empresa] [nvarchar](4) NOT NULL,
	[Em_Nombre] [nvarchar](60) NOT NULL,
	[Em_Calle] [nvarchar](50) NOT NULL,
	[Em_Cruzamiento] [nvarchar](50) NOT NULL,
	[Em_Numext] [nvarchar](10) NOT NULL,
	[Em_Numint] [nvarchar](10) NULL,
	[Em_Colonia] [nvarchar](50) NOT NULL,
	[Em_Localidad] [nvarchar](60) NOT NULL,
	[Em_Municipio] [nvarchar](60) NOT NULL,
	[Em_Ciudad] [nvarchar](60) NOT NULL,
	[Em_Estado] [nvarchar](60) NOT NULL,
	[Em_Pais] [nvarchar](50) NOT NULL,
	[Em_CodPost] [nvarchar](5) NOT NULL,
	[Em_Telefono] [nvarchar](50) NULL,
	[Em_RFC] [nvarchar](13) NOT NULL,
	[Em_Email] [nvarchar](50) NULL,
	[Em_Regimen_Fiscal] [nvarchar](250) NOT NULL,
	[Em_Cve_Regimen_Fiscal] [nvarchar](3) NOT NULL,
	[Em_Logo] [image] NULL,
	[Em_Maneja_Impuesto] [nvarchar](10) NOT NULL,
	[Em_Impuesto] [nvarchar](50) NULL,
	[Em_Impuesto_porcentaje] [decimal](18, 0) NULL,
	[Em_Moneda] [nvarchar](50) NULL,
	[Modo_Busqueda] [nvarchar](50) NOT NULL,
	[Ruta_Backup] [nvarchar](255) NULL,
	[Ultimo_backup] [datetime] NOT NULL,
	[Frecuencia_backup] [int] NOT NULL,
	[Estado_backup] [nvarchar](50) NOT NULL,
	[Redondeo_Total] [nvarchar](4) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Alta] [nvarchar](35) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](35) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Oper_Baja] [nvarchar](35) NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[Em_Cve_Empresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estado]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estado](
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Es_Descripcion] [nvarchar](50) NULL,
 CONSTRAINT [PK_Estado] PRIMARY KEY CLUSTERED 
(
	[Es_Cve_Estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folios]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folios](
	[Tipo] [nvarchar](50) NOT NULL,
	[Folio] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Folios] PRIMARY KEY CLUSTERED 
(
	[Tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Licencias]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Licencias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](150) NOT NULL,
	[FI] [nvarchar](255) NOT NULL,
	[FF] [nvarchar](255) NOT NULL,
	[FA] [nvarchar](255) NOT NULL,
	[Serial_PC] [nvarchar](255) NOT NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Licencias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operadores]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operadores](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_completo] [nvarchar](100) NULL,
	[Login] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Icono] [image] NULL,
	[Nombre_Icono] [nvarchar](100) NULL,
	[Correo] [nvarchar](100) NULL,
	[Rol] [nvarchar](100) NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Operadores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegimenFiscal]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegimenFiscal](
	[Rf_Id] [int] IDENTITY(1,1) NOT NULL,
	[Rf_Codigo] [nvarchar](255) NULL,
	[Rf_Descripcion] [nvarchar](255) NULL,
	[Rf_Fisica] [nvarchar](255) NULL,
	[Rf_Moral] [nvarchar](255) NULL,
	[Rf_FechaInicio] [date] NULL,
	[Rf_FechaFin] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Remision_Detalle]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Remision_Detalle](
	[Rm_Folio] [nvarchar](10) NOT NULL,
	[Rm_ID] [nvarchar](4) NOT NULL,
	[Rm_Fecha] [datetime] NOT NULL,
	[Rm_Concepto_Id] [int] NOT NULL,
	[Rm_Concepto] [nvarchar](100) NOT NULL,
	[Rm_Cantidad] [decimal](18, 9) NOT NULL,
	[Rm_Precio] [decimal](18, 9) NOT NULL,
	[Rm_Importe] [decimal](18, 9) NOT NULL,
	[Oper_Alta] [nvarchar](15) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](15) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Baja] [nvarchar](15) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Remision_Encabezado]    Script Date: 02/12/2020 10:59:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Remision_Encabezado](
	[Rm_Folio] [nvarchar](10) NOT NULL,
	[Rm_Fecha] [datetime] NOT NULL,
	[Rm_Referencia] [nvarchar](15) NOT NULL,
	[Rm_Comentario] [text] NULL,
	[Vn_Cve_Vendedor] [nvarchar](10) NULL,
	[Cl_Cve_Cliente] [nvarchar](10) NULL,
	[Rm_Importe] [decimal](18, 9) NOT NULL,
	[Oper_Alta] [nvarchar](15) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](15) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Baja] [nvarchar](15) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Clientes] ([Cl_Cve_Cliente], [Cl_Nombre], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta]) VALUES (N'0000000001', N'CESAR ENRIQUE CHAB ULUAC', N'AC', CAST(N'2020-12-02T22:22:13.297' AS DateTime), N'admin')
GO
INSERT [dbo].[Clientes] ([Cl_Cve_Cliente], [Cl_Nombre], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta]) VALUES (N'0000000002', N'CESAR ENRIQUE CHAB ULUAC', N'AC', CAST(N'2020-12-02T22:24:37.540' AS DateTime), N'admin')
GO
INSERT [dbo].[Clientes] ([Cl_Cve_Cliente], [Cl_Nombre], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta]) VALUES (N'0000000003', N'CESAR ENRIQUE CHAB ULUAC', N'AC', CAST(N'2020-12-02T22:25:07.090' AS DateTime), N'admin')
GO
INSERT [dbo].[Clientes] ([Cl_Cve_Cliente], [Cl_Nombre], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta]) VALUES (N'0000000004', N'cesar chab', N'AC', CAST(N'2020-12-02T22:26:43.057' AS DateTime), N'admin')
GO
INSERT [dbo].[Clientes] ([Cl_Cve_Cliente], [Cl_Nombre], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta]) VALUES (N'0000000005', N'cesar demostracion', N'AC', CAST(N'2020-12-02T22:47:37.527' AS DateTime), N'admin')
GO
SET IDENTITY_INSERT [dbo].[Conceptos] ON 
GO
INSERT [dbo].[Conceptos] ([id], [descripcion], [precio_gasolina], [precio_diesel], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta], [Fecha_Ult_Modif], [Oper_Ult_Modif], [Fecha_Baja], [Oper_Baja]) VALUES (1, N'Demo', CAST(100.000 AS Decimal(18, 3)), CAST(150.000 AS Decimal(18, 3)), N'AC', CAST(N'2020-12-02T22:41:45.727' AS DateTime), N'admin', CAST(N'2020-12-02T22:41:45.727' AS DateTime), N'admin', NULL, N'')
GO
INSERT [dbo].[Conceptos] ([id], [descripcion], [precio_gasolina], [precio_diesel], [Es_Cve_Estado], [Fecha_Alta], [Oper_Alta], [Fecha_Ult_Modif], [Oper_Ult_Modif], [Fecha_Baja], [Oper_Baja]) VALUES (2, N'Demo 2', CAST(250.000 AS Decimal(18, 3)), CAST(300.000 AS Decimal(18, 3)), N'AC', CAST(N'2020-12-02T22:41:53.300' AS DateTime), N'admin', CAST(N'2020-12-02T22:41:53.300' AS DateTime), N'admin', NULL, N'')
GO
SET IDENTITY_INSERT [dbo].[Conceptos] OFF
GO
INSERT [dbo].[Empresa] ([Em_Cve_Empresa], [Em_Nombre], [Em_Calle], [Em_Cruzamiento], [Em_Numext], [Em_Numint], [Em_Colonia], [Em_Localidad], [Em_Municipio], [Em_Ciudad], [Em_Estado], [Em_Pais], [Em_CodPost], [Em_Telefono], [Em_RFC], [Em_Email], [Em_Regimen_Fiscal], [Em_Cve_Regimen_Fiscal], [Em_Logo], [Em_Maneja_Impuesto], [Em_Impuesto], [Em_Impuesto_porcentaje], [Em_Moneda], [Modo_Busqueda], [Ruta_Backup], [Ultimo_backup], [Frecuencia_backup], [Estado_backup], [Redondeo_Total], [Fecha_Alta], [Oper_Alta], [Fecha_Ult_Modif], [Oper_Ult_Modif], [Fecha_Baja], [Oper_Baja], [Es_Cve_Estado]) VALUES (N'0001', N'TEST', N'CENTRO', N'5X10', N'103', N'0', N'CIELO ALTO', N'KANASIN', N'KANASIN', N'KANASIN', N'YUCATAN', N'MEXICO', N'97370', N'9995048783', N'CAUC890127FU2', N'', N'General de Ley Personas Morales', N'601', NULL, N'', N'', CAST(0 AS Decimal(18, 0)), N'', N'', N'C:\BD', CAST(N'2020-12-02T21:37:18.947' AS DateTime), 1, N'PENDIENTE', N'', CAST(N'2020-12-02T21:37:18.947' AS DateTime), N'CONFIG', CAST(N'2020-12-02T21:37:18.947' AS DateTime), N'CONFIG', NULL, NULL, N'AC')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'0', N'(TODOS)')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AB', N'ABIERTO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AC', N'ACTIVO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'ACT', N'ACTUALIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AD', N'APLICADO Y DEPOSITADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AP', N'APLICADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'APT', N'APARTADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AR', N'ACUSE DE RECIBO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AS', N'ASIGNADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AU', N'AUTORIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'BA', N'BAJA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'BO', N'BACKORDER')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'BS', N'BLOQUEADO POR SISTEMA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CA', N'CANCELADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CE', N'CERRADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CO', N'CONFIRMADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CON', N'CONSOLIDADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CT', N'COTIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'DE', N'DEVUELTO TOTALMENTE')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'DP', N'DEPOSITADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'DS', N'DESACTIVADO POR SISTEMA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'EM', N'EMITIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'EN', N'ENVIADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'EP', N'EN PROCESO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FA', N'FACTURADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FC', N'FIN DE CONSIGNA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FI', N'FIRME')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FN', N'FINALIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FR', N'FACTURADO CON REMISION')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'IM', N'IMPRESO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'LI', N'LIBERADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PA', N'PAGADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PAU', N'PRE-AUTORIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PD', N'PENDIENTE')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PE', N'PEDIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PR', N'PROGRAMADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PXA', N'PENDIENTE POR AUTORIZAR')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PXC', N'PENDIENTE POR CANCELAR')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RA', N'ACUSE DE RECIBO CON RECHAZO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RCP', N'RECIBIDO PARCIAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RCT', N'RECIBIDO TOTAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RE', N'REVISADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RF', N'RE-FACTURACION')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RM', N'REMISION')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RMP', N'REMISIONADO PARCIAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RMT', N'REMISIONADO TOTAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RP', N'REPROGRAMADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RZ', N'RECHAZO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'SU', N'SUSPENDIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'SUR', N'SURTIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'TE', N'TERMINADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'TR', N'TRANSITO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'UN', N'UNIDA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'VA', N'VENTA APARTADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'VC', N'VENCIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'VE', N'VENTA')
GO
INSERT [dbo].[Folios] ([Tipo], [Folio]) VALUES (N'Clientes', CAST(5 AS Decimal(18, 0)))
GO
INSERT [dbo].[Folios] ([Tipo], [Folio]) VALUES (N'Remision', CAST(2 AS Decimal(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[Licencias] ON 
GO
INSERT [dbo].[Licencias] ([Id], [Descripcion], [FI], [FF], [FA], [Serial_PC], [Es_Cve_Estado]) VALUES (1, N'Licencia de prueba gratuita (30 días)', N'uSSSnHCLd1uOTQLWf7ov58vT/0ExWFFt7RYlBeKDUvg=', N'yb/teRDPng66om+rHe9Gc8vT/0ExWFFt7RYlBeKDUvg=', N'uSSSnHCLd1uOTQLWf7ov58vT/0ExWFFt7RYlBeKDUvg=', N'LXK4rzOvQRSPEULLOqXcc8S5jhw8t7645MReDc+eR08=', N'PE')
GO
SET IDENTITY_INSERT [dbo].[Licencias] OFF
GO
SET IDENTITY_INSERT [dbo].[Operadores] ON 
GO
INSERT [dbo].[Operadores] ([Id], [Nombre_completo], [Login], [Password], [Icono], [Nombre_Icono], [Correo], [Rol], [Es_Cve_Estado]) VALUES (1, N'CESAR CHAB', N'admin', N'MTIzNDU ', 0xFFD8FFE000104A46494600010101004800480000FFE1005A4578696600004D4D002A00000008000503010005000000010000004A030300010000000100000000511000010000000101000000511100040000000100000B13511200040000000100000B1300000000000186A00000B18FFFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC00011080080008003012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F9FE8A28A0028A0024E00C93D05769A17825A554B9D57722100ADBA9C37FC0BD38EC39E7B6315B51A13ACED10395B1D36F75394C7676EF2B0EA47017AF52781D0F5AEB2C7E1FB921B50BC0067948064918FEF1E9CFB1AEDA1822B6896282248A35E888A001F80A92BD5A597D38EB3D595639F87C17A24512A3DB3CCC3ABBCAC09FC881FA5697F62E95FF0040CB3FFBF0BFE157A8AEB8D1A71DA280A3FD8BA57FD032CFFEFC2FF8566CDE0BD12588A25B3C2C7A3A4AC48FCC91FA57414512A34E5BC501C2DF7C3F704B69F7808C8C24E3040C73F30EBCFB0AE4EFB4DBDD32511DE5BBC2C7A12386E9D08E0F51D2BD9AA39A18AE2268A789258DBAA3A820FE06B92AE5F4E5AC346163C4E8AED35DF04344AF73A565D14166B76396FF00809EFC763CF1DF38AE2C820E08C11D457955A84E8BB4890A28A2B100A0024E00C93D0515DA78274212B7F6ADCA2B229C40AC3F887F1FE1D07BE7A6056D428BAD3E5406C7863C30BA3A8BABA0AD7CC31C1C8881EA01EE7D4FE038CE7A4A28AFA1A74E34E3CB1D8A0A28A2B4185145140051451400514514005737E27F0C0D617ED56A156F9401C9C0940E809EC7D0FE078E9D25159D4A71A91E596C23C3C820E08C11D4515DA78DF42589BFB56D915518E275507EF1E8FF008F43EF8F535C5D7CF57A2E8CF95925AD36C64D4F5282CE338695B05BFBA3A93F8004E2BD8A1863B78238225DB1C6A110673800605713F0FEC4992EB50607000850E4609EADEFC7CBF99AEEABD5CBE972D3E77BB290514515E80C28A28A002BA9D37C01AE6A30ACC638AD636195FB431048FA0048FC715A3F0DF438AFAFA6D4AE103A5A90B1291C6F3CE7F01FCEBD56BCDC5E35D3972404D9E31A978035CD3A16984715D46A32DF6762481F4201FCB35CBD7D1F5E53F123438AC6FE1D4ADD0225D12255038DE3BFE23F95184C6BA92E4982670D451457A430A28A280239E18EE20920957747229475CE3208C1AF1DD4AC64D33529ECE439689B01BFBC3A83ED91838AF66AE1BE20D936FB3BF1B8AED303648C0EACB81D79CB7E55E7E614B9A9F3ADD099B3E0B8638BC336EE8B86959DDCE7A9DC57F9015D0551D17FE405A7FF00D7B47FFA08ABD5D7463CB4E2BC8028A28AD46145145007A7FC2DBA8CE9F7D6991E62CA25C7A8231FFB2FEB5E815F3FE8FABDD689A9477B68C37AF0CA7A3AF706BD5B4DF881A1DF42A679CD9CD8F9A3941C7E0C38FE55E2E370D3F68E71574C968EAABCFF00E29DD462C2C2D323CC694CB8F40063FAFE95ADA97C40D0EC616304E6F26C7CB1C40E3F163C7F3AF29D6357BAD6F5292F6ED86F6E1547445EC07B5182C34FDA29C9592048A1451457B45051451400573FE3386397C3370EEB968991D0E7A1DC17F913F9D7415475AFF9016A1FF5ED27FE826B2AD1E6A725E420D17FE405A7FF00D7B47FFA08ABD5CFF82E68E5F0CDBA2365A2674718E87716FE4C2BA0A28CB9A9C5F900514515A8C2A48209AE6748208DE595CE1510649351D7ABFC37D122B6D27FB564406E2E4908C47DD4071C7D483FA56188ACA8C39988E6AD3E1A6B73C41E692D6DC9FE0772587E408FD6AC7FC2AED53FE7FACFF36FF0AF55A2BC87985615CF2AFF00855DAA7FCFF59FE6DFE155EEFE1A6B76F117864B5B823F811C863F9803F5AF5DA28598560B9F3A4F04B6D3BC33C6F1CA870C8E3041FA5475EADF123448AE349FED68D00B8B62048C07DE4271CFD091FAD794D7AF87ACAB43990D0514515B8C2A8EB5FF00202D43FEBDA4FF00D04D5EAE7FC6734717866E11DB0D2B2220C753B837F207F2ACAB4B969C9F908C5F87F7C44975A7B138204C830300F46E7AF3F2FE46BBAAF19D36FA4D335282F2319689B257FBC3A11ED91915EC50CD1DC411CF136E8E450E8D8C6411915C997D5E6A7C8F7408928A28AF40615EE5E0CFF91434DFFAE67FF4235E1B5D6E97F10352D2B4C82C61B5B478E15DAACEAD93CE79C1AE3C6D19D58250EE267B1515E51FF0B4757FF9F3B1FF00BE5FFF008AA3FE168EAFFF003E763FF7CBFF00F155E6FD42BF6158F57A2BCA3FE168EAFF00F3E763FF007CBFFF001547FC2D1D5FFE7CEC7FEF97FF00E2A8FA857EC163BAF19FFC8A1A97FD731FFA10AF0DAEB754F881A96ABA64F6335ADA24732ED66456C8E73C64D7255E960A8CE941A9F71A0A28A2BB0615C37C41BD6DF67603701833B02A307AAAE0F5E30DF9FE5DB4D325BC124F2B6D8E352EE719C00324D78E6A57D26A7A94F7928C34AD903FBA3A01F80C0AF3F30ABCB4F93AB132AD769E08D75626FECAB9755463981989FBC7F83F1EA3DF3EA2B8BA0120E41C11D0D79542B3A33E6449EE145737E18F138D617ECB74556F9413C0C0940EA40EC7D47E238E9D257D0D3A91A91E68EC50514515A0C28A28A0028A28A0028A28A0028A2B99F1278AA3D2E336D64E925E9E09E1962FAFBFB7E7E862A548D38F3CB615CC8F1BEBAB338D2AD9C32236E9D949E587F07A71D4F5E71D315C651457CE56ACEACDCD921451456400090720E08E86BB4D0BC6ED12A5B6AB97450156E1465BFE043BF1DC73C77CE6B8BA2B6A35E745DE207B6433C57112CB04A92C6DD1D18303F88A92BC66C752BDD3253259DC3C2C7A81C86EBD41E0F53D6BADB2F882DBF17F64BB49277DB923031C0DAC79E7DEBD5A598539693D19573B9A2B9F87C67A24B12BBDCBC4C7AA3C4C48FC811FAD697F6D695FF413B3FF00BFEBFE35D71AD4E5B49017A8AA3FDB5A57FD04ECFF00EFFAFF008D66CDE33D122899D2E5E561D1122604FE600FD68956A71DE480E829934D15BC4659E548A35EAEEC140EDD4D7137BF105B762C2C9701810F7073918E46D53C73EFFF00D6E4AFB52BDD4E512DE5C3CCC3A03C05E9D00E0741D2B92AE614E3F06AC2E753AE78DDA647B6D28322302AD70C30DD7F87D38EE79E7A022B8CA28AF26B569D5779B2428A28AC80FFD9, N'', N'', N'Administrador', N'AC')
GO
SET IDENTITY_INSERT [dbo].[Operadores] OFF
GO
SET IDENTITY_INSERT [dbo].[RegimenFiscal] ON 
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (1, N'601', N'General de Ley Personas Morales', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (2, N'603', N'Personas Morales con Fines no Lucrativos', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (3, N'605', N'Sueldos y Salarios e Ingresos Asimilados a Salarios', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (4, N'606', N'Arrendamiento', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (5, N'608', N'Demás ingresos', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (6, N'609', N'Consolidación', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (7, N'610', N'Residentes en el Extranjero sin Establecimiento Permanente en México', N'Si', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (8, N'611', N'Ingresos por Dividendos (socios y accionistas)', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (9, N'612', N'Personas FÍsicas con Actividades Empresariales y Profesionales', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (10, N'614', N'Ingresos por intereses', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (11, N'616', N'Sin obligaciones fiscales', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (12, N'620', N'Sociedades Cooperativas de Producción que optan por diferir sus ingresos', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (13, N'621', N'Incorporación Fiscal', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (14, N'622', N'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras', N'Si', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (15, N'623', N'Opcional para Grupos de Sociedades', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (16, N'624', N'Coordinados', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (17, N'628', N'Hidrocarburos', N'No', N'Si', CAST(N'2020-01-01' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (18, N'607', N'Régimen de Enajenación o Adquisición de Bienes', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (19, N'629', N'De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales', N'Si', N'No', CAST(N'2020-01-01' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (20, N'630', N'Enajenación de acciones en bolsa de valores', N'Si', N'No', CAST(N'2020-01-01' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (21, N'615', N'Régimen de los ingresos por obtención de premios', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[RegimenFiscal] OFF
GO
INSERT [dbo].[Remision_Detalle] ([Rm_Folio], [Rm_ID], [Rm_Fecha], [Rm_Concepto_Id], [Rm_Concepto], [Rm_Cantidad], [Rm_Precio], [Rm_Importe], [Oper_Alta], [Fecha_Alta], [Oper_Ult_Modif], [Fecha_Ult_Modif], [Oper_Baja], [Fecha_Baja], [Es_Cve_Estado]) VALUES (N'0000000001', N'0001', CAST(N'2020-12-02T22:47:37.577' AS DateTime), 2, N'Demo 2', CAST(50.000000000 AS Decimal(18, 9)), CAST(250.000000000 AS Decimal(18, 9)), CAST(12500.000000000 AS Decimal(18, 9)), N'admin', CAST(N'2020-12-02T22:47:37.577' AS DateTime), N'admin', CAST(N'2020-12-02T22:47:37.577' AS DateTime), N'', NULL, N'AC')
GO
INSERT [dbo].[Remision_Detalle] ([Rm_Folio], [Rm_ID], [Rm_Fecha], [Rm_Concepto_Id], [Rm_Concepto], [Rm_Cantidad], [Rm_Precio], [Rm_Importe], [Oper_Alta], [Fecha_Alta], [Oper_Ult_Modif], [Fecha_Ult_Modif], [Oper_Baja], [Fecha_Baja], [Es_Cve_Estado]) VALUES (N'0000000001', N'0002', CAST(N'2020-12-02T22:47:37.580' AS DateTime), 1, N'Demo', CAST(10.000000000 AS Decimal(18, 9)), CAST(150.000000000 AS Decimal(18, 9)), CAST(1500.000000000 AS Decimal(18, 9)), N'admin', CAST(N'2020-12-02T22:47:37.580' AS DateTime), N'admin', CAST(N'2020-12-02T22:47:37.580' AS DateTime), N'', NULL, N'AC')
GO
INSERT [dbo].[Remision_Detalle] ([Rm_Folio], [Rm_ID], [Rm_Fecha], [Rm_Concepto_Id], [Rm_Concepto], [Rm_Cantidad], [Rm_Precio], [Rm_Importe], [Oper_Alta], [Fecha_Alta], [Oper_Ult_Modif], [Fecha_Ult_Modif], [Oper_Baja], [Fecha_Baja], [Es_Cve_Estado]) VALUES (N'0000000002', N'0001', CAST(N'2020-12-02T22:54:44.963' AS DateTime), 2, N'Demo 2', CAST(100.000000000 AS Decimal(18, 9)), CAST(250.000000000 AS Decimal(18, 9)), CAST(25000.000000000 AS Decimal(18, 9)), N'admin', CAST(N'2020-12-02T22:54:44.963' AS DateTime), N'admin', CAST(N'2020-12-02T22:54:44.963' AS DateTime), N'', NULL, N'AC')
GO
INSERT [dbo].[Remision_Encabezado] ([Rm_Folio], [Rm_Fecha], [Rm_Referencia], [Rm_Comentario], [Vn_Cve_Vendedor], [Cl_Cve_Cliente], [Rm_Importe], [Oper_Alta], [Fecha_Alta], [Oper_Ult_Modif], [Fecha_Ult_Modif], [Oper_Baja], [Fecha_Baja], [Es_Cve_Estado]) VALUES (N'0000000001', CAST(N'2020-12-02T22:47:37.573' AS DateTime), N'', N'terminos y condiciones', NULL, N'0000000005', CAST(14000.000000000 AS Decimal(18, 9)), N'admin', CAST(N'2020-12-02T22:47:37.573' AS DateTime), N'admin', CAST(N'2020-12-02T22:47:37.573' AS DateTime), N'', NULL, N'AC')
GO
INSERT [dbo].[Remision_Encabezado] ([Rm_Folio], [Rm_Fecha], [Rm_Referencia], [Rm_Comentario], [Vn_Cve_Vendedor], [Cl_Cve_Cliente], [Rm_Importe], [Oper_Alta], [Fecha_Alta], [Oper_Ult_Modif], [Fecha_Ult_Modif], [Oper_Baja], [Fecha_Baja], [Es_Cve_Estado]) VALUES (N'0000000002', CAST(N'2020-12-02T22:54:44.963' AS DateTime), N'', N'asdsadasd', NULL, N'0000000001', CAST(25000.000000000 AS Decimal(18, 9)), N'admin', CAST(N'2020-12-02T22:54:44.963' AS DateTime), N'admin', CAST(N'2020-12-02T22:54:44.963' AS DateTime), N'', NULL, N'AC')
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UNQ1_Empresas_RFC]    Script Date: 02/12/2020 10:59:32 p. m. ******/
ALTER TABLE [dbo].[Empresa] ADD  CONSTRAINT [UNQ1_Empresas_RFC] UNIQUE NONCLUSTERED 
(
	[Em_RFC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Operadores]    Script Date: 02/12/2020 10:59:32 p. m. ******/
ALTER TABLE [dbo].[Operadores] ADD  CONSTRAINT [IX_Operadores] UNIQUE NONCLUSTERED 
(
	[Login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [PK_Remision]    Script Date: 02/12/2020 10:59:32 p. m. ******/
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [PK_Remision] PRIMARY KEY NONCLUSTERED 
(
	[Rm_Folio] ASC,
	[Rm_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [PK_Remision_Encabezado]    Script Date: 02/12/2020 10:59:32 p. m. ******/
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [PK_Remision_Encabezado] PRIMARY KEY NONCLUSTERED 
(
	[Rm_Folio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_Fecha_Alta]  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_precio_gasolina]  DEFAULT ((0)) FOR [precio_gasolina]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_precio_diesel]  DEFAULT ((0)) FOR [precio_diesel]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Fecha_Alta]  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Fecha_Ult_Modif]  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Oper_Ult_Modif]  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Oper_Baja]  DEFAULT ('') FOR [Oper_Baja]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('NO') FOR [Em_Maneja_Impuesto]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('TECLADO') FOR [Modo_Busqueda]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT (getdate()) FOR [Ultimo_backup]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ((1)) FOR [Frecuencia_backup]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('PENDIENTE') FOR [Estado_backup]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('NO') FOR [Redondeo_Total]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Licencias] ADD  CONSTRAINT [DF_Licencias_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Operadores] ADD  CONSTRAINT [DF_Operadores_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  DEFAULT (getdate()) FOR [Rm_Fecha]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Detalle_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Detalle_Oper_Ult_Modif]  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Oper_Baja]  DEFAULT ('') FOR [Oper_Baja]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Es_Cve_Estado]  DEFAULT ('AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Fecha_Alta]  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Oper_Ult_Modif]  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Fecha_Ult_Modif]  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Oper_Baja]  DEFAULT ('') FOR [Oper_Baja]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Es_Cve_Estado]  DEFAULT ('AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Estado]
GO
ALTER TABLE [dbo].[Conceptos]  WITH CHECK ADD  CONSTRAINT [FK_Conceptos_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Conceptos] CHECK CONSTRAINT [FK_Conceptos_Estado]
GO
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_Empresas_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_Empresas_Estado]
GO
ALTER TABLE [dbo].[Licencias]  WITH CHECK ADD  CONSTRAINT [FK_Licencias_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Licencias] CHECK CONSTRAINT [FK_Licencias_Estado]
GO
ALTER TABLE [dbo].[Operadores]  WITH CHECK ADD  CONSTRAINT [FK_Operadores_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Operadores] CHECK CONSTRAINT [FK_Operadores_Estado]
GO
ALTER TABLE [dbo].[Remision_Detalle]  WITH CHECK ADD  CONSTRAINT [RL_Estado_Remision] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Remision_Detalle] CHECK CONSTRAINT [RL_Estado_Remision]
GO
ALTER TABLE [dbo].[Remision_Detalle]  WITH CHECK ADD  CONSTRAINT [RL_Remision_Encabezado_Remision] FOREIGN KEY([Rm_Folio])
REFERENCES [dbo].[Remision_Encabezado] ([Rm_Folio])
GO
ALTER TABLE [dbo].[Remision_Detalle] CHECK CONSTRAINT [RL_Remision_Encabezado_Remision]
GO
ALTER TABLE [dbo].[Remision_Detalle]  WITH CHECK ADD  CONSTRAINT [RL_Rm_Concepto_Id_Remision] FOREIGN KEY([Rm_Concepto_Id])
REFERENCES [dbo].[Conceptos] ([id])
GO
ALTER TABLE [dbo].[Remision_Detalle] CHECK CONSTRAINT [RL_Rm_Concepto_Id_Remision]
GO
ALTER TABLE [dbo].[Remision_Encabezado]  WITH CHECK ADD  CONSTRAINT [RL_Estado_Remision_Encabezado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Remision_Encabezado] CHECK CONSTRAINT [RL_Estado_Remision_Encabezado]
GO
/****** Object:  StoredProcedure [dbo].[Cliente_IU]    Script Date: 02/12/2020 10:59:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.S.U. Cesar Chab
-- Create date: 02/12/2020
-- Description: Alta y mantenimiento de clientes
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_IU]
	@Cl_Cve_Cliente NVARCHAR(10),
	@Cl_Nombre NVARCHAR(150),
	@Oper_Alta NVARCHAR(35),
	@Tipo_Movimiento INT --0 Nuevo, 1 Actualizar, 2 Baja, 3 Activar	
AS
BEGIN
	BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @Result int 
		DECLARE @Mensaje_Retorno INT = 1

		--Obtenemos el folio consecutivo
		DECLARE @Folio nvarchar(10);
		SET @Folio = (SELECT dbo.CIntToChar(CASE WHEN Folio IS NULL THEN 0 ELSE Folio END + 1,10) AS FOLIO FROM Folios WHERE Tipo='Clientes')

		IF @Tipo_Movimiento = 0
		BEGIN
			INSERT INTO [dbo].[Clientes]
					   ([Cl_Cve_Cliente]
					   ,[Cl_Nombre]
					   ,[Es_Cve_Estado]
					   ,[Fecha_Alta]
					   ,[Oper_Alta])
				 VALUES
					   (@Folio
					   ,@Cl_Nombre
					   ,'AC'
					   ,GETDATE	()
					   ,@Oper_Alta)
		
				UPDATE Folios SET Folio = [dbo].[Get_ValueOf](@Folio) WHERE Tipo = 'Clientes';

			SET @Message = 'Cliente guardado correctamente con el folio #' + @Folio 
			SET @Result = 1		

		END
		ELSE IF @Tipo_Movimiento = 1
		BEGIN
			IF (EXISTS(SELECT Cl_Cve_Cliente FROM dbo.Clientes WHERE (Cl_Cve_Cliente = @Cl_Cve_Cliente))) 
			BEGIN
				
				UPDATE [dbo].[Clientes]
				   SET 
					   [Cl_Nombre] = @Cl_Nombre
				 WHERE [Cl_Cve_Cliente] = @Cl_Cve_Cliente				

				 SET @Message = 'Cliente guardado correctamente';
				 SET @Result = 1
				 SET @Folio = @Cl_Cve_Cliente
			END
		END
		ELSE IF @Tipo_Movimiento = 2
		BEGIN
			IF (EXISTS(SELECT Cl_Cve_Cliente FROM Clientes WHERE (Cl_Cve_Cliente = @Cl_Cve_Cliente))) 
			BEGIN
				UPDATE dbo.Clientes SET Es_Cve_Estado='BA' 
				WHERE Cl_Cve_Cliente=@Cl_Cve_Cliente

				SET @Message = 'Clinete dado de baja correctamente con el folio #' + @Cl_Cve_Cliente
				SET @Result = 1
				SET @Folio = @Cl_Cve_Cliente
			END
		END
		ELSE IF @Tipo_Movimiento = 3
		BEGIN
			IF (EXISTS(SELECT Cl_Cve_Cliente FROM Clientes WHERE (Cl_Cve_Cliente = @Cl_Cve_Cliente))) 
			BEGIN
				UPDATE dbo.Clientes SET Es_Cve_Estado='AC' 
				WHERE Cl_Cve_Cliente=@Cl_Cve_Cliente

				SET @Message = 'Clinete activado correctamente con el folio #' + @Cl_Cve_Cliente
				SET @Result = 1
				SET @Folio = @Cl_Cve_Cliente
			END
		END		
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;           
	IF @Mensaje_Retorno = 1 
	BEGIN
		SELECT @Message, @Result, @Folio
	END	
END

GO
/****** Object:  StoredProcedure [dbo].[Empresas_IU]    Script Date: 02/12/2020 10:59:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.S.U. Cesar Chab
-- Create date: 29/11/2020 07:28:00 a.m
-- Description: Alta y mantenimiento del catalogo de empresas
-- =============================================
CREATE PROCEDURE [dbo].[Empresas_IU]
	@Em_Cve_Empresa NVARCHAR(4),
    @Em_Nombre NVARCHAR(60),
    @Em_Calle NVARCHAR(50),
    @Em_Cruzamiento NVARCHAR(50),
    @Em_Numext NVARCHAR(10),
    @Em_Numint NVARCHAR(10),
    @Em_Colonia NVARCHAR(50),
    @Em_Localidad NVARCHAR(60),
    @Em_Municipio NVARCHAR(60),
    @Em_Ciudad NVARCHAR(60),
    @Em_Estado NVARCHAR(60),
    @Em_Pais NVARCHAR(50),
    @Em_CodPost NVARCHAR(5),
    @Em_Telefono NVARCHAR(50),
    @Em_RFC NVARCHAR(13),
    @Em_Email NVARCHAR(50),
    @Em_Regimen_Fiscal NVARCHAR(250),
    @Em_Cve_Regimen_Fiscal NVARCHAR(3),
	@Em_Logo IMAGE,    
	@Em_Maneja_Impuesto NVARCHAR(10),
	@Em_Impuesto NVARCHAR(50),
	@Em_Impuesto_porcentaje decimal(18,0),
	@Em_Moneda NVARCHAR(50),
	@Modo_Busqueda NVARCHAR(50),
	@Ruta_Backup NVARCHAR(255),
	@Ultimo_Backup DATETIME,
	@Frecuencia_Backup INT,
	@Redondeo_Total NVARCHAR(4),
    @Oper_Alta NVARCHAR(35),
	@Tipo_Movimiento INT, --0 Nuevo, 1 Actualizar, 2 Baja, 3 Activar	
	@Crea_Operador bit = 0,
	@Nombre nvarchar(100),
	@Login nvarchar(50),
	@Password nvarchar(50),
	@Icono image,
	@Correo nvarchar(100),
	@Rol nvarchar(100),
	--Active Licence 30 dias
	@Crea_Licencia bit = 0,
	@Lic_Descripcion nvarchar(150),
	@Lic_FI NVARCHAR(255),
	@Lic_FF NVARCHAR(255),
	@LIC_FA NVARCHAR(255),
	@SerialPC varchar(255)
	
AS
BEGIN

BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @MessageCaja NVARCHAR(MAX)
		DECLARE @MessageOperador NVARCHAR(MAX)
		DECLARE @Result int 

		--Obtenemos el folio consecutivo
		DECLARE @Folio nvarchar(10) 

		IF @Tipo_Movimiento = 0
		BEGIN
			SET @Folio = (SELECT 
							dbo.CIntToChar(CASE WHEN MAX(Em_Cve_Empresa) IS NULL THEN 0 ELSE MAX(Em_Cve_Empresa) END + 1, 4) AS Folio 
							FROM dbo.Empresa);

			INSERT INTO [dbo].[Empresa]
					   ([Em_Cve_Empresa]
					   ,[Em_Nombre]
					   ,[Em_Calle]
					   ,[Em_Cruzamiento]
					   ,[Em_Numext]
					   ,[Em_Numint]
					   ,[Em_Colonia]
					   ,[Em_Localidad]
					   ,[Em_Municipio]
					   ,[Em_Ciudad]
					   ,[Em_Estado]
					   ,[Em_Pais]
					   ,[Em_CodPost]
					   ,[Em_Telefono]
					   ,[Em_RFC]
					   ,[Em_Email]
					   ,[Em_Regimen_Fiscal]
					   ,[Em_Cve_Regimen_Fiscal]
					   ,[Em_Logo]
					   ,[Em_Maneja_Impuesto]
					   ,[Em_Impuesto]
					   ,[Em_Impuesto_porcentaje]
					   ,[Em_Moneda]
					   ,[Modo_Busqueda]
					   ,[Ruta_Backup]
					   ,[Ultimo_backup]
					   ,[Frecuencia_backup]
					   ,[Estado_backup]
					   ,[Redondeo_Total]					  
					   ,[Oper_Alta]					   
					   ,[Oper_Ult_Modif]
				)
				 VALUES
					   (@Folio
					   ,@Em_Nombre
					   ,@Em_Calle
					   ,@Em_Cruzamiento
					   ,@Em_Numext
					   ,@Em_Numint
					   ,@Em_Colonia
					   ,@Em_Localidad
					   ,@Em_Municipio
					   ,@Em_Ciudad
					   ,@Em_Estado
					   ,@Em_Pais
					   ,@Em_CodPost
					   ,@Em_Telefono
					   ,@Em_RFC
					   ,@Em_Email
					   ,@Em_Regimen_Fiscal
					   ,@Em_Cve_Regimen_Fiscal
					   ,@Em_Logo
					   ,@Em_Maneja_Impuesto
					   ,@Em_Impuesto
					   ,@Em_Impuesto_porcentaje
					   ,@Em_Moneda
					   ,@Modo_Busqueda
					   ,@Ruta_Backup
					   ,GETDATE()
					   ,@Frecuencia_backup
					   ,'PENDIENTE'
					   ,@Redondeo_Total
					   ,@Oper_Alta
					   ,@Oper_Alta
					  );
			
			IF @Crea_Operador = 1 
			BEGIN
				EXEC @MessageOperador = dbo.Operadores_IU 0, @Nombre, @Login, @Password, @Icono, @Correo, @Rol, 0,0,0,0,1;
			END

			IF @Crea_Licencia = 1
			BEGIN

				INSERT INTO [dbo].[Licencias]
						   ([Descripcion]
						   ,[FI]
						   ,[FF]
						   ,[FA]
						   ,[Serial_PC]
						   ,[Es_Cve_Estado])
					 VALUES
						   (@Lic_Descripcion
						   ,@Lic_FI
						   ,@Lic_FF
						   ,@LIC_FA
						   ,@SerialPC
						   ,'PE');

			END
		
			SET @Message = 'Empresa guardado correctamente con el folio #' + @Folio 
			SET @Result = 1		

		END
		ELSE IF @Tipo_Movimiento = 1
		BEGIN
			IF (EXISTS(SELECT Em_Cve_Empresa FROM Empresa WHERE (Em_Cve_Empresa = @Em_Cve_Empresa))) 
			BEGIN
				UPDATE [dbo].[Empresa]
				   SET [Em_Nombre] = @Em_Nombre
					  ,[Em_Calle] = @Em_Calle
					  ,[Em_Cruzamiento] = @Em_Cruzamiento
					  ,[Em_Numext] = @Em_Numext
					  ,[Em_Numint] = @Em_Numint
					  ,[Em_Colonia] = @Em_Colonia
					  ,[Em_Localidad] = @Em_Localidad
					  ,[Em_Municipio] = @Em_Municipio
					  ,[Em_Ciudad] = @Em_Ciudad
					  ,[Em_Estado] = @Em_Estado
					  ,[Em_Pais] = @Em_Pais
					  ,[Em_CodPost] = @Em_CodPost
					  ,[Em_Telefono] = @Em_Telefono
					  ,[Em_RFC] = @Em_RFC
					  ,[Em_Email] = @Em_Email
					  ,[Em_Regimen_Fiscal] = @Em_Regimen_Fiscal
					  ,[Em_Cve_Regimen_Fiscal] = @Em_Cve_Regimen_Fiscal
					  ,[Em_Logo] = @Em_Logo
					  ,[Em_Maneja_Impuesto] = @Em_Maneja_Impuesto
					  ,[Em_Impuesto] = @Em_Impuesto
					  ,[Em_Impuesto_porcentaje] = @Em_Impuesto_porcentaje
					  ,[Em_Moneda] = @Em_Moneda
					  ,[Modo_Busqueda] = @Modo_Busqueda
					  ,[Ruta_Backup] = @Ruta_Backup
					  --,[Ultimo_backup] = @Ultimo_backup
					  ,[Frecuencia_backup] = @Frecuencia_backup
					  --,[Estado_backup] = @Estado_backup
					  ,[Redondeo_Total] = @Redondeo_Total
					  ,[Fecha_Ult_Modif] = GETDATE()
					  ,[Oper_Ult_Modif] = @Oper_Alta
				 WHERE [Em_Cve_Empresa] = @Em_Cve_Empresa
				 SET @Message = 'Empresa guardado correctamente';
				 SET @Result = 1
				 SET @Folio = @Em_Cve_Empresa
			END
		END
		ELSE IF @Tipo_Movimiento = 2
		BEGIN
			IF (EXISTS(SELECT Em_Cve_Empresa FROM Empresa WHERE (Em_Cve_Empresa = @Em_Cve_Empresa))) 
			BEGIN
				UPDATE dbo.Empresa SET Es_Cve_Estado='BA', Oper_Ult_Modif=@Oper_Alta, Fecha_Ult_Modif=GETDATE(), Oper_Baja=@Oper_Alta, Fecha_Baja=GETDATE()
				WHERE Em_Cve_Empresa=@Em_Cve_Empresa

				SET @Message = 'Empresa inactivamento correctamente con el folio #' + @Em_Cve_Empresa
				SET @Result = 1
				SET @Folio = @Em_Cve_Empresa
			END
		END
		ELSE IF @Tipo_Movimiento = 3
		BEGIN
			IF (EXISTS(SELECT Em_Cve_Empresa FROM Empresa WHERE (Em_Cve_Empresa = @Em_Cve_Empresa))) 
			BEGIN
				UPDATE dbo.Empresa SET Es_Cve_Estado='AC', Oper_Ult_Modif=@Oper_Alta, Fecha_Ult_Modif=GETDATE(), Oper_Baja='', Fecha_Baja=NULL
				WHERE Em_Cve_Empresa=@Em_Cve_Empresa

				SET @Message = 'Empresa activado correctamente con el folio #' + @Em_Cve_Empresa
				SET @Result = 1
				SET @Folio = @Em_Cve_Empresa
			END
		END		
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;           
	SELECT @Message, @Result, @Folio
END
GO
/****** Object:  StoredProcedure [dbo].[Operadores_IU]    Script Date: 02/12/2020 10:59:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.S.U. Cesar Chab
-- Create date: 29/11/2020 14:44:00 p.m
-- Description:	Alta y mantenimiento de usuarios del sistema
-- =============================================
CREATE PROCEDURE [dbo].[Operadores_IU]
	@Id int,
	@Nombre nvarchar(100),
	@Login nvarchar(50),
	@Password nvarchar(50),
	@Icono image,
	@Correo nvarchar(100),
	@Rol nvarchar(100),
	@Tipo_Movimiento int,
	@Mensaje_Retorno int = 0,
	@Cambiar_Password int = 0,
	@Cambiar_Imagen int = 0,
	@Permiso int = 0
	
AS
BEGIN
	BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @Result int

		--Obtenemos el folio consecutivoA
		DECLARE @Folio nvarchar(10) 

		IF @Tipo_Movimiento = 0
		BEGIN

			INSERT INTO [dbo].[Operadores]
				([Nombre_completo]
				,[Login]
				,[Password]
				,[Icono]
				,[Nombre_Icono]
				,[Correo]
				,[Rol]
				,[Es_Cve_Estado])
			VALUES
				(@Nombre
				,@Login
				,@Password
				,@Icono
				,''
				,@Correo
				,@Rol
				,'AC');
			SET @Folio = (SELECT @@IDENTITY)
			
			/*--Insert Permisos admin
			IF @Permiso = 1 
			BEGIN
				INSERT INTO [dbo].[Permisos]
						   ([Usuario_Id]
						   ,[ventas]
						   ,[aplicar_descuentos]
						   ,[Devoluciones]
						   ,[clientes_proveedores]
						   ,[productos]
						   ,[invetarios_kardex]
						   ,[configuraciones]
						   ,[usuarios]
						   ,[reportes]
						   ,[Manejar_caja]
						   ,[Herramientas])
					 VALUES
						   (@Folio
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1);

			END*/
			SET @Message = 'Operador guardado correctamente con el folio # ' + @Folio
			SET @Result = 1					

		END
		ELSE IF @Tipo_Movimiento = 1
		BEGIN
			IF (EXISTS(SELECT Id FROM Operadores WHERE (Id = @Id))) 
			BEGIN
				IF @Cambiar_Password = 1 
				BEGIN
					UPDATE [dbo].[Operadores]
					   SET [Nombre_completo] = @Nombre
						  ,[Login] = @Login
						  ,[Password] = @Password
						  ,[Icono] = @Icono
						  ,[Correo] = @Correo
						  ,[Rol] = @Rol
					 WHERE Id=@Id;
				END
				ELSE 
				BEGIN
					UPDATE [dbo].[Operadores]
					   SET [Nombre_completo] = @Nombre
						  ,[Login] = @Login
						  ,[Icono] = @Icono
						  ,[Correo] = @Correo
						  ,[Rol] = @Rol
					 WHERE Id=@Id;
				END				
				SET @Message = 'Operador guardado correctamente con el folio # ' + @Id
				SET @Result = 1
				SET @Folio = @Id
			END
		END
		ELSE IF @Tipo_Movimiento = 2
		BEGIN
			IF (EXISTS(SELECT Id FROM Operadores WHERE (Id = @Id))) 
			BEGIN
				UPDATE [dbo].[Operadores] SET Es_Cve_Estado='BA' WHERE Id=@Id
				SET @Message = 'Operador inactivamento correctamente con el folio # ' + @Id
				SET @Result = 1
				SET @Folio = @Id
			END
		END
		ELSE IF @Tipo_Movimiento = 3
		BEGIN
			IF (EXISTS(SELECT Id FROM Operadores WHERE (Id = @Id))) 
			BEGIN
				UPDATE [dbo].[Operadores] SET Es_Cve_Estado='AC' WHERE Id=@Id

				SET @Message = 'Operador activado correctamente con el folio # ' + @Id
				SET @Result = 1
				SET @Folio = @Id
			END
		END		
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;   
	IF @Mensaje_Retorno = 1 
	BEGIN
		SELECT @Message, @Result, @Folio
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[Remision_IU]    Script Date: 02/12/2020 10:59:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Remision_IU]
	@Rm_Folio nvarchar(10),
	@Rm_Referencia nvarchar(15),
	@Rm_Comentarios nvarchar(50),
	@Rm_Importe decimal(18,9),
    @Oper_Alta nvarchar(35),
	--DETALLE
	@Rm_Id int,
	@Concepto_id int,
	@Concepto nvarchar(100),
	@Cantidad decimal(18,2),
	@Precio decimal(18,2),
	@Importe decimal(18, 2),
	@Cl_Cve_Cliente nvarchar(150)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)		
		DECLARE @Result int
		DECLARE @Identity INT
		DECLARE @EXISTE_FOLIO INTEGER
		
		--Obtenemos el folio consecutivo
		DECLARE @Folio nvarchar(12) 								
		SET @EXISTE_FOLIO = (SELECT COUNT(Rm_Folio) FROM Remision_Encabezado WHERE Rm_Folio = @Rm_Folio)

		IF @EXISTE_FOLIO = 0
		BEGIN
				--SET @Folio = (SELECT dbo.CIntToChar(CASE WHEN Folio IS NULL THEN 0 ELSE Folio END + 1,10) AS FOLIO FROM Folios WHERE Tipo='Remision')

				INSERT INTO [dbo].[Remision_Encabezado]
					   ([Rm_Folio]
					   ,[Rm_Fecha]
					   ,[Rm_Referencia]
					   ,[Rm_Comentario]
					   ,[Cl_Cve_Cliente]
					   ,[Rm_Importe]
					   ,[Oper_Alta]
					   ,[Fecha_Alta]
					   ,[Oper_Ult_Modif]
					   ,[Fecha_Ult_Modif]
					   ,[Oper_Baja]
					   ,[Es_Cve_Estado])
				 VALUES
					   (@Rm_Folio
					   ,GETDATE()
					   ,@Rm_Referencia
					   ,@Rm_Comentarios
					   ,@Cl_Cve_Cliente
					   ,@Rm_Importe
					   ,@Oper_Alta
					   ,GETDATE()
					   ,@Oper_Alta
					   ,GETDATE()
					   ,''
					   ,'AC')		

				INSERT INTO [dbo].[Remision_Detalle]
					   ([Rm_Folio]
					   ,[Rm_ID]
					   ,[Rm_Fecha]
					   ,[Rm_Concepto_Id]
					   ,[Rm_Concepto]
					   ,[Rm_Cantidad]
					   ,[Rm_Precio]
					   ,[Rm_Importe]
					   ,[Oper_Alta]
					   ,[Fecha_Alta]
					   ,[Oper_Ult_Modif]
					   ,[Fecha_Ult_Modif]
					   ,[Oper_Baja]
					   ,[Es_Cve_Estado])
				 VALUES
					   (@Rm_Folio
					   ,dbo.CIntToChar(@Rm_Id, 4)
					   ,GETDATE()
					   ,@Concepto_Id
					   ,@Concepto
					   ,@Cantidad
					   ,@Precio
					   ,(@Cantidad * @Precio)
					   ,@Oper_Alta
					   ,GETDATE()
					   ,@Oper_Alta
					   ,GETDATE()
					   ,''
					   ,'AC')	

				UPDATE Folios SET Folio = [dbo].[Get_ValueOf](@Rm_Folio) WHERE Tipo = 'Remision'

		END
		ELSE 
		BEGIN
			--SET @Folio = (SELECT Rm_Folio FROM dbo.Remision_Encabezado WHERE (documento = @Folio) AND tipo_documento='Factura')
			INSERT INTO [dbo].[Remision_Detalle]
					   ([Rm_Folio]
					   ,[Rm_ID]
					   ,[Rm_Fecha]
					   ,[Rm_Concepto_Id]
					   ,[Rm_Concepto]
					   ,[Rm_Cantidad]
					   ,[Rm_Precio]
					   ,[Rm_Importe]
					   ,[Oper_Alta]
					   ,[Fecha_Alta]
					   ,[Oper_Ult_Modif]
					   ,[Fecha_Ult_Modif]
					   ,[Oper_Baja]
					   ,[Es_Cve_Estado])
				 VALUES
					   (@Rm_Folio
					   ,dbo.CIntToChar(@Rm_Id, 4)
					   ,GETDATE()
					   ,@Concepto_Id
					   ,@Concepto
					   ,@Cantidad
					   ,@Precio
					   ,(@Cantidad * @Precio)
					   ,@Oper_Alta
					   ,GETDATE()
					   ,@Oper_Alta
					   ,GETDATE()
					   ,''
					   ,'AC')	
		END
		
		SET @Message = 'Remisión generada correctamente con el # de documento: ' + @Folio 
		SET @Result = 1
			
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;           
	SELECT @Message, @Result, @Folio
END
GO
USE [master]
GO
ALTER DATABASE [INTROIT] SET  READ_WRITE 
GO
