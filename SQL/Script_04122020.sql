CREATE FUNCTION [dbo].[CIntToChar](@intVal BIGINT, @intLen Int) RETURNS nvarchar(20)
AS
BEGIN

    -- BIGINT = 2^63-1 (9,223,372,036,854,775,807) Max size number
    -- @intlen contains the string size to return
    IF @intlen > 20
       SET @intlen = 20

    RETURN REPLICATE('0',@intLen-LEN(RTRIM(CONVERT(nvarchar(20),@intVal)))) 
        + CONVERT(nvarchar(20),@intVal)
END	
GO
/****** Object:  UserDefinedFunction [dbo].[Get_Numbers]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------------------------
--RECIBE UNA CADENA Y REGRESA LOS NUMEROS CONTENIDOS EN ELLA
----------------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[Get_Numbers] (
	@Var AS nvarchar(3000) 
)  
RETURNS nvarchar(3000)
AS  
BEGIN 

DECLARE @Car as integer
DECLARE @Line as nvarchar(3000) 

SET @Line = ''
SET @Car = 0

WHILE @Car < LEN(@Var)
BEGIN
	SET @Car = @Car + 1
		
	IF  SUBSTRING(@Var,@Car,1) like '[0-9]'
	BEGIN				
		SET @Line = @Line +  SUBSTRING(@Var,@Car,1)  
	END 
END		

	RETURN  @Line 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Get_ValueOf]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------------------------
--RECIBE UNA CADENA Y QUITA LOS CEROS Y LETRAS A LA IZQUIERA 
--HASTA ENCONTRAR EL PRIMER NUMERO DIFERENTE DE CERO
----------------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[Get_ValueOf] (
	@Var as nvarchar(3000) 
)  
RETURNS nvarchar(3000)
AS  
BEGIN 

DECLARE @Car as integer
DECLARE @Line as nvarchar(3000) 
DECLARE @Caracter as nvarchar(2)

SET @Car = 0
SET @Caracter = ''
SET @Line = ''
SET @Var = dbo.Get_Numbers(@Var)

WHILE @Car < LEN(@Var)
BEGIN
	SET @Car = @Car + 1
	SET @Caracter = SUBSTRING(@Var,@Car,1)

	IF  @Caracter LIKE '[1-9]'
	BEGIN			
		
		SET @Line = RIGHT(@Var,LEN(@Var)-(@Car-1))  	

		BREAK
	END 

END		
	
IF  @Line = ''
BEGIN					
	SET @Line = '0'
END 

	RETURN @Line
END
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[Cl_Cve_Cliente] [nvarchar](10) NOT NULL,
	[Cl_Nombre] [nvarchar](150) NOT NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Alta] [nvarchar](35) NOT NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[Cl_Cve_Cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Conceptos]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conceptos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](150) NOT NULL,
	[precio_gasolina] [decimal](18, 3) NOT NULL,
	[precio_diesel] [decimal](18, 3) NOT NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Alta] [nvarchar](35) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](35) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Oper_Baja] [nvarchar](35) NOT NULL,
 CONSTRAINT [PK_Conceptos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresa](
	[Em_Cve_Empresa] [nvarchar](4) NOT NULL,
	[Em_Nombre] [nvarchar](60) NOT NULL,
	[Em_Calle] [nvarchar](50) NOT NULL,
	[Em_Cruzamiento] [nvarchar](50) NOT NULL,
	[Em_Numext] [nvarchar](10) NOT NULL,
	[Em_Numint] [nvarchar](10) NULL,
	[Em_Colonia] [nvarchar](50) NOT NULL,
	[Em_Localidad] [nvarchar](60) NOT NULL,
	[Em_Municipio] [nvarchar](60) NOT NULL,
	[Em_Ciudad] [nvarchar](60) NOT NULL,
	[Em_Estado] [nvarchar](60) NOT NULL,
	[Em_Pais] [nvarchar](50) NOT NULL,
	[Em_CodPost] [nvarchar](5) NOT NULL,
	[Em_Telefono] [nvarchar](50) NULL,
	[Em_RFC] [nvarchar](13) NOT NULL,
	[Em_Email] [nvarchar](50) NULL,
	[Em_Regimen_Fiscal] [nvarchar](250) NOT NULL,
	[Em_Cve_Regimen_Fiscal] [nvarchar](3) NOT NULL,
	[Em_Logo] [image] NULL,
	[Em_Maneja_Impuesto] [nvarchar](10) NOT NULL,
	[Em_Impuesto] [nvarchar](50) NULL,
	[Em_Impuesto_porcentaje] [decimal](18, 0) NULL,
	[Em_Moneda] [nvarchar](50) NULL,
	[Modo_Busqueda] [nvarchar](50) NOT NULL,
	[Ruta_Backup] [nvarchar](255) NULL,
	[Ultimo_backup] [datetime] NOT NULL,
	[Frecuencia_backup] [int] NOT NULL,
	[Estado_backup] [nvarchar](50) NOT NULL,
	[Redondeo_Total] [nvarchar](4) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Alta] [nvarchar](35) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](35) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Oper_Baja] [nvarchar](35) NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[Em_Cve_Empresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UNQ1_Empresas_RFC] UNIQUE NONCLUSTERED 
(
	[Em_RFC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estado]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estado](
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Es_Descripcion] [nvarchar](50) NULL,
 CONSTRAINT [PK_Estado] PRIMARY KEY CLUSTERED 
(
	[Es_Cve_Estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folios]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folios](
	[Tipo] [nvarchar](50) NOT NULL,
	[Folio] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Folios] PRIMARY KEY CLUSTERED 
(
	[Tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Licencias]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Licencias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](150) NOT NULL,
	[FI] [nvarchar](255) NOT NULL,
	[FF] [nvarchar](255) NOT NULL,
	[FA] [nvarchar](255) NOT NULL,
	[Serial_PC] [nvarchar](255) NOT NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Licencias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operadores]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operadores](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_completo] [nvarchar](100) NULL,
	[Login] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Icono] [image] NULL,
	[Nombre_Icono] [nvarchar](100) NULL,
	[Correo] [nvarchar](100) NULL,
	[Rol] [nvarchar](100) NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Operadores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Operadores] UNIQUE NONCLUSTERED 
(
	[Login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegimenFiscal]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegimenFiscal](
	[Rf_Id] [int] IDENTITY(1,1) NOT NULL,
	[Rf_Codigo] [nvarchar](255) NULL,
	[Rf_Descripcion] [nvarchar](255) NULL,
	[Rf_Fisica] [nvarchar](255) NULL,
	[Rf_Moral] [nvarchar](255) NULL,
	[Rf_FechaInicio] [date] NULL,
	[Rf_FechaFin] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Remision_Detalle]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Remision_Detalle](
	[Rm_Folio] [nvarchar](10) NOT NULL,
	[Rm_ID] [nvarchar](4) NOT NULL,
	[Rm_Fecha] [datetime] NOT NULL,
	[Rm_Concepto_Id] [int] NOT NULL,
	[Rm_Concepto] [nvarchar](100) NOT NULL,
	[Rm_Cantidad] [decimal](18, 9) NOT NULL,
	[Rm_Precio] [decimal](18, 9) NOT NULL,
	[Rm_Importe] [decimal](18, 9) NOT NULL,
	[Oper_Alta] [nvarchar](15) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](15) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Baja] [nvarchar](15) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Remision] PRIMARY KEY NONCLUSTERED 
(
	[Rm_Folio] ASC,
	[Rm_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Remision_Encabezado]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Remision_Encabezado](
	[Rm_Folio] [nvarchar](10) NOT NULL,
	[Rm_Fecha] [datetime] NOT NULL,
	[Rm_Referencia] [nvarchar](15) NOT NULL,
	[Rm_Comentario] [text] NULL,
	[Vn_Cve_Vendedor] [nvarchar](10) NULL,
	[Cl_Cve_Cliente] [nvarchar](10) NULL,
	[Rm_Importe] [decimal](18, 9) NOT NULL,
	[Oper_Alta] [nvarchar](15) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Oper_Ult_Modif] [nvarchar](15) NOT NULL,
	[Fecha_Ult_Modif] [datetime] NOT NULL,
	[Oper_Baja] [nvarchar](15) NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Es_Cve_Estado] [nvarchar](4) NOT NULL,
	[Em_Cve_Empresa] [nvarchar](4) NOT NULL,
	[Em_Empresa] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_Remision_Encabezado] PRIMARY KEY NONCLUSTERED 
(
	[Rm_Folio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_Fecha_Alta]  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_Clientes_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_precio_gasolina]  DEFAULT ((0)) FOR [precio_gasolina]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_precio_diesel]  DEFAULT ((0)) FOR [precio_diesel]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Fecha_Alta]  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Fecha_Ult_Modif]  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Oper_Ult_Modif]  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Conceptos] ADD  CONSTRAINT [DF_Conceptos_Oper_Baja]  DEFAULT ('') FOR [Oper_Baja]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('NO') FOR [Em_Maneja_Impuesto]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('TECLADO') FOR [Modo_Busqueda]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT (getdate()) FOR [Ultimo_backup]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ((1)) FOR [Frecuencia_backup]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('PENDIENTE') FOR [Estado_backup]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('NO') FOR [Redondeo_Total]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Empresa] ADD  DEFAULT ('AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Licencias] ADD  CONSTRAINT [DF_Licencias_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Operadores] ADD  CONSTRAINT [DF_Operadores_Es_Cve_Estado]  DEFAULT (N'AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  DEFAULT (getdate()) FOR [Rm_Fecha]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Detalle_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Detalle_Oper_Ult_Modif]  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Oper_Baja]  DEFAULT ('') FOR [Oper_Baja]
GO
ALTER TABLE [dbo].[Remision_Detalle] ADD  CONSTRAINT [DF_Remision_Es_Cve_Estado]  DEFAULT ('AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Oper_Alta]  DEFAULT ('') FOR [Oper_Alta]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Fecha_Alta]  DEFAULT (getdate()) FOR [Fecha_Alta]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Oper_Ult_Modif]  DEFAULT ('') FOR [Oper_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Fecha_Ult_Modif]  DEFAULT (getdate()) FOR [Fecha_Ult_Modif]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Oper_Baja]  DEFAULT ('') FOR [Oper_Baja]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  CONSTRAINT [DF_Remision_Encabezado_Es_Cve_Estado]  DEFAULT ('AC') FOR [Es_Cve_Estado]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  DEFAULT ('0001') FOR [Em_Cve_Empresa]
GO
ALTER TABLE [dbo].[Remision_Encabezado] ADD  DEFAULT ('0001') FOR [Em_Empresa]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Estado]
GO
ALTER TABLE [dbo].[Conceptos]  WITH CHECK ADD  CONSTRAINT [FK_Conceptos_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Conceptos] CHECK CONSTRAINT [FK_Conceptos_Estado]
GO
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_Empresas_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_Empresas_Estado]
GO
ALTER TABLE [dbo].[Licencias]  WITH CHECK ADD  CONSTRAINT [FK_Licencias_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Licencias] CHECK CONSTRAINT [FK_Licencias_Estado]
GO
ALTER TABLE [dbo].[Operadores]  WITH CHECK ADD  CONSTRAINT [FK_Operadores_Estado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Operadores] CHECK CONSTRAINT [FK_Operadores_Estado]
GO
ALTER TABLE [dbo].[Remision_Detalle]  WITH CHECK ADD  CONSTRAINT [RL_Estado_Remision] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Remision_Detalle] CHECK CONSTRAINT [RL_Estado_Remision]
GO
ALTER TABLE [dbo].[Remision_Detalle]  WITH CHECK ADD  CONSTRAINT [RL_Remision_Encabezado_Remision] FOREIGN KEY([Rm_Folio])
REFERENCES [dbo].[Remision_Encabezado] ([Rm_Folio])
GO
ALTER TABLE [dbo].[Remision_Detalle] CHECK CONSTRAINT [RL_Remision_Encabezado_Remision]
GO
ALTER TABLE [dbo].[Remision_Detalle]  WITH CHECK ADD  CONSTRAINT [RL_Rm_Concepto_Id_Remision] FOREIGN KEY([Rm_Concepto_Id])
REFERENCES [dbo].[Conceptos] ([id])
GO
ALTER TABLE [dbo].[Remision_Detalle] CHECK CONSTRAINT [RL_Rm_Concepto_Id_Remision]
GO
ALTER TABLE [dbo].[Remision_Encabezado]  WITH CHECK ADD  CONSTRAINT [FK_Remision_Encabezado_Empresa] FOREIGN KEY([Em_Cve_Empresa])
REFERENCES [dbo].[Empresa] ([Em_Cve_Empresa])
GO
ALTER TABLE [dbo].[Remision_Encabezado] CHECK CONSTRAINT [FK_Remision_Encabezado_Empresa]
GO
ALTER TABLE [dbo].[Remision_Encabezado]  WITH CHECK ADD  CONSTRAINT [RL_Estado_Remision_Encabezado] FOREIGN KEY([Es_Cve_Estado])
REFERENCES [dbo].[Estado] ([Es_Cve_Estado])
GO
ALTER TABLE [dbo].[Remision_Encabezado] CHECK CONSTRAINT [RL_Estado_Remision_Encabezado]
GO
/****** Object:  StoredProcedure [dbo].[Cliente_IU]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.S.U. Cesar Chab
-- Create date: 02/12/2020
-- Description: Alta y mantenimiento de clientes
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_IU]
	@Cl_Cve_Cliente NVARCHAR(10),
	@Cl_Nombre NVARCHAR(150),
	@Oper_Alta NVARCHAR(35),
	@Tipo_Movimiento INT --0 Nuevo, 1 Actualizar, 2 Baja, 3 Activar	
AS
BEGIN
	BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @Result int 
		DECLARE @Mensaje_Retorno INT = 1

		--Obtenemos el folio consecutivo
		DECLARE @Folio nvarchar(10);
		SET @Folio = (SELECT dbo.CIntToChar(CASE WHEN Folio IS NULL THEN 0 ELSE Folio END + 1,10) AS FOLIO FROM Folios WHERE Tipo='Clientes')

		IF @Tipo_Movimiento = 0
		BEGIN
			INSERT INTO [dbo].[Clientes]
					   ([Cl_Cve_Cliente]
					   ,[Cl_Nombre]
					   ,[Es_Cve_Estado]
					   ,[Fecha_Alta]
					   ,[Oper_Alta])
				 VALUES
					   (@Folio
					   ,@Cl_Nombre
					   ,'AC'
					   ,GETDATE	()
					   ,@Oper_Alta)
		
				UPDATE Folios SET Folio = [dbo].[Get_ValueOf](@Folio) WHERE Tipo = 'Clientes';

			SET @Message = 'Cliente guardado correctamente con el folio #' + @Folio 
			SET @Result = 1		

		END
		ELSE IF @Tipo_Movimiento = 1
		BEGIN
			IF (EXISTS(SELECT Cl_Cve_Cliente FROM dbo.Clientes WHERE (Cl_Cve_Cliente = @Cl_Cve_Cliente))) 
			BEGIN
				
				UPDATE [dbo].[Clientes]
				   SET 
					   [Cl_Nombre] = @Cl_Nombre
				 WHERE [Cl_Cve_Cliente] = @Cl_Cve_Cliente				

				 SET @Message = 'Cliente guardado correctamente';
				 SET @Result = 1
				 SET @Folio = @Cl_Cve_Cliente
			END
		END
		ELSE IF @Tipo_Movimiento = 2
		BEGIN
			IF (EXISTS(SELECT Cl_Cve_Cliente FROM Clientes WHERE (Cl_Cve_Cliente = @Cl_Cve_Cliente))) 
			BEGIN
				UPDATE dbo.Clientes SET Es_Cve_Estado='BA' 
				WHERE Cl_Cve_Cliente=@Cl_Cve_Cliente

				SET @Message = 'Clinete dado de baja correctamente con el folio #' + @Cl_Cve_Cliente
				SET @Result = 1
				SET @Folio = @Cl_Cve_Cliente
			END
		END
		ELSE IF @Tipo_Movimiento = 3
		BEGIN
			IF (EXISTS(SELECT Cl_Cve_Cliente FROM Clientes WHERE (Cl_Cve_Cliente = @Cl_Cve_Cliente))) 
			BEGIN
				UPDATE dbo.Clientes SET Es_Cve_Estado='AC' 
				WHERE Cl_Cve_Cliente=@Cl_Cve_Cliente

				SET @Message = 'Clinete activado correctamente con el folio #' + @Cl_Cve_Cliente
				SET @Result = 1
				SET @Folio = @Cl_Cve_Cliente
			END
		END		
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;           
	IF @Mensaje_Retorno = 1 
	BEGIN
		SELECT @Message, @Result, @Folio
	END	
END

GO
/****** Object:  StoredProcedure [dbo].[Empresas_IU]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.S.U. Cesar Chab
-- Create date: 29/11/2020 07:28:00 a.m
-- Description: Alta y mantenimiento del catalogo de empresas
-- =============================================
CREATE PROCEDURE [dbo].[Empresas_IU]
	@Em_Cve_Empresa NVARCHAR(4),
    @Em_Nombre NVARCHAR(60),
    @Em_Calle NVARCHAR(50),
    @Em_Cruzamiento NVARCHAR(50),
    @Em_Numext NVARCHAR(10),
    @Em_Numint NVARCHAR(10),
    @Em_Colonia NVARCHAR(50),
    @Em_Localidad NVARCHAR(60),
    @Em_Municipio NVARCHAR(60),
    @Em_Ciudad NVARCHAR(60),
    @Em_Estado NVARCHAR(60),
    @Em_Pais NVARCHAR(50),
    @Em_CodPost NVARCHAR(5),
    @Em_Telefono NVARCHAR(50),
    @Em_RFC NVARCHAR(13),
    @Em_Email NVARCHAR(50),
    @Em_Regimen_Fiscal NVARCHAR(250),
    @Em_Cve_Regimen_Fiscal NVARCHAR(3),
	@Em_Logo IMAGE,    
	@Em_Maneja_Impuesto NVARCHAR(10),
	@Em_Impuesto NVARCHAR(50),
	@Em_Impuesto_porcentaje decimal(18,0),
	@Em_Moneda NVARCHAR(50),
	@Modo_Busqueda NVARCHAR(50),
	@Ruta_Backup NVARCHAR(255),
	@Ultimo_Backup DATETIME,
	@Frecuencia_Backup INT,
	@Redondeo_Total NVARCHAR(4),
    @Oper_Alta NVARCHAR(35),
	@Tipo_Movimiento INT, --0 Nuevo, 1 Actualizar, 2 Baja, 3 Activar	
	@Crea_Operador bit = 0,
	@Nombre nvarchar(100),
	@Login nvarchar(50),
	@Password nvarchar(50),
	@Icono image,
	@Correo nvarchar(100),
	@Rol nvarchar(100),
	--Active Licence 30 dias
	@Crea_Licencia bit = 0,
	@Lic_Descripcion nvarchar(150),
	@Lic_FI NVARCHAR(255),
	@Lic_FF NVARCHAR(255),
	@LIC_FA NVARCHAR(255),
	@SerialPC varchar(255)
	
AS
BEGIN

BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @MessageCaja NVARCHAR(MAX)
		DECLARE @MessageOperador NVARCHAR(MAX)
		DECLARE @Result int 

		--Obtenemos el folio consecutivo
		DECLARE @Folio nvarchar(10) 

		IF @Tipo_Movimiento = 0
		BEGIN
			SET @Folio = (SELECT 
							dbo.CIntToChar(CASE WHEN MAX(Em_Cve_Empresa) IS NULL THEN 0 ELSE MAX(Em_Cve_Empresa) END + 1, 4) AS Folio 
							FROM dbo.Empresa);

			INSERT INTO [dbo].[Empresa]
					   ([Em_Cve_Empresa]
					   ,[Em_Nombre]
					   ,[Em_Calle]
					   ,[Em_Cruzamiento]
					   ,[Em_Numext]
					   ,[Em_Numint]
					   ,[Em_Colonia]
					   ,[Em_Localidad]
					   ,[Em_Municipio]
					   ,[Em_Ciudad]
					   ,[Em_Estado]
					   ,[Em_Pais]
					   ,[Em_CodPost]
					   ,[Em_Telefono]
					   ,[Em_RFC]
					   ,[Em_Email]
					   ,[Em_Regimen_Fiscal]
					   ,[Em_Cve_Regimen_Fiscal]
					   ,[Em_Logo]
					   ,[Em_Maneja_Impuesto]
					   ,[Em_Impuesto]
					   ,[Em_Impuesto_porcentaje]
					   ,[Em_Moneda]
					   ,[Modo_Busqueda]
					   ,[Ruta_Backup]
					   ,[Ultimo_backup]
					   ,[Frecuencia_backup]
					   ,[Estado_backup]
					   ,[Redondeo_Total]					  
					   ,[Oper_Alta]					   
					   ,[Oper_Ult_Modif]
				)
				 VALUES
					   (@Folio
					   ,@Em_Nombre
					   ,@Em_Calle
					   ,@Em_Cruzamiento
					   ,@Em_Numext
					   ,@Em_Numint
					   ,@Em_Colonia
					   ,@Em_Localidad
					   ,@Em_Municipio
					   ,@Em_Ciudad
					   ,@Em_Estado
					   ,@Em_Pais
					   ,@Em_CodPost
					   ,@Em_Telefono
					   ,@Em_RFC
					   ,@Em_Email
					   ,@Em_Regimen_Fiscal
					   ,@Em_Cve_Regimen_Fiscal
					   ,@Em_Logo
					   ,@Em_Maneja_Impuesto
					   ,@Em_Impuesto
					   ,@Em_Impuesto_porcentaje
					   ,@Em_Moneda
					   ,@Modo_Busqueda
					   ,@Ruta_Backup
					   ,GETDATE()
					   ,@Frecuencia_backup
					   ,'PENDIENTE'
					   ,@Redondeo_Total
					   ,@Oper_Alta
					   ,@Oper_Alta
					  );
			
			IF @Crea_Operador = 1 
			BEGIN
				EXEC @MessageOperador = dbo.Operadores_IU 0, @Nombre, @Login, @Password, @Icono, @Correo, @Rol, 0,0,0,0,1;
			END

			IF @Crea_Licencia = 1
			BEGIN

				INSERT INTO [dbo].[Licencias]
						   ([Descripcion]
						   ,[FI]
						   ,[FF]
						   ,[FA]
						   ,[Serial_PC]
						   ,[Es_Cve_Estado])
					 VALUES
						   (@Lic_Descripcion
						   ,@Lic_FI
						   ,@Lic_FF
						   ,@LIC_FA
						   ,@SerialPC
						   ,'PE');

			END
		
			SET @Message = 'Empresa guardado correctamente con el folio #' + @Folio 
			SET @Result = 1		

		END
		ELSE IF @Tipo_Movimiento = 1
		BEGIN
			IF (EXISTS(SELECT Em_Cve_Empresa FROM Empresa WHERE (Em_Cve_Empresa = @Em_Cve_Empresa))) 
			BEGIN
				UPDATE [dbo].[Empresa]
				   SET [Em_Nombre] = @Em_Nombre
					  ,[Em_Calle] = @Em_Calle
					  ,[Em_Cruzamiento] = @Em_Cruzamiento
					  ,[Em_Numext] = @Em_Numext
					  ,[Em_Numint] = @Em_Numint
					  ,[Em_Colonia] = @Em_Colonia
					  ,[Em_Localidad] = @Em_Localidad
					  ,[Em_Municipio] = @Em_Municipio
					  ,[Em_Ciudad] = @Em_Ciudad
					  ,[Em_Estado] = @Em_Estado
					  ,[Em_Pais] = @Em_Pais
					  ,[Em_CodPost] = @Em_CodPost
					  ,[Em_Telefono] = @Em_Telefono
					  ,[Em_RFC] = @Em_RFC
					  ,[Em_Email] = @Em_Email
					  ,[Em_Regimen_Fiscal] = @Em_Regimen_Fiscal
					  ,[Em_Cve_Regimen_Fiscal] = @Em_Cve_Regimen_Fiscal
					  ,[Em_Logo] = @Em_Logo
					  ,[Em_Maneja_Impuesto] = @Em_Maneja_Impuesto
					  ,[Em_Impuesto] = @Em_Impuesto
					  ,[Em_Impuesto_porcentaje] = @Em_Impuesto_porcentaje
					  ,[Em_Moneda] = @Em_Moneda
					  ,[Modo_Busqueda] = @Modo_Busqueda
					  ,[Ruta_Backup] = @Ruta_Backup
					  --,[Ultimo_backup] = @Ultimo_backup
					  ,[Frecuencia_backup] = @Frecuencia_backup
					  --,[Estado_backup] = @Estado_backup
					  ,[Redondeo_Total] = @Redondeo_Total
					  ,[Fecha_Ult_Modif] = GETDATE()
					  ,[Oper_Ult_Modif] = @Oper_Alta
				 WHERE [Em_Cve_Empresa] = @Em_Cve_Empresa
				 SET @Message = 'Empresa guardado correctamente';
				 SET @Result = 1
				 SET @Folio = @Em_Cve_Empresa
			END
		END
		ELSE IF @Tipo_Movimiento = 2
		BEGIN
			IF (EXISTS(SELECT Em_Cve_Empresa FROM Empresa WHERE (Em_Cve_Empresa = @Em_Cve_Empresa))) 
			BEGIN
				UPDATE dbo.Empresa SET Es_Cve_Estado='BA', Oper_Ult_Modif=@Oper_Alta, Fecha_Ult_Modif=GETDATE(), Oper_Baja=@Oper_Alta, Fecha_Baja=GETDATE()
				WHERE Em_Cve_Empresa=@Em_Cve_Empresa

				SET @Message = 'Empresa inactivamento correctamente con el folio #' + @Em_Cve_Empresa
				SET @Result = 1
				SET @Folio = @Em_Cve_Empresa
			END
		END
		ELSE IF @Tipo_Movimiento = 3
		BEGIN
			IF (EXISTS(SELECT Em_Cve_Empresa FROM Empresa WHERE (Em_Cve_Empresa = @Em_Cve_Empresa))) 
			BEGIN
				UPDATE dbo.Empresa SET Es_Cve_Estado='AC', Oper_Ult_Modif=@Oper_Alta, Fecha_Ult_Modif=GETDATE(), Oper_Baja='', Fecha_Baja=NULL
				WHERE Em_Cve_Empresa=@Em_Cve_Empresa

				SET @Message = 'Empresa activado correctamente con el folio #' + @Em_Cve_Empresa
				SET @Result = 1
				SET @Folio = @Em_Cve_Empresa
			END
		END		
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;           
	SELECT @Message, @Result, @Folio
END
GO
/****** Object:  StoredProcedure [dbo].[Operadores_IU]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.S.U. Cesar Chab
-- Create date: 29/11/2020 14:44:00 p.m
-- Description:	Alta y mantenimiento de usuarios del sistema
-- =============================================
CREATE PROCEDURE [dbo].[Operadores_IU]
	@Id int,
	@Nombre nvarchar(100),
	@Login nvarchar(50),
	@Password nvarchar(50),
	@Icono image,
	@Correo nvarchar(100),
	@Rol nvarchar(100),
	@Tipo_Movimiento int,
	@Mensaje_Retorno int = 0,
	@Cambiar_Password int = 0,
	@Cambiar_Imagen int = 0,
	@Permiso int = 0
	
AS
BEGIN
	BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @Result int

		--Obtenemos el folio consecutivoA
		DECLARE @Folio nvarchar(10) 

		IF @Tipo_Movimiento = 0
		BEGIN

			INSERT INTO [dbo].[Operadores]
				([Nombre_completo]
				,[Login]
				,[Password]
				,[Icono]
				,[Nombre_Icono]
				,[Correo]
				,[Rol]
				,[Es_Cve_Estado])
			VALUES
				(@Nombre
				,@Login
				,@Password
				,@Icono
				,''
				,@Correo
				,@Rol
				,'AC');
			SET @Folio = (SELECT @@IDENTITY)
			
			/*--Insert Permisos admin
			IF @Permiso = 1 
			BEGIN
				INSERT INTO [dbo].[Permisos]
						   ([Usuario_Id]
						   ,[ventas]
						   ,[aplicar_descuentos]
						   ,[Devoluciones]
						   ,[clientes_proveedores]
						   ,[productos]
						   ,[invetarios_kardex]
						   ,[configuraciones]
						   ,[usuarios]
						   ,[reportes]
						   ,[Manejar_caja]
						   ,[Herramientas])
					 VALUES
						   (@Folio
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1
						   ,1);

			END*/
			SET @Message = 'Operador guardado correctamente con el folio # ' + @Folio
			SET @Result = 1					

		END
		ELSE IF @Tipo_Movimiento = 1
		BEGIN
			IF (EXISTS(SELECT Id FROM Operadores WHERE (Id = @Id))) 
			BEGIN
				IF @Cambiar_Password = 1 
				BEGIN
					UPDATE [dbo].[Operadores]
					   SET [Nombre_completo] = @Nombre
						  ,[Login] = @Login
						  ,[Password] = @Password
						  ,[Icono] = @Icono
						  ,[Correo] = @Correo
						  ,[Rol] = @Rol
					 WHERE Id=@Id;
				END
				ELSE 
				BEGIN
					UPDATE [dbo].[Operadores]
					   SET [Nombre_completo] = @Nombre
						  ,[Login] = @Login
						  ,[Icono] = @Icono
						  ,[Correo] = @Correo
						  ,[Rol] = @Rol
					 WHERE Id=@Id;
				END				
				SET @Message = 'Operador guardado correctamente con el folio # ' + @Id
				SET @Result = 1
				SET @Folio = @Id
			END
		END
		ELSE IF @Tipo_Movimiento = 2
		BEGIN
			IF (EXISTS(SELECT Id FROM Operadores WHERE (Id = @Id))) 
			BEGIN
				UPDATE [dbo].[Operadores] SET Es_Cve_Estado='BA' WHERE Id=@Id
				SET @Message = 'Operador inactivamento correctamente con el folio # ' + @Id
				SET @Result = 1
				SET @Folio = @Id
			END
		END
		ELSE IF @Tipo_Movimiento = 3
		BEGIN
			IF (EXISTS(SELECT Id FROM Operadores WHERE (Id = @Id))) 
			BEGIN
				UPDATE [dbo].[Operadores] SET Es_Cve_Estado='AC' WHERE Id=@Id

				SET @Message = 'Operador activado correctamente con el folio # ' + @Id
				SET @Result = 1
				SET @Folio = @Id
			END
		END		
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;   
	IF @Mensaje_Retorno = 1 
	BEGIN
		SELECT @Message, @Result, @Folio
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[Remision_IU]    Script Date: 04/12/2020 10:48:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Remision_IU]
	@Rm_Folio nvarchar(10),
	@Rm_Referencia nvarchar(15),
	@Rm_Comentarios nvarchar(50),
	@Rm_Importe decimal(18,9),
    @Oper_Alta nvarchar(35),
	--DETALLE
	@Rm_Id int,
	@Concepto_id int,
	@Concepto nvarchar(100),
	@Cantidad decimal(18,2),
	@Precio decimal(18,2),
	@Importe decimal(18, 2),
	@Cl_Cve_Cliente nvarchar(150)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	BEGIN TRY 	
		DECLARE @Message NVARCHAR(MAX)		
		DECLARE @Result int
		DECLARE @Identity INT
		DECLARE @EXISTE_FOLIO INTEGER
		
		--Obtenemos el folio consecutivo
		DECLARE @Folio nvarchar(12) 								
		SET @EXISTE_FOLIO = (SELECT COUNT(Rm_Folio) FROM Remision_Encabezado WHERE Rm_Folio = @Rm_Folio)

		IF @EXISTE_FOLIO = 0
		BEGIN
				--SET @Folio = (SELECT dbo.CIntToChar(CASE WHEN Folio IS NULL THEN 0 ELSE Folio END + 1,10) AS FOLIO FROM Folios WHERE Tipo='Remision')

				INSERT INTO [dbo].[Remision_Encabezado]
					   ([Rm_Folio]
					   ,[Rm_Fecha]
					   ,[Rm_Referencia]
					   ,[Rm_Comentario]
					   ,[Cl_Cve_Cliente]
					   ,[Rm_Importe]
					   ,[Oper_Alta]
					   ,[Fecha_Alta]
					   ,[Oper_Ult_Modif]
					   ,[Fecha_Ult_Modif]
					   ,[Oper_Baja]
					   ,[Es_Cve_Estado])
				 VALUES
					   (@Rm_Folio
					   ,GETDATE()
					   ,@Rm_Referencia
					   ,@Rm_Comentarios
					   ,@Cl_Cve_Cliente
					   ,@Rm_Importe
					   ,@Oper_Alta
					   ,GETDATE()
					   ,@Oper_Alta
					   ,GETDATE()
					   ,''
					   ,'AC')		

				INSERT INTO [dbo].[Remision_Detalle]
					   ([Rm_Folio]
					   ,[Rm_ID]
					   ,[Rm_Fecha]
					   ,[Rm_Concepto_Id]
					   ,[Rm_Concepto]
					   ,[Rm_Cantidad]
					   ,[Rm_Precio]
					   ,[Rm_Importe]
					   ,[Oper_Alta]
					   ,[Fecha_Alta]
					   ,[Oper_Ult_Modif]
					   ,[Fecha_Ult_Modif]
					   ,[Oper_Baja]
					   ,[Es_Cve_Estado])
				 VALUES
					   (@Rm_Folio
					   ,dbo.CIntToChar(@Rm_Id, 4)
					   ,GETDATE()
					   ,@Concepto_Id
					   ,@Concepto
					   ,@Cantidad
					   ,@Precio
					   ,(@Cantidad * @Precio)
					   ,@Oper_Alta
					   ,GETDATE()
					   ,@Oper_Alta
					   ,GETDATE()
					   ,''
					   ,'AC')	

				UPDATE Folios SET Folio = [dbo].[Get_ValueOf](@Rm_Folio) WHERE Tipo = 'Remision'

		END
		ELSE 
		BEGIN
			--SET @Folio = (SELECT Rm_Folio FROM dbo.Remision_Encabezado WHERE (documento = @Folio) AND tipo_documento='Factura')
			INSERT INTO [dbo].[Remision_Detalle]
					   ([Rm_Folio]
					   ,[Rm_ID]
					   ,[Rm_Fecha]
					   ,[Rm_Concepto_Id]
					   ,[Rm_Concepto]
					   ,[Rm_Cantidad]
					   ,[Rm_Precio]
					   ,[Rm_Importe]
					   ,[Oper_Alta]
					   ,[Fecha_Alta]
					   ,[Oper_Ult_Modif]
					   ,[Fecha_Ult_Modif]
					   ,[Oper_Baja]
					   ,[Es_Cve_Estado])
				 VALUES
					   (@Rm_Folio
					   ,dbo.CIntToChar(@Rm_Id, 4)
					   ,GETDATE()
					   ,@Concepto_Id
					   ,@Concepto
					   ,@Cantidad
					   ,@Precio
					   ,(@Cantidad * @Precio)
					   ,@Oper_Alta
					   ,GETDATE()
					   ,@Oper_Alta
					   ,GETDATE()
					   ,''
					   ,'AC')	
		END
		
		SET @Message = 'Remisión generada correctamente con el # de documento: ' + @Folio 
		SET @Result = 1
			
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()	
		SET @Result = 2	
		 IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;			
	RAISERROR (@Message, 10, 1)
	END CATCH			
											
	IF @@TRANCOUNT > 0		
		COMMIT TRANSACTION;           
	SELECT @Message, @Result, @Folio
END
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'0', N'(TODOS)')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AB', N'ABIERTO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AC', N'ACTIVO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'ACT', N'ACTUALIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AD', N'APLICADO Y DEPOSITADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AP', N'APLICADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'APT', N'APARTADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AR', N'ACUSE DE RECIBO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AS', N'ASIGNADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'AU', N'AUTORIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'BA', N'BAJA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'BO', N'BACKORDER')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'BS', N'BLOQUEADO POR SISTEMA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CA', N'CANCELADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CE', N'CERRADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CO', N'CONFIRMADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CON', N'CONSOLIDADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'CT', N'COTIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'DE', N'DEVUELTO TOTALMENTE')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'DP', N'DEPOSITADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'DS', N'DESACTIVADO POR SISTEMA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'EM', N'EMITIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'EN', N'ENVIADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'EP', N'EN PROCESO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FA', N'FACTURADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FC', N'FIN DE CONSIGNA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FI', N'FIRME')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FN', N'FINALIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'FR', N'FACTURADO CON REMISION')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'IM', N'IMPRESO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'LI', N'LIBERADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PA', N'PAGADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PAU', N'PRE-AUTORIZADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PD', N'PENDIENTE')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PE', N'PEDIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PR', N'PROGRAMADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PXA', N'PENDIENTE POR AUTORIZAR')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'PXC', N'PENDIENTE POR CANCELAR')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RA', N'ACUSE DE RECIBO CON RECHAZO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RCP', N'RECIBIDO PARCIAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RCT', N'RECIBIDO TOTAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RE', N'REVISADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RF', N'RE-FACTURACION')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RM', N'REMISION')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RMP', N'REMISIONADO PARCIAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RMT', N'REMISIONADO TOTAL')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RP', N'REPROGRAMADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'RZ', N'RECHAZO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'SU', N'SUSPENDIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'SUR', N'SURTIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'TE', N'TERMINADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'TR', N'TRANSITO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'UN', N'UNIDA')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'VA', N'VENTA APARTADO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'VC', N'VENCIDO')
GO
INSERT [dbo].[Estado] ([Es_Cve_Estado], [Es_Descripcion]) VALUES (N'VE', N'VENTA')
GO
INSERT [dbo].[Folios] ([Tipo], [Folio]) VALUES (N'Clientes', CAST(5 AS Decimal(18, 0)))
GO
INSERT [dbo].[Folios] ([Tipo], [Folio]) VALUES (N'Remision', CAST(2 AS Decimal(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[RegimenFiscal] ON 
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (1, N'601', N'General de Ley Personas Morales', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (2, N'603', N'Personas Morales con Fines no Lucrativos', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (3, N'605', N'Sueldos y Salarios e Ingresos Asimilados a Salarios', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (4, N'606', N'Arrendamiento', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (5, N'608', N'Demás ingresos', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (6, N'609', N'Consolidación', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (7, N'610', N'Residentes en el Extranjero sin Establecimiento Permanente en México', N'Si', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (8, N'611', N'Ingresos por Dividendos (socios y accionistas)', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (9, N'612', N'Personas FÍsicas con Actividades Empresariales y Profesionales', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (10, N'614', N'Ingresos por intereses', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (11, N'616', N'Sin obligaciones fiscales', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (12, N'620', N'Sociedades Cooperativas de Producción que optan por diferir sus ingresos', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (13, N'621', N'Incorporación Fiscal', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (14, N'622', N'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras', N'Si', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (15, N'623', N'Opcional para Grupos de Sociedades', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (16, N'624', N'Coordinados', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (17, N'628', N'Hidrocarburos', N'No', N'Si', CAST(N'2020-01-01' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (18, N'607', N'Régimen de Enajenación o Adquisición de Bienes', N'No', N'Si', CAST(N'2016-12-11' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (19, N'629', N'De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales', N'Si', N'No', CAST(N'2020-01-01' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (20, N'630', N'Enajenación de acciones en bolsa de valores', N'Si', N'No', CAST(N'2020-01-01' AS Date), NULL)
GO
INSERT [dbo].[RegimenFiscal] ([Rf_Id], [Rf_Codigo], [Rf_Descripcion], [Rf_Fisica], [Rf_Moral], [Rf_FechaInicio], [Rf_FechaFin]) VALUES (21, N'615', N'Régimen de los ingresos por obtención de premios', N'Si', N'No', CAST(N'2016-12-11' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[RegimenFiscal] OFF
GO
