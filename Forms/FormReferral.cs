﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using AppINTROIT.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    public partial class FormReferral : Form
    {
        private bool isNewItem = false;
        WebBrowser myWebBrowser = new WebBrowser();

        public FormReferral()
        {
            InitializeComponent();

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearForm()
        {
            isNewItem = false;
            txtConcepto.Text = "";
            txtClave.Text = "";
            txtPrecioDiesel.Text = "0.00";
            txtPrecioGasolina.Text = "0.00";
            txtDescripcion.Text = "";
            rtbComentarios.Text = "";
            txtCliente.Text = "";
            ActiveCustomer._Cl_Cve_Cliente = "";
            ActiveCustomer._Cl_Nombre = "";
            for (int index = dataGridView1.Rows.Count - 1; index >= 0; index += -1)
            {
                if (dataGridView1.Rows[index].IsNewRow)
                    continue;
                dataGridView1.Rows.RemoveAt(index);
            }
            txtGrandTotal.Text = "0.00";
            txtCliente.AutoCompleteCustomSource = Customer.Autocomplete();
            txtCliente.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtCliente.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtCliente.Focus();

            txtConcepto.AutoCompleteCustomSource = Concept.Autocomplete();
            txtConcepto.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtConcepto.AutoCompleteSource = AutoCompleteSource.CustomSource;

            dataGridView1.ClearSelection();

        }

        private void FormReferral_Load(object sender, EventArgs e)
        {
            clearForm();
            // myWebBrowser.DocumentCompleted += myWebBrowser_DocumentCompleted;
            // myWebBrowser.DocumentText = System.IO.File.ReadAllText(Application.StartupPath + @"\\invoice.html");
            //txtConcepto.AutoCompleteCustomSource = Concept.Autocomplete();
            //txtConcepto.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //txtConcepto.AutoCompleteSource = AutoCompleteSource.CustomSource;


            //// Cargo los datos que tendra el combobox
            //cboConcepto.DataSource = Concept.All(true);
            //cboConcepto.DisplayMember = "Descripcion";
            //cboConcepto.ValueMember = "Clave";

            //// cargo la lista de items para el autocomplete dle combobox
            //cboConcepto.AutoCompleteCustomSource = Concept.Autocomplete();
            //cboConcepto.AutoCompleteMode = AutoCompleteMode.Suggest;
            //cboConcepto.AutoCompleteSource = AutoCompleteSource.CustomSource; ;
            //cboCliente.AutoCompleteCustomSource = Customer.Autocomplete();
            //cboCliente.AutoCompleteMode = AutoCompleteMode.Suggest;
            //cboCliente.AutoCompleteSource = AutoCompleteSource.CustomSource;


        }

        private void txtConcepto_TextChanged(object sender, EventArgs e)
        {
            txtCantidad.Text = "0.00";
            txtPrecioDiesel.Text = "0.00";
            txtPrecioGasolina.Text = "0.00";
            //if (!String.IsNullOrEmpty(txtConcepto.Text))
            //{
            //    searchText(txtConcepto.Text);
            //}
            //else
            //{
            //    gridConceptos.Visible = false;
            //    gridConceptos.DataSource = null;
            //    gridConceptos.ClearSelection();
            //}

        }

        private void txtConcepto_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            //{
            //    int x = gridConceptos.CurrentCell.RowIndex;// + 1;
            //    gridConceptos.Rows[x].Selected = true;
            //    gridConceptos.DefaultCellStyle.SelectionBackColor = Color.WhiteSmoke;
            //    gridConceptos.Focus();
            //}
        }

        private void txtConcepto_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            //{
            //    int x = gridConceptos.CurrentCell.RowIndex;// + 1;
            //    gridConceptos.Rows[x].Selected = true;
            //    gridConceptos.DefaultCellStyle.SelectionBackColor = Color.WhiteSmoke;
            //    gridConceptos.Focus();

            //}
            //else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
            //{
            //    isNewItem = false;
            //}
        }


        private void searchText(string concept)
        {
            if (!isNewItem)
            {
                gridConceptos.Visible = false;
                var DT = new Concept().searchConcept(concept);
                if (DT.Rows.Count > 0)
                {
                    gridConceptos.Visible = true;
                    gridConceptos.DataSource = DT;
                    gridConceptos.ClearSelection();
                }
            }
        }

        private void searchCustomer(string customer)
        {
            gridCustomer.DataSource = new Customer().searchCustomer(customer);
            gridCustomer.ClearSelection();
        }

        private void gridConceptos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //txtConcepto.Text = "";
            //txtCantidad.Text = "1";
            //MessageBox.Show(gridConceptos.SelectedRows[e.RowIndex].Cells[1].Value.ToString());
            ////txtClave.Text = gridConceptos.SelectedRows[0].Cells[0].Value.ToString();
            ////txtConcepto.Text = gridConceptos.SelectedRows[0].Cells[1].Value.ToString();
            ////txtDescripcion.Text = gridConceptos.SelectedRows[0].Cells[1].Value.ToString();
            ////txtPrecioGasolina.Text = gridConceptos.SelectedRows[0].Cells[2].Value.ToString();
            ////txtPrecioDiesel.Text = gridConceptos.SelectedRows[0].Cells[3].Value.ToString();

            //txtCantidad.Focus();
            //txtCantidad.SelectAll();
            //gridConceptos.Visible = false;

        }

        private void gridConceptos_KeyUp(object sender, KeyEventArgs e)
        {
            //if (gridConceptos.SelectedRows.Count > 0)
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {

            //        isNewItem = true;
            //        txtConcepto.Text = "";
            //        txtCantidad.Text = "1";
            //        int row = gridConceptos.CurrentCell.RowIndex;
            //        MessageBox.Show(gridConceptos.Rows[row].Cells[2].Value.ToString());

            //        //txtPrecioGasolina.Text = gridConceptos.SelectedRows[0].Cells[2].Value.ToString();
            //        //txtPrecioDiesel.Text = gridConceptos.SelectedRows[0].Cells[3].Value.ToString();
            //        //txtClave.Text = gridConceptos.CurrentRow.Cells[0].Value.ToString();
            //        //txtConcepto.Text = gridConceptos.CurrentRow.Cells[1].Value.ToString();
            //        //txtPrecioGasolina.Text = gridConceptos.CurrentRow.Cells[2].Value.ToString();
            //        //txtPrecioDiesel.Text = gridConceptos.CurrentRow.Cells[3].Value.ToString();

            //        txtCantidad.Focus();
            //        txtCantidad.SelectAll();
            //        gridConceptos.Visible = false;

            //    }
            //}
        }


        private void gridConceptos_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                //if (gridConceptos.SelectedRows.Count > 0)
                //{
                //    DataGridViewRow dgr = gridConceptos.CurrentRow;
                //    //string column0 = dgr.Cells[0].Value.ToString();                                        
                //    txtConcepto.Text = "";
                //    txtCantidad.Text = "1";

                //    txtClave.Text = dgr.Cells[0].Value.ToString();
                //    txtConcepto.Text = dgr.Cells[1].Value.ToString();
                //    txtPrecioGasolina.Text = dgr.Cells[2].Value.ToString();
                //    txtPrecioDiesel.Text = dgr.Cells[3].Value.ToString();

                //    txtCantidad.Focus();
                //    txtCantidad.SelectAll();
                //gridConceptos.Visible = false;

                //    if (gridConceptos.SelectedRows.Count > 0)
                //    {
                //        txtConcepto.Text = "";
                //        txtCantidad.Text = "1";
                //        txtClave.Text = gridConceptos.CurrentRow.Cells[0].Value.ToString();
                //        txtConcepto.Text = gridConceptos.CurrentRow.Cells[1].Value.ToString();
                //        txtPrecioGasolina.Text = gridConceptos.CurrentRow.Cells[2].Value.ToString();
                //        txtPrecioDiesel.Text = gridConceptos.CurrentRow.Cells[3].Value.ToString();

                //        txtCantidad.Focus();
                //        txtCantidad.SelectAll();
                //        gridConceptos.Visible = false;
                //    }
                //  }
            }
        }

        private void calculateTotal(double desc, double iva)
        {
            double SubTotal = 0;
            double Total = 0;

            //double.TryParse(desc, ref desc);
            //double.TryParse(iva, ref iva);

            foreach (DataGridViewRow R in dataGridView1.Rows)
            {
                if (!R.IsNewRow)
                    SubTotal += Convert.ToDouble(R.Cells[4].Value);
            }

            Total = SubTotal; //(SubTotal - desc) + iva;

            txtGrandTotal.Text = Total.ToString("##,###,##0.00"); // Format((SubTotal - desc) + iva, "##,###,##0.0000")
        }

        private void addToCart(int isGasoline = 1)
        {
            bool Exists = false;
            gridConceptos.Visible = false;
            if (txtCantidad.Text.Trim() != "" && txtCantidad.Text != "0.00")
            {
                foreach (DataGridViewRow R in dataGridView1.Rows)
                {
                    if (!R.IsNewRow)
                        if (R.Cells[1].Value.ToString().Trim() == txtConcepto.Text.Trim())
                        {
                            Exists = true;
                        }
                }

                if (dataGridView1.Rows.Count > 0 && Exists == true)
                {
                    MessageBox.Show("Actualmente ya se encuentra el concepto " + txtDescripcion.Text + " en el detalle, favor de validar o modificar la cantidad", "Detalle concepto", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    decimal price = (isGasoline == 1) ? decimal.Parse(txtPrecioGasolina.Text) : decimal.Parse(txtPrecioDiesel.Text);
                    decimal total = decimal.Parse(txtCantidad.Text) * price;
                    decimal pGasolina = 0;
                    decimal pDiesel = 0;
                    if (isGasoline == 1)
                    {
                        pGasolina = price;
                    }
                    else
                    {
                        pDiesel = price;
                    }
                    dataGridView1.Rows.Add(txtClave.Text, txtConcepto.Text, decimal.Parse(txtCantidad.Text), price.ToString("##,###,##0.00"), total.ToString("##,###,##0.00"), pGasolina, pDiesel);
                    calculateTotal(0, 0);
                    txtConcepto.Text = "";
                    txtDescripcion.Text = "";
                    txtClave.Text = "";
                    txtDescripcion.Text = "";
                    txtCantidad.Text = "0.00";
                    txtPrecioDiesel.Text = "0.00";
                    txtPrecioGasolina.Text = "0.00";
                    txtConcepto.Focus();
                    dataGridView1.ClearSelection();
                    gridConceptos.Visible = false;
                    isNewItem = false;
                }
            }
        }

        private void btnAddGasoline_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtPrecioGasolina.Text))
            {
                addToCart();
            }
            else
            {
                MessageBox.Show("Debe agregar el precio de gasolina", "Precio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPrecioGasolina.Text = "0.00";
                txtPrecioGasolina.SelectAll();
            }

        }

        private void btnAddDiesel_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtPrecioDiesel.Text))
            {
                addToCart(0);
            }
            else
            {
                MessageBox.Show("Debe agregar el precio diesel", "Precio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPrecioDiesel.Text = "0.00";
                txtPrecioDiesel.SelectAll();
            }

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            decimal quantity = Convert.ToDecimal(dataGridView1.Rows[e.RowIndex].Cells[2].Value);
            decimal price = Convert.ToDecimal(dataGridView1.Rows[e.RowIndex].Cells[3].Value);
            decimal total = quantity * price;
            dataGridView1.Rows[e.RowIndex].Cells[4].Value = total.ToString("##,###,##0.00");
            calculateTotal(0, 0);
        }

        private PrintDocument printDocument1 = new PrintDocument();
        Bitmap memoryImage;
        private void CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            bool existisCustomer = new Customer().findExistsByName(txtCliente.Text);
            if (!existisCustomer)
            {
                ActiveCustomer._Cl_Nombre = txtCliente.Text;
                ActiveCustomer._Action = 1;
                bool _result = new Customer().Customer_IU();
            }

            var ClsDatos = new Conexion();

            if (dataGridView1.Rows.Count - 1 > 0)
            {
                string _folioDocumento = "0";
                string query = "SELECT dbo.CIntToChar(CASE WHEN Folio IS NULL THEN 0 ELSE Folio END + 1,10) AS FOLIO FROM Folios WHERE Tipo='Remision'";
                DataTable DTFolio = new DataTable();
                if (ClsDatos.CargaTabla(query, ref DTFolio))
                {
                    if (DTFolio.Rows.Count > 0)
                        _folioDocumento = DTFolio.Rows[0][0].ToString();
                    ActiveReferral._Rm_Folio = _folioDocumento;
                }
                int id = 1;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (!row.IsNewRow)
                    {
                        bool existisConcepto = new Concept().findExistsByConcept(row.Cells[1].Value.ToString());
                        if (!existisConcepto)
                        {
                            ActiveConcept._Id = -1;
                            ActiveConcept._Descripcion = row.Cells[1].Value.ToString();
                            ActiveConcept._PrecioGasolina = decimal.Parse(row.Cells[5].Value.ToString());
                            ActiveConcept._PrecioDiesel = decimal.Parse(row.Cells[6].Value.ToString());
                            ActiveConcept._Action = 1;
                            bool _result = new Concept().Concepto_IU();
                        }
                        else
                        {
                            ActiveConcept._Id = int.Parse(row.Cells[0].Value.ToString());
                            ActiveConcept._Descripcion = row.Cells[1].Value.ToString();
                        }

                        string queryRemision = "EXEC [dbo].[Remision_IU] '" + _folioDocumento + "', '', '" + rtbComentarios.Text + "', " + decimal.Parse(txtGrandTotal.Text) + ", '" + ActiveOperator._Login + "', " +
                            " " + id + ",  " + ActiveConcept._Id + ", '" + ActiveConcept._Descripcion + "', " + decimal.Parse(row.Cells[2].Value.ToString()) + ", " + decimal.Parse(row.Cells[3].Value.ToString()) + ", " + decimal.Parse(row.Cells[4].Value.ToString()) + ", '" + ActiveCustomer._Cl_Cve_Cliente + "' ";
                        ClsDatos.CargaComando(queryRemision);
                        id = id + 1;
                    }
                }

                if (ClsDatos.Execute())
                {
                    ActiveReferral._Rm_Folio = _folioDocumento;
                    ActiveReferral._Rm_Fecha = Referral.FechaAlta();
                    

                    MessageBox.Show("Remisión generado correctamente con el folio #" + _folioDocumento, "Remisión Generado!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FormPrint print = new FormPrint();
                    print.ShowDialog();
                    clearForm();
                }
                else
                {
                    MessageBox.Show(ClsDatos.MsjError, "Error Encontrado!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe capturar el detalle de la remisión", "Remisión Detalle!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clearForm();
        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {
            //if (String.IsNullOrEmpty(txtCliente.Text))
            //{
            //    gridCustomer.Visible = false;
            //}
            //else
            //{
            //    gridCustomer.Visible = true;
            //    searchCustomer(txtCliente.Text);
            //}
        }

        private void txtCliente_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            //{
            //    gridCustomer.DefaultCellStyle.SelectionBackColor = Color.WhiteSmoke;
            //    gridCustomer.Focus();
            //} else if (e.KeyCode == Keys.Tab)
            //{
            //    gridCustomer.Visible = false;
            //}

            //if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            //{
            //    if (!String.IsNullOrEmpty(txtCliente.Text))
            //    {

            //    }
            //}
        }

        private void txtCliente_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            //{
            //    gridCustomer.Focus();
            //}

            //if (e.KeyCode == Keys.Right)
            //{
            //    gridCustomer.Focus();
            //    //txtConcepto.Text = gridConceptos.SelectedCells[2].Value.ToString();
            //    gridCustomer.Visible = false;
            //}
        }

        private void txtCliente_Leave(object sender, EventArgs e)
        {
            // gridCustomer.Visible = false;
        }

        private void gridCustomer_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //txtConcepto.Text = "";
            //txtCantidad.Text = "1";
            ActiveCustomer._Cl_Cve_Cliente = gridCustomer.SelectedRows[0].Cells[0].Value.ToString();
            txtCliente.Text = gridCustomer.SelectedRows[0].Cells[1].Value.ToString();
            gridCustomer.Visible = false;
        }

        private void FormReferral_Click(object sender, EventArgs e)
        {
            gridCustomer.Visible = false;
            gridConceptos.Visible = false;
        }

        private void rtbComentarios_Click(object sender, EventArgs e)
        {
            gridConceptos.Visible = false;
            gridCustomer.Visible = false;
        }

        private void gridConceptos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void gridConceptos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (gridConceptos.SelectedRows.Count > 0)
            //{
            //    isNewItem = true;
            //    txtConcepto.Text = "";
            //    txtCantidad.Text = "1";
            //    txtClave.Text = gridConceptos.CurrentRow.Cells[0].Value.ToString();
            //    txtConcepto.Text = gridConceptos.CurrentRow.Cells[1].Value.ToString();
            //    txtPrecioGasolina.Text = gridConceptos.CurrentRow.Cells[2].Value.ToString();
            //    txtPrecioDiesel.Text = gridConceptos.CurrentRow.Cells[3].Value.ToString();

            //    txtCantidad.Focus();
            //    txtCantidad.SelectAll();
            //    gridConceptos.Visible = false;
            //}
        }

        private void txtConcepto_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtConcepto.Text))
            {
                var DT = new Concept().findByConcept(txtConcepto.Text);
                if (DT.Rows.Count > 0)
                {
                    txtCantidad.Text = "1";
                    txtClave.Text = DT.Rows[0][0].ToString();
                    txtConcepto.Text = DT.Rows[0][1].ToString();
                    txtPrecioGasolina.Text = DT.Rows[0][2].ToString();
                    txtPrecioDiesel.Text = DT.Rows[0][3].ToString();
                    txtCantidad.Focus();
                    txtCantidad.SelectAll();

                }
                else
                {
                    txtClave.Text = "0";
                    txtPrecioGasolina.Text = "0.00";
                    txtPrecioDiesel.Text = "0.00";
                }
            }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Application.StartupPath);
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //MessageBox.Show("Hola")
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            calculateTotal(0, 0);
        }

        //private void myWebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        //{
        //    myWebBrowser.Print();
        //}
    }
}
