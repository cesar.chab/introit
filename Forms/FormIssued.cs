﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using AppINTROIT.Modelos;
using AppINTROIT.Reports;
using CrystalDecisions.CrystalReports.Engine;

namespace AppINTROIT.Forms
{
    public partial class FormIssued : Form
    {
        public FormIssued()
        {
            InitializeComponent();
        }

        private void FormIssued_Load(object sender, EventArgs e)
        {

            txtCliente.AutoCompleteCustomSource = Customer.Autocomplete();
            txtCliente.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtCliente.AutoCompleteSource = AutoCompleteSource.CustomSource;
            ActiveReferral._Fecha_Inicio = txtDesde.Value;
            ActiveReferral._Fecha_Final = txtHasta.Value;
            ActiveReferral._Cl_Nombre = txtCliente.Text;

            gridRemisiones.DataSource = Referral.All();
            gridRemisiones.ClearSelection();
        }

        private void gridRemisiones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ActiveReferral._Rm_Folio = gridRemisiones.CurrentRow.Cells[0].Value.ToString();
            ActiveReferral._Rm_Total = Convert.ToDecimal(gridRemisiones.CurrentRow.Cells[5].Value.ToString());
            ActiveReferral._Rm_Comentario = gridRemisiones.CurrentRow.Cells[4].Value.ToString();
            FormReferralDetail detail = new FormReferralDetail();
            detail.Text = "Detalle";
            detail.ShowDialog();
        }

        private void gridRemisiones_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (gridRemisiones.Rows.Count > 0)
                gridRemisiones.Columns["Total"].DefaultCellStyle.Format = "##,###,##0.00";
            //gridRemisiones.DefaultCellStyle.BackColor = Color.Red;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (gridRemisiones.SelectedRows.Count > 0)
            {
                ActiveReferral._Rm_Folio = gridRemisiones.CurrentRow.Cells[0].Value.ToString(); 
                ActiveReferral._Rm_Fecha = Convert.ToDateTime(gridRemisiones.CurrentRow.Cells[3].Value.ToString());


                FormPrint print = new FormPrint();
                print.ShowDialog();
                // CRNote cRNote = new CRNote();
                //cRNote.Parameter_fecha//
                //cRNote.SetDataSource();

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para poder imprimir la remisión", "Seleccionar Fila", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            if (gridRemisiones.SelectedRows.Count > 0)
            {
                ActiveReferral._Rm_Folio = gridRemisiones.CurrentRow.Cells[0].Value.ToString();
                ActiveReferral._Estado = gridRemisiones.CurrentRow.Cells[6].Value.ToString();
                if (ActiveReferral._Estado.Equals("BAJA"))
                {
                    MessageBox.Show("La remisión actualmente estad dado de baja", "Actualmente cancelado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DialogResult result = MessageBox.Show("Desea cancelar la remisión con el folio " + ActiveReferral._Rm_Folio, "Cancelar Remisión", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (result == DialogResult.OK)
                    {
                        bool res = Referral.destroy();
                        if (res)
                        {
                            MessageBox.Show(ActiveReferral._Message, "Remision Cancelado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            gridRemisiones.DataSource = Referral.All();
                            gridRemisiones.ClearSelection();
                        }
                        else
                        {
                            MessageBox.Show("Detalle: " + ActiveReferral._Message + ", intente más tarde", "Error al cancelar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para poder imprimir la remisión", "Seleccionar Fila", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void gridRemisiones_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //foreach (DataGridViewRow row in gridRemisiones.Rows)
            //{            //Here 2 cell is target value and 1 cell is Volume
            //    if (row.Cells["Estado"].Value.Equals("BAJA"))
            //    {
            //        row.DefaultCellStyle.BackColor = Color.Red;
            //    }

            //}
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtDesde.Value > txtHasta.Value)
            {
                MessageBox.Show("La fecha final no puede ser menor a la fecha inicial", "Rango de fecha", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                ActiveReferral._Fecha_Inicio = txtDesde.Value;
                ActiveReferral._Fecha_Final = txtHasta.Value;
                ActiveReferral._Cl_Nombre = txtCliente.Text;

                gridRemisiones.DataSource = Referral.All();
                gridRemisiones.ClearSelection();
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}