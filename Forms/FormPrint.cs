﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using AppINTROIT.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    public partial class FormPrint : Form
    {
        private Button printButton = new Button();
        private PrintDocument printDocument1 = new PrintDocument();

        public FormPrint()
        {
            InitializeComponent();

            //printButton.Text = "Print Form";
            //printButton.Click += new EventHandler(printButton_Click);
            //printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            //this.Controls.Add(printButton);
        }

        private void FormPrint_Load(object sender, EventArgs e)
        {
            CRNote cR = new CRNote();


            string sqlHeader = "SELECT Rm_Folio, Rm_Fecha, Rm_Referencia, Rm_Comentario, Cl_Nombre AS Rm_Cliente, Rm_Importe FROM Remision_Encabezado rm INNER JOIN Empresa e ON rm.Em_Cve_Empresa = e.Em_Cve_Empresa " +
                " INNER JOIN Clientes c ON rm.Cl_Cve_Cliente = c.Cl_Cve_Cliente " +
                " WHERE Rm_Folio='" + ActiveReferral._Rm_Folio + "'";
            string sqlDetail = "SELECT Rm_ID, Rm_Concepto, Rm_Cantidad, Rm_Precio, Rm_Importe, Rm_Folio FROM Remision_Detalle WHERE Rm_Folio='" + ActiveReferral._Rm_Folio + "'";

            DS.DSCompany.DTRMDetailDataTable detailRows = new DS.DSCompany.DTRMDetailDataTable();
            DS.DSCompany.DTRMHeaderDataTable dTRMHeaders = new DS.DSCompany.DTRMHeaderDataTable();

            var DT = new DataTable();
            var DTH = new DataTable();
            var ClsDatos = new Conexion();
            decimal total = 0;
            string _customer = "";
            string _comments = "";

            if (ClsDatos.CargaTabla(sqlHeader, ref DTH))
            {
                // foreach (DataRow row in DTH.Rows)
                //{
                // dTRMHeaders.ImportRow(row);
                //}
                _customer = DTH.Rows[0]["Rm_Cliente"].ToString();
                _comments = DTH.Rows[0]["Rm_Comentario"].ToString();
               
            }

            if (ClsDatos.CargaTabla(sqlDetail, ref DT))
            {
                foreach(DataRow row in DT.Rows)
                {
                    total += Convert.ToDecimal(row["Rm_Importe"]);
                    detailRows.ImportRow(row);
                }

                
            }
            
           // cR.Database.Tables["DTRMHeader"].SetDataSource((DataTable)dTRMHeaders);
            cR.Database.Tables["DTRMDetail"].SetDataSource((DataTable)detailRows);
            cR.SetParameterValue("customer", _customer);
            cR.SetParameterValue("comments", _comments);
            cR.SetParameterValue("folio_remision", "RM" + ActiveReferral._Rm_Folio);
            cR.SetParameterValue("fecha_remision", ActiveReferral._Rm_Fecha);
            cR.SetParameterValue("total", total);

            crystalReportViewer1.ReportSource = cR;
            crystalReportViewer1.Refresh();



            //string directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //var ClsDatos = new Conexion();
            ////FuncionesGenerales func = new FuncionesGenerales();
            //string appPath = Application.StartupPath + @"\Template\Invoice.html";
            //StringBuilder Template = new StringBuilder();
            //StringBuilder Detail = new StringBuilder();
            //Template.Append(Helper.GetHTMLFromAddress(appPath));
            //Template.Replace("$TITLE$", "NOTA DE REMISIÓN");

            //string sqlHeader = "SELECT * FROM Remision_Encabezado rm INNER JOIN Empresa e ON rm.Em_Cve_Empresa = e.Em_Cve_Empresa " +
            //    " INNER JOIN Clientes c ON rm.Cl_Cve_Cliente = c.Cl_Cve_Cliente " +
            //    " WHERE Rm_Folio='" + ActiveReferral._Rm_Folio + "'";
            //string sqlDetail = "SELECT * FROM Remision_Detalle WHERE Rm_Folio='" + ActiveReferral._Rm_Folio + "'";

            //var DT = new DataTable();
            //var DTD = new DataTable();
            //if (ClsDatos.CargaTabla(sqlHeader, ref DT))
            //{
            //    if (DT.Rows.Count > 0)
            //    {
            //        string address = DT.Rows[0]["Em_Calle"].ToString() + " " + DT.Rows[0]["Em_Cruzamiento"].ToString() + " <br>" + DT.Rows[0]["Em_Numext"].ToString() +
            //            " " + DT.Rows[0]["Em_Numint"].ToString() + " " + DT.Rows[0]["Em_Colonia"].ToString() + " <br>" + DT.Rows[0]["Em_Localidad"].ToString() + " " + DT.Rows[0]["Em_Municipio"].ToString() +
            //            " " + DT.Rows[0]["Em_Ciudad"].ToString() + " " + DT.Rows[0]["Em_CodPost"].ToString() + " <br>" + DT.Rows[0]["Em_Ciudad"].ToString() + " " + DT.Rows[0]["Em_Estado"].ToString() + " " + DT.Rows[0]["Em_Pais"].ToString();


            //        Template.Replace("$COMPANY$", DT.Rows[0]["Em_Nombre"].ToString());
            //        Template.Replace("$CEDULA_COMPANY$", DT.Rows[0]["Em_RFC"].ToString());
            //        //Template.Replace("$DIRECCION_COMPANY$", address);
            //        //Template.Replace("$TELEFONO_COMPANY$", DT.Rows[0]["Em_Telefono"].ToString());
            //        //Template.Replace("$CORREO_COMPANY$", DT.Rows[0]["Em_Email"].ToString());
            //        Template.Replace("$NOMBRE_CLIENTE$", DT.Rows[0]["Cl_Nombre"].ToString());

            //        Template.Replace("$FOLIO$", "RM" + DT.Rows[0]["Rm_Folio"].ToString());
            //        Template.Replace("$FECHA$", DT.Rows[0]["Rm_Fecha"].ToString());
            //        Template.Replace("$CONDICION_VENTA$", DT.Rows[0]["Rm_Comentario"].ToString());

            //        // Detail
            //        if (ClsDatos.CargaTabla(sqlDetail, ref DTD))
            //        {
            //            if (DTD.Rows.Count > 0)
            //            {
            //                foreach (DataRow row in DTD.Rows)
            //                {
            //                    Detail.Append("<tr class='item'>");
            //                    Detail.Append("<td align='center'  style='width:100px!important;'>" + decimal.Parse(row["Rm_Cantidad"].ToString()).ToString("##,###,##0.00") + " </td>");
            //                    //Detail.Append("<td>" + row["Rm_ID"].ToString() + "</td>");
            //                    Detail.Append("<td align='left' style='text-align:left!important;'>" + row["Rm_Concepto"].ToString() + "</td>");                                
            //                    //Detail.Append("<td align='right'>" + decimal.Parse(row["Rm_Precio"].ToString()).ToString("##,###,##0.00") + "</td>");
            //                    Detail.Append("<td align='right'>" + decimal.Parse(row["Rm_Importe"].ToString()).ToString("##,###,##0.00") + "</td>");
            //                    Detail.Append("</tr>");
            //                }
            //                Template.Replace("$DETALLE$", Detail.ToString());
            //                Template.Replace("$TOTAL$", Decimal.Parse(DT.Rows[0]["Rm_Importe"].ToString()).ToString("##,###,##0.00"));
            //            }
            //        }

            //        webBrowser1.DocumentText = Template.ToString();
            //    }
            // }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            //webBrowser1.ShowPrintDialog();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //void printButton_Click(object sender, EventArgs e)
        //{
        //    CaptureScreen();
        //    printDocument1.Print();
        //}

        //Bitmap memoryImage;

        //private void CaptureScreen()
        //{
        //    Graphics myGraphics = this.CreateGraphics();
        //    Size s = this.Size;
        //    memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
        //    Graphics memoryGraphics = Graphics.FromImage(memoryImage);
        //    memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        //}

        //private void printDocument1_PrintPage(System.Object sender,
        //       System.Drawing.Printing.PrintPageEventArgs e)
        //{
        //    e.Graphics.DrawImage(memoryImage, 0, 0);
        //}

    }
}
