﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using AppINTROIT.Modelos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AppINTROIT.Forms
{
    public partial class FormFactory : Form
    {
        private System.IO.MemoryStream ms = new System.IO.MemoryStream();

        public FormFactory()
        {
            InitializeComponent();
            loadRegimenFiscalData();
            loadDataFactory();
        }

        private void loadRegimenFiscalData()
        {
            var DT = new TaxRegime().all();
            if (DT.Rows.Count > 0)
            {
                cboRegimen.DataSource = DT;
                cboRegimen.ValueMember = "Rf_Codigo";
                cboRegimen.DisplayMember = "Rf_Descripcion";
                cboRegimen.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Obtener la empresa por default 
        /// </summary>
        private void loadDataFactory()
        {
            var ClsDatos = new Conexion();
            string query = "SELECT * FROM Empresa WHERE Em_Cve_Empresa = '0001'";
            var DT = new DataTable();
            if (ClsDatos.CargaTabla(query, ref DT))
            {
                if (DT.Rows.Count > 0)
                {
                    ActiveCompany._Em_Cve_Empresa = DT.Rows[0][0].ToString();
                    txtNombreEmpresa.Text = DT.Rows[0][1].ToString();
                    txtCalle.Text = DT.Rows[0][2].ToString();
                    txtCruzamientos.Text = DT.Rows[0][3].ToString();
                    txtNumExterior.Text = DT.Rows[0][4].ToString();
                    txtNumInterior.Text = DT.Rows[0][5].ToString();
                    txtColonia.Text = DT.Rows[0][6].ToString();
                    txtLocalidad.Text = DT.Rows[0][7].ToString();
                    txtMunicipio.Text = DT.Rows[0][8].ToString();
                    txtCiudad.Text = DT.Rows[0][9].ToString();
                    txtEstado.Text = DT.Rows[0][10].ToString();
                    txtPais.Text = DT.Rows[0][11].ToString();
                    txtCodigoPostal.Text = DT.Rows[0][12].ToString();
                    txtTelefono.Text = DT.Rows[0][13].ToString();
                    txtRFC.Text = DT.Rows[0][14].ToString();
                    //txtCorreo.Text = DT.Rows[0][16].ToString();
                    if (!String.IsNullOrEmpty(DT.Rows[0][17].ToString()))
                    {
                        cboRegimen.SelectedValue = DT.Rows[0][17].ToString();
                    }
                   
                    txtRutaBackup.Text = DT.Rows[0][24].ToString();
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormFactory_Load(object sender, EventArgs e)
        {

        }       

        private void btnRuta_Click(object sender, EventArgs e)
        {
            if (FolderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRutaBackup.Text = FolderBrowserDialog1.SelectedPath;
            }
        }

       

        /// <summary>
        /// Funcion para validar el nombre de la empresa
        /// </summary>
        /// <returns></returns>
        private bool valEmpresa()
        {
            bool status = true;
            if (String.IsNullOrEmpty(txtNombreEmpresa.Text.Trim()))
            {
                errorProvider1.SetError(txtNombreEmpresa, "Nombre empresa requerido");
                status = false;
            }
            else
            {
                errorProvider1.SetError(txtNombreEmpresa, "");
            }

            return status;
        }  

        private void btnSave_Click(object sender, EventArgs e)
        {

            ActiveCompany._Em_Nombre = txtNombreEmpresa.Text;           
            ActiveCompany._Ruta_Backup = txtRutaBackup.Text;          
            ActiveCompany._Em_Calle = txtCalle.Text;
            ActiveCompany._Em_Cruzamiento = txtCruzamientos.Text;
            ActiveCompany._Em_Numext = txtNumExterior.Text;
            ActiveCompany._Em_Numint = txtNumInterior.Text;
            ActiveCompany._Em_CodPost = txtCodigoPostal.Text;
            ActiveCompany._Em_Colonia = txtColonia.Text;
            ActiveCompany._Em_Localidad = txtLocalidad.Text;
            ActiveCompany._Em_Municipio = txtMunicipio.Text;
            ActiveCompany._Em_Ciudad = txtCiudad.Text;
            ActiveCompany._Em_Pais = txtPais.Text;
            ActiveCompany._Em_Estado = txtEstado.Text;
            ActiveCompany._Em_RFC = txtRFC.Text;
            ActiveCompany._Em_CURP = "";
            ActiveCompany._Em_Telefono = txtTelefono.Text;
            if (!String.IsNullOrEmpty(cboRegimen.Text))
            {
                ActiveCompany._Em_Cve_Regimen_Fiscal = cboRegimen.SelectedValue.ToString();
                ActiveCompany._Em_Regimen_Fiscal = cboRegimen.Text;
            }
            else
            {
                ActiveCompany._Em_Cve_Regimen_Fiscal = "";
                ActiveCompany._Em_Regimen_Fiscal = "";
            }

            //Crear empresa
            var ClsDatos = new Conexion();
            String _sql = "EXEC [dbo].[Empresas_IU] '" + ActiveCompany._Em_Cve_Empresa + "', '" + ActiveCompany._Em_Nombre + "', '" + ActiveCompany._Em_Calle + "', " +
                "'" + ActiveCompany._Em_Cruzamiento + "', '" + ActiveCompany._Em_Numext + "', '" + ActiveCompany._Em_Numint + "', '" + ActiveCompany._Em_Colonia + "', " +
                "'" + ActiveCompany._Em_Localidad + "', '" + ActiveCompany._Em_Municipio + "', '" + ActiveCompany._Em_Ciudad + "', '" + ActiveCompany._Em_Estado + "', " +
                "'" + ActiveCompany._Em_Pais + "', '" + ActiveCompany._Em_CodPost + "', '" + ActiveCompany._Em_Telefono + "', '" + ActiveCompany._Em_RFC + "', " +
                "'" + ActiveCompany._Em_Email + "', '" + ActiveCompany._Em_Regimen_Fiscal + "', '" + ActiveCompany._Em_Cve_Regimen_Fiscal + "', null, '" + ActiveCompany._Em_Maneja_Impuesto + "', " +
                "'" + ActiveCompany._Em_Impuesto + "', " + ActiveCompany._Em_Impuesto_porcentaje + ", '" + ActiveCompany._Em_Moneda + "', '" + ActiveCompany._Modo_Busqueda + "', " +
                "'" + ActiveCompany._Ruta_Backup + "', null, 1, '" + ActiveCompany._Redondeo_Total + "', '" + ActiveOperator._Login + "', 1, 0, '', " +
                "'', '', null, '', 0, 0, '', '', '', '', '' ";
            var DT = new DataTable();
            if (ClsDatos.CargaTabla(_sql, ref DT))
            {
                if (DT.Rows.Count > 0)
                {
                    //Collection<SQLpar> Parametros = new Collection<SQLpar>();
                    //MemoryStream ms1 = new MemoryStream();
                    //var sqlpar = new Helper.SQLpar();
                    //sqlpar.StrNombreParametro = "@logo";
                    //sqlpar.DataType = SqlDbType.VarBinary;
                    //pictureBoxLogo.Image.Size.ToString();
                    //pictureBoxLogo.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //sqlpar.obj = ms1;
                    //Parametros.Add(sqlpar);
                    //string sqlUpdate = "UPDATE Empresa SET Em_Logo=@logo WHERE Em_Cve_Empresa='0001'";
                    //ClsDatos.CargaComando(sqlUpdate, Parametros);
                    //ClsDatos.Execute();
                    MessageBox.Show(DT.Rows[0][0].ToString(), "Actualización Exitoso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show(ClsDatos.MsjError, "Error generado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
