﻿using AppINTROIT.Entidades;
using AppINTROIT.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    public partial class FormConcept : Form
    {
        public FormConcept()
        {
            InitializeComponent();
            resetForm();
            loadConceptData();
        }

        private void resetForm()
        {
            txtDescripcion.Text = "";
            txtPGasolina.Text = "0.00";
            txtPDiesel.Text = "0.00";
            splitContainer1.Panel2Collapsed = true;
            ActiveConcept._Id = 0;
            ActiveConcept._Action = 0;
            ActiveConcept._Descripcion = "";
            ActiveConcept._PrecioDiesel = 0;
            ActiveConcept._PrecioGasolina = 0;
            btnRemove.Visible = true;
            btnActive.Visible = false;

        }

        private void loadConceptData()
        {
            var DT = Concept.All();
            DataGridView1.DataSource = DT;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            resetForm();
            splitContainer1.Panel2Collapsed = false;
            txtDescripcion.Focus();
            ActiveConcept._Action = 1; //Insert
            DataGridView1.ClearSelection();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            resetForm();
            DataGridView1.ClearSelection();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (DataGridView1.SelectedRows.Count > 0)
            {
                ActiveConcept._Action = 2;
                ActiveConcept._Id = (int)DataGridView1.CurrentRow.Cells[0].Value;
                txtDescripcion.Text = DataGridView1.CurrentRow.Cells[1].Value.ToString();
                txtPGasolina.Text= DataGridView1.CurrentRow.Cells[2].Value.ToString();
                txtPDiesel.Text = DataGridView1.CurrentRow.Cells[3].Value.ToString();
                splitContainer1.Panel2Collapsed = false;
                txtDescripcion.Focus();
                txtDescripcion.SelectAll();
            }
            else
            {
                MessageBox.Show("Selecciona una fila", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (DataGridView1.SelectedRows.Count > 0)
            {
                ActiveConcept._Action = 3;
                ActiveConcept._Id = (int)DataGridView1.CurrentRow.Cells[0].Value;
                DialogResult result = MessageBox.Show("Se procede a dar de baja al concepto\n ya no podra ser utilizado en el sistema", "Baja Concepto", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.OK)
                {
                    bool _result = new Concept().Concepto_IU();
                    if (_result)
                    {
                        MessageBox.Show(ActiveConcept._Message, "Concepto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        loadConceptData();
                        resetForm();
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecciona una fila", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            if (DataGridView1.SelectedRows.Count > 0)
            {
                ActiveConcept._Action = 4;
                ActiveConcept._Id = (int)DataGridView1.CurrentRow.Cells[0].Value;
                DialogResult result = MessageBox.Show("Se procede a activar el concepto", "Activar Concepto", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.OK)
                {
                    bool _result = new Concept().Concepto_IU();
                    if (_result)
                    {
                        MessageBox.Show(ActiveConcept._Message, "Concepto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        loadConceptData();
                        resetForm();
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecciona una fila", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView1.ClearSelection();
        }

        private void DataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (DataGridView1.Rows[e.RowIndex].Cells["Estado"].Value.ToString() == "BA")
                {
                    e.CellStyle.Font = new Font(Font.FontFamily, 8, FontStyle.Strikeout);
                }
            }
        }

        private void DataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            resetForm();
            ActiveConcept._Es_Cve_Estado = DataGridView1.CurrentRow.Cells[6].Value.ToString();
            if (ActiveConcept._Es_Cve_Estado == "BA")
            {
                btnRemove.Visible = false;
                btnEdit.Enabled = false;
                btnActive.Visible = true;
            }
            else
            {
                btnRemove.Visible = true;
                btnEdit.Enabled = true;
                btnActive.Visible = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            ActiveConcept._Descripcion = txtDescripcion.Text;
            ActiveConcept._PrecioGasolina = decimal.Parse(txtPGasolina.Text);
            ActiveConcept._PrecioDiesel = decimal.Parse(txtPDiesel.Text);
            
            if (String.IsNullOrEmpty(ActiveConcept._Descripcion) || String.IsNullOrEmpty(ActiveConcept._PrecioGasolina.ToString()) || String.IsNullOrEmpty(ActiveConcept._PrecioDiesel.ToString()))
            {
                MessageBox.Show("(*) Campos importantes", "Campos vacíos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                bool _result = new Concept().Concepto_IU();
                if (_result)
                {
                    MessageBox.Show(ActiveConcept._Message, "Concepto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loadConceptData();
                    resetForm();
                }
                else
                {
                    MessageBox.Show(ActiveConcept._Message, "Concept", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
