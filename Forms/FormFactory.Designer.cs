﻿namespace AppINTROIT.Forms
{
    partial class FormFactory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFactory));
            this.btnCerrar = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.FolderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ofdUser = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.txtNumInterior = new System.Windows.Forms.TextBox();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.txtNumExterior = new System.Windows.Forms.TextBox();
            this.txtCiudad = new System.Windows.Forms.TextBox();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCruzamientos = new System.Windows.Forms.TextBox();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRuta = new System.Windows.Forms.Button();
            this.txtRutaBackup = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.PanelLogo = new System.Windows.Forms.Panel();
            this.cboRegimen = new System.Windows.Forms.ComboBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.PanelBorder = new System.Windows.Forms.Panel();
            this.txtNombreEmpresa = new System.Windows.Forms.TextBox();
            this.lblempresa = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.PanelLogo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(81)))), ((int)(((byte)(120)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Ebrima", 12F);
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCerrar.Location = new System.Drawing.Point(24, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(35, 35);
            this.btnCerrar.TabIndex = 64;
            this.btnCerrar.Text = "X";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.White;
            this.Label3.Location = new System.Drawing.Point(65, 12);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(122, 31);
            this.Label3.TabIndex = 65;
            this.Label3.Text = "Empresa";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.SeaGreen;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(607, 611);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 38);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // ofdUser
            // 
            this.ofdUser.FileName = "openFileDialog1";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 65);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(725, 533);
            this.tabControl1.TabIndex = 66;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(81)))), ((int)(((byte)(120)))));
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.PanelLogo);
            this.tabPage1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(717, 507);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.panel12);
            this.panel3.Controls.Add(this.panel13);
            this.panel3.Controls.Add(this.panel17);
            this.panel3.Controls.Add(this.panel26);
            this.panel3.Controls.Add(this.panel27);
            this.panel3.Controls.Add(this.panel29);
            this.panel3.Controls.Add(this.panel30);
            this.panel3.Controls.Add(this.panel33);
            this.panel3.Controls.Add(this.panel34);
            this.panel3.Controls.Add(this.panel35);
            this.panel3.Controls.Add(this.panel36);
            this.panel3.Controls.Add(this.txtNumInterior);
            this.panel3.Controls.Add(this.txtEstado);
            this.panel3.Controls.Add(this.txtLocalidad);
            this.panel3.Controls.Add(this.txtNumExterior);
            this.panel3.Controls.Add(this.txtCiudad);
            this.panel3.Controls.Add(this.txtColonia);
            this.panel3.Controls.Add(this.txtCruzamientos);
            this.panel3.Controls.Add(this.txtPais);
            this.panel3.Controls.Add(this.txtMunicipio);
            this.panel3.Controls.Add(this.txtCodigoPostal);
            this.panel3.Controls.Add(this.txtCalle);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Location = new System.Drawing.Point(9, 174);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(705, 213);
            this.panel3.TabIndex = 596;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel12.Location = new System.Drawing.Point(574, 57);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(95, 2);
            this.panel12.TabIndex = 554;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel13.Location = new System.Drawing.Point(470, 149);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(199, 2);
            this.panel13.TabIndex = 554;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel17.Location = new System.Drawing.Point(470, 103);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(199, 2);
            this.panel17.TabIndex = 554;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel26.Location = new System.Drawing.Point(470, 57);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(95, 2);
            this.panel26.TabIndex = 554;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel27.Location = new System.Drawing.Point(258, 149);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(201, 2);
            this.panel27.TabIndex = 554;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel29.Location = new System.Drawing.Point(258, 103);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(201, 2);
            this.panel29.TabIndex = 554;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel30.Location = new System.Drawing.Point(258, 57);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(201, 2);
            this.panel30.TabIndex = 554;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel33.Location = new System.Drawing.Point(51, 196);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(201, 2);
            this.panel33.TabIndex = 554;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel34.Location = new System.Drawing.Point(48, 149);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(201, 2);
            this.panel34.TabIndex = 554;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel35.Location = new System.Drawing.Point(48, 103);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(201, 2);
            this.panel35.TabIndex = 554;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel36.Location = new System.Drawing.Point(48, 57);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(201, 2);
            this.panel36.TabIndex = 554;
            // 
            // txtNumInterior
            // 
            this.txtNumInterior.BackColor = System.Drawing.Color.White;
            this.txtNumInterior.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumInterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNumInterior.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNumInterior.ForeColor = System.Drawing.Color.Black;
            this.txtNumInterior.Location = new System.Drawing.Point(574, 34);
            this.txtNumInterior.Name = "txtNumInterior";
            this.txtNumInterior.Size = new System.Drawing.Size(95, 21);
            this.txtNumInterior.TabIndex = 4;
            // 
            // txtEstado
            // 
            this.txtEstado.BackColor = System.Drawing.Color.White;
            this.txtEstado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEstado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtEstado.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtEstado.ForeColor = System.Drawing.Color.Black;
            this.txtEstado.Location = new System.Drawing.Point(470, 126);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(199, 21);
            this.txtEstado.TabIndex = 10;
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.Color.White;
            this.txtLocalidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLocalidad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtLocalidad.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtLocalidad.ForeColor = System.Drawing.Color.Black;
            this.txtLocalidad.Location = new System.Drawing.Point(470, 80);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(199, 21);
            this.txtLocalidad.TabIndex = 7;
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.Color.White;
            this.txtNumExterior.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumExterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNumExterior.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNumExterior.ForeColor = System.Drawing.Color.Black;
            this.txtNumExterior.Location = new System.Drawing.Point(470, 34);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.Size = new System.Drawing.Size(95, 21);
            this.txtNumExterior.TabIndex = 3;
            // 
            // txtCiudad
            // 
            this.txtCiudad.BackColor = System.Drawing.Color.White;
            this.txtCiudad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCiudad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCiudad.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCiudad.ForeColor = System.Drawing.Color.Black;
            this.txtCiudad.Location = new System.Drawing.Point(258, 126);
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.Size = new System.Drawing.Size(201, 21);
            this.txtCiudad.TabIndex = 9;
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.Color.White;
            this.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtColonia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtColonia.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtColonia.ForeColor = System.Drawing.Color.Black;
            this.txtColonia.Location = new System.Drawing.Point(258, 80);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(201, 21);
            this.txtColonia.TabIndex = 6;
            // 
            // txtCruzamientos
            // 
            this.txtCruzamientos.BackColor = System.Drawing.Color.White;
            this.txtCruzamientos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCruzamientos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCruzamientos.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCruzamientos.ForeColor = System.Drawing.Color.Black;
            this.txtCruzamientos.Location = new System.Drawing.Point(258, 34);
            this.txtCruzamientos.Name = "txtCruzamientos";
            this.txtCruzamientos.Size = new System.Drawing.Size(201, 21);
            this.txtCruzamientos.TabIndex = 2;
            // 
            // txtPais
            // 
            this.txtPais.BackColor = System.Drawing.Color.White;
            this.txtPais.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPais.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtPais.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtPais.ForeColor = System.Drawing.Color.Black;
            this.txtPais.Location = new System.Drawing.Point(48, 172);
            this.txtPais.Name = "txtPais";
            this.txtPais.Size = new System.Drawing.Size(201, 21);
            this.txtPais.TabIndex = 11;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.Color.White;
            this.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMunicipio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtMunicipio.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtMunicipio.ForeColor = System.Drawing.Color.Black;
            this.txtMunicipio.Location = new System.Drawing.Point(48, 126);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(201, 21);
            this.txtMunicipio.TabIndex = 8;
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.BackColor = System.Drawing.Color.White;
            this.txtCodigoPostal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCodigoPostal.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCodigoPostal.ForeColor = System.Drawing.Color.Black;
            this.txtCodigoPostal.Location = new System.Drawing.Point(48, 80);
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.Size = new System.Drawing.Size(201, 21);
            this.txtCodigoPostal.TabIndex = 5;
            // 
            // txtCalle
            // 
            this.txtCalle.BackColor = System.Drawing.Color.White;
            this.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtCalle.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtCalle.ForeColor = System.Drawing.Color.Black;
            this.txtCalle.Location = new System.Drawing.Point(48, 34);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(201, 21);
            this.txtCalle.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(571, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 342;
            this.label1.Text = "Num. Interior";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(467, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 15);
            this.label8.TabIndex = 342;
            this.label8.Text = "Estado";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(467, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 15);
            this.label14.TabIndex = 342;
            this.label14.Text = "Localidad";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(467, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 15);
            this.label15.TabIndex = 342;
            this.label15.Text = "Num. Exterior";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(255, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 15);
            this.label16.TabIndex = 342;
            this.label16.Text = "Ciudad";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(255, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 15);
            this.label17.TabIndex = 342;
            this.label17.Text = "Colonia";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(255, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 15);
            this.label18.TabIndex = 342;
            this.label18.Text = "Cruzamientos";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label19.Location = new System.Drawing.Point(45, 154);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 15);
            this.label19.TabIndex = 342;
            this.label19.Text = "País";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(45, 108);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 15);
            this.label22.TabIndex = 342;
            this.label22.Text = "Municipio";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label32.Location = new System.Drawing.Point(45, 62);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 15);
            this.label32.TabIndex = 342;
            this.label32.Text = "Código Postal";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label33.Location = new System.Drawing.Point(45, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(33, 15);
            this.label33.TabIndex = 342;
            this.label33.Text = "Calle";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.btnRuta);
            this.panel2.Controls.Add(this.txtRutaBackup);
            this.panel2.Controls.Add(this.Label9);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.panel2.Location = new System.Drawing.Point(9, 396);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(705, 95);
            this.panel2.TabIndex = 594;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel4.Location = new System.Drawing.Point(87, 65);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(493, 2);
            this.panel4.TabIndex = 559;
            // 
            // btnRuta
            // 
            this.btnRuta.BackColor = System.Drawing.Color.Transparent;
            this.btnRuta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRuta.BackgroundImage")));
            this.btnRuta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRuta.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRuta.FlatAppearance.BorderSize = 2;
            this.btnRuta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRuta.ForeColor = System.Drawing.Color.White;
            this.btnRuta.Location = new System.Drawing.Point(48, 41);
            this.btnRuta.Name = "btnRuta";
            this.btnRuta.Size = new System.Drawing.Size(36, 29);
            this.btnRuta.TabIndex = 13;
            this.btnRuta.UseVisualStyleBackColor = false;
            // 
            // txtRutaBackup
            // 
            this.txtRutaBackup.BackColor = System.Drawing.Color.White;
            this.txtRutaBackup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRutaBackup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtRutaBackup.Enabled = false;
            this.txtRutaBackup.Font = new System.Drawing.Font("Ebrima", 12F);
            this.txtRutaBackup.Location = new System.Drawing.Point(87, 43);
            this.txtRutaBackup.Name = "txtRutaBackup";
            this.txtRutaBackup.Size = new System.Drawing.Size(490, 22);
            this.txtRutaBackup.TabIndex = 14;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.White;
            this.Label9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label9.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.Label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label9.Location = new System.Drawing.Point(44, 17);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(484, 21);
            this.Label9.TabIndex = 599;
            this.Label9.Text = "Seleccione una carpeta donde guardar las copias de seguridad\r\n";
            // 
            // PanelLogo
            // 
            this.PanelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelLogo.BackColor = System.Drawing.Color.White;
            this.PanelLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelLogo.BackgroundImage")));
            this.PanelLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelLogo.Controls.Add(this.cboRegimen);
            this.PanelLogo.Controls.Add(this.panel28);
            this.PanelLogo.Controls.Add(this.panel31);
            this.PanelLogo.Controls.Add(this.panel32);
            this.PanelLogo.Controls.Add(this.txtTelefono);
            this.PanelLogo.Controls.Add(this.txtRFC);
            this.PanelLogo.Controls.Add(this.label26);
            this.PanelLogo.Controls.Add(this.label29);
            this.PanelLogo.Controls.Add(this.label30);
            this.PanelLogo.Controls.Add(this.PanelBorder);
            this.PanelLogo.Controls.Add(this.txtNombreEmpresa);
            this.PanelLogo.Controls.Add(this.lblempresa);
            this.PanelLogo.Location = new System.Drawing.Point(9, 6);
            this.PanelLogo.Name = "PanelLogo";
            this.PanelLogo.Size = new System.Drawing.Size(705, 147);
            this.PanelLogo.TabIndex = 590;
            // 
            // cboRegimen
            // 
            this.cboRegimen.DropDownHeight = 100;
            this.cboRegimen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRegimen.DropDownWidth = 230;
            this.cboRegimen.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRegimen.Font = new System.Drawing.Font("Ebrima", 12F);
            this.cboRegimen.FormattingEnabled = true;
            this.cboRegimen.IntegralHeight = false;
            this.cboRegimen.Location = new System.Drawing.Point(48, 92);
            this.cboRegimen.Name = "cboRegimen";
            this.cboRegimen.Size = new System.Drawing.Size(400, 29);
            this.cboRegimen.TabIndex = 556;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel28.Location = new System.Drawing.Point(48, 121);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(400, 2);
            this.panel28.TabIndex = 561;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel31.Location = new System.Drawing.Point(459, 120);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(201, 2);
            this.panel31.TabIndex = 562;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.panel32.Location = new System.Drawing.Point(461, 63);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(201, 2);
            this.panel32.TabIndex = 563;
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.White;
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtTelefono.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtTelefono.ForeColor = System.Drawing.Color.Black;
            this.txtTelefono.Location = new System.Drawing.Point(457, 97);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(201, 21);
            this.txtTelefono.TabIndex = 557;
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.Color.White;
            this.txtRFC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRFC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtRFC.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtRFC.ForeColor = System.Drawing.Color.Black;
            this.txtRFC.Location = new System.Drawing.Point(461, 45);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(201, 21);
            this.txtRFC.TabIndex = 555;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label26.Location = new System.Drawing.Point(45, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 15);
            this.label26.TabIndex = 558;
            this.label26.Text = "Regimen Fiscal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label29.Location = new System.Drawing.Point(454, 78);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(57, 15);
            this.label29.TabIndex = 559;
            this.label29.Text = "Teléfono";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label30.Location = new System.Drawing.Point(458, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 15);
            this.label30.TabIndex = 560;
            this.label30.Text = "R.F.C";
            // 
            // PanelBorder
            // 
            this.PanelBorder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.PanelBorder.Location = new System.Drawing.Point(48, 64);
            this.PanelBorder.Name = "PanelBorder";
            this.PanelBorder.Size = new System.Drawing.Size(400, 2);
            this.PanelBorder.TabIndex = 554;
            // 
            // txtNombreEmpresa
            // 
            this.txtNombreEmpresa.BackColor = System.Drawing.Color.White;
            this.txtNombreEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombreEmpresa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtNombreEmpresa.Font = new System.Drawing.Font("Ebrima", 12F, System.Drawing.FontStyle.Bold);
            this.txtNombreEmpresa.ForeColor = System.Drawing.Color.Black;
            this.txtNombreEmpresa.Location = new System.Drawing.Point(48, 41);
            this.txtNombreEmpresa.Name = "txtNombreEmpresa";
            this.txtNombreEmpresa.Size = new System.Drawing.Size(400, 21);
            this.txtNombreEmpresa.TabIndex = 2;
            // 
            // lblempresa
            // 
            this.lblempresa.AutoSize = true;
            this.lblempresa.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Bold);
            this.lblempresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblempresa.Location = new System.Drawing.Point(45, 23);
            this.lblempresa.Name = "lblempresa";
            this.lblempresa.Size = new System.Drawing.Size(135, 15);
            this.lblempresa.TabIndex = 342;
            this.lblempresa.Text = "Nombre de tu Empresa";
            // 
            // FormFactory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(81)))), ((int)(((byte)(120)))));
            this.ClientSize = new System.Drawing.Size(749, 664);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.btnCerrar);
            this.Font = new System.Drawing.Font("Ebrima", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormFactory";
            this.Text = "FormFactory";
            this.Load += new System.EventHandler(this.FormFactory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.PanelLogo.ResumeLayout(false);
            this.PanelLogo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Button btnCerrar;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.OpenFileDialog ofdUser;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Panel panel12;
        internal System.Windows.Forms.Panel panel13;
        internal System.Windows.Forms.Panel panel17;
        internal System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Panel panel27;
        internal System.Windows.Forms.Panel panel29;
        internal System.Windows.Forms.Panel panel30;
        internal System.Windows.Forms.Panel panel33;
        internal System.Windows.Forms.Panel panel34;
        internal System.Windows.Forms.Panel panel35;
        internal System.Windows.Forms.Panel panel36;
        internal System.Windows.Forms.TextBox txtNumInterior;
        internal System.Windows.Forms.TextBox txtEstado;
        internal System.Windows.Forms.TextBox txtLocalidad;
        internal System.Windows.Forms.TextBox txtNumExterior;
        internal System.Windows.Forms.TextBox txtCiudad;
        internal System.Windows.Forms.TextBox txtColonia;
        internal System.Windows.Forms.TextBox txtCruzamientos;
        internal System.Windows.Forms.TextBox txtPais;
        internal System.Windows.Forms.TextBox txtMunicipio;
        internal System.Windows.Forms.TextBox txtCodigoPostal;
        internal System.Windows.Forms.TextBox txtCalle;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label label33;
        internal System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.Button btnRuta;
        internal System.Windows.Forms.TextBox txtRutaBackup;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel PanelLogo;
        internal System.Windows.Forms.ComboBox cboRegimen;
        internal System.Windows.Forms.Panel panel28;
        internal System.Windows.Forms.Panel panel31;
        internal System.Windows.Forms.Panel panel32;
        internal System.Windows.Forms.TextBox txtTelefono;
        internal System.Windows.Forms.TextBox txtRFC;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Panel PanelBorder;
        internal System.Windows.Forms.TextBox txtNombreEmpresa;
        internal System.Windows.Forms.Label lblempresa;
    }
}