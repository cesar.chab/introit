﻿using AppINTROIT.Entidades;
using AppINTROIT.Helpers;
using AppINTROIT.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    public partial class FormUsers : Form
    {
        public FormUsers()
        {
            InitializeComponent();
            loadUserData();
            resetForm();
        }

        private void resetForm()
        {
            txtName.Text = "";
            txtEmail.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
            cboProfile.SelectedIndex = -1;
            splitContainer1.Panel2Collapsed = true;
            ActiveOperator._Action = 0;
            ActiveOperator._Nombre = "";
            ActiveOperator._Login = "";
            ActiveOperator._Password = "";
            ActiveOperator._Rol = "";
            ActiveOperator.Correo = "";
            ActiveOperator._Message = "";
            btnRemove.Visible = true;
            btnActive.Visible = false;

        }

        private void loadUserData()
        {
            var DT = new Operator().All();
            DataGridView1.DataSource = DT;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormUsers_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            resetForm();
            splitContainer1.Panel2Collapsed = false;
            txtName.Focus();
            ActiveOperator._Action = 1; //Insert
            DataGridView1.ClearSelection();

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            resetForm();
            DataGridView1.ClearSelection();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(cboProfile.Text))
            {
                MessageBox.Show("(*) Campos importantes", "Campos vacíos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            ActiveOperator._Nombre = txtName.Text;
            ActiveOperator._Login = txtUsername.Text;
            ActiveOperator._Password = txtPassword.Text;
            ActiveOperator._Rol = cboProfile.SelectedItem.ToString();
            ActiveOperator.Correo = txtEmail.Text;

            if (String.IsNullOrEmpty(ActiveOperator._Rol) || String.IsNullOrEmpty(ActiveOperator._Nombre) || String.IsNullOrEmpty(ActiveOperator._Login)
                || String.IsNullOrEmpty(ActiveOperator._Password))
            {
                MessageBox.Show("(*) Campos importantes", "Campos vacíos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                bool _result = new Operator().OperadoresIU();
                if (_result)
                {
                    MessageBox.Show(ActiveOperator._Message, "Operador", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loadUserData();
                    resetForm();
                }
                else
                {
                    MessageBox.Show(ActiveOperator._Message, "Operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (DataGridView1.SelectedRows.Count > 0)
            {
                ActiveOperator._Action = 2;
                ActiveOperator._Id = (int)DataGridView1.CurrentRow.Cells[0].Value;
                txtName.Text = DataGridView1.CurrentRow.Cells[1].Value.ToString();
                txtUsername.Text = DataGridView1.CurrentRow.Cells[2].Value.ToString();
                txtPassword.Text = Helper.DeEncripta(DataGridView1.CurrentRow.Cells[3].Value.ToString());
                txtEmail.Text = DataGridView1.CurrentRow.Cells[4].Value.ToString();
                cboProfile.Text = DataGridView1.CurrentRow.Cells[5].Value.ToString();
                splitContainer1.Panel2Collapsed = false;
                txtName.Focus();
                txtName.SelectAll();
            }
            else
            {
                MessageBox.Show("Selecciona una fila", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (DataGridView1.SelectedRows.Count > 0)
            {
                ActiveOperator._Action = 3;
                ActiveOperator._Id = (int)DataGridView1.CurrentRow.Cells[0].Value;
                DialogResult result = MessageBox.Show("Se procede a inactivar al operador\n ya no tendra acceso al sistema", "Inactivar Operador", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.OK)
                {
                    bool _result = new Operator().OperadoresIU();
                    if (_result)
                    {
                        MessageBox.Show(ActiveOperator._Message, "Operador", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        loadUserData();
                        resetForm();
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecciona una fila", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView1.Columns["Password"].Visible = false;
            DataGridView1.ClearSelection();
        }

        private void DataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (DataGridView1.Rows[e.RowIndex].Cells["Estado"].Value.ToString() == "BA")
                {
                    e.CellStyle.Font = new Font(Font.FontFamily, 8, FontStyle.Strikeout);
                }
            }
        }

        private void DataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            resetForm();
            ActiveOperator._Es_Cve_Estado = DataGridView1.CurrentRow.Cells[6].Value.ToString();
            if (ActiveOperator._Es_Cve_Estado == "BA")
            {
                btnRemove.Visible = false;
                btnEdit.Enabled = false;
                btnActive.Visible = true;

            }
            else
            {
                btnRemove.Visible = true;
                btnEdit.Enabled = true;
                btnActive.Visible = false;
            }

        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            if (DataGridView1.SelectedRows.Count > 0)
            {
                ActiveOperator._Action = 4;
                ActiveOperator._Id = (int)DataGridView1.CurrentRow.Cells[0].Value;
                DialogResult result = MessageBox.Show("Se procede a activar al operador\n tendra acceso al sistema", "Activar Operador", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.OK)
                {
                    bool _result = new Operator().OperadoresIU();
                    if (_result)
                    {
                        MessageBox.Show(ActiveOperator._Message, "Operador", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        loadUserData();
                        resetForm();
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecciona una fila", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
