﻿
namespace AppINTROIT.Forms
{
    partial class FormReferral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReferral));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.txtClave = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbComentarios = new System.Windows.Forms.RichTextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.Label38 = new System.Windows.Forms.Label();
            this.btnAddDiesel = new System.Windows.Forms.Button();
            this.btnAddGasoline = new System.Windows.Forms.Button();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.Label39 = new System.Windows.Forms.Label();
            this.Label43 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Concepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PGasolina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDiesel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPrecioDiesel = new System.Windows.Forms.TextBox();
            this.txtConcepto = new System.Windows.Forms.TextBox();
            this.Label37 = new System.Windows.Forms.Label();
            this.txtPrecioGasolina = new System.Windows.Forms.TextBox();
            this.Label34 = new System.Windows.Forms.Label();
            this.gridConceptos = new System.Windows.Forms.DataGridView();
            this.gridCustomer = new System.Windows.Forms.DataGridView();
            this.Label3 = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 65);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1042, 529);
            this.tabControl1.TabIndex = 69;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(81)))), ((int)(((byte)(120)))));
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtCliente);
            this.tabPage1.Controls.Add(this.txtClave);
            this.tabPage1.Controls.Add(this.txtDescripcion);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.rtbComentarios);
            this.tabPage1.Controls.Add(this.btnCancel);
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Controls.Add(this.Label38);
            this.tabPage1.Controls.Add(this.btnAddDiesel);
            this.tabPage1.Controls.Add(this.btnAddGasoline);
            this.tabPage1.Controls.Add(this.txtGrandTotal);
            this.tabPage1.Controls.Add(this.txtCantidad);
            this.tabPage1.Controls.Add(this.Label39);
            this.tabPage1.Controls.Add(this.Label43);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.txtPrecioDiesel);
            this.tabPage1.Controls.Add(this.txtConcepto);
            this.tabPage1.Controls.Add(this.Label37);
            this.tabPage1.Controls.Add(this.txtPrecioGasolina);
            this.tabPage1.Controls.Add(this.Label34);
            this.tabPage1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1034, 503);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Ebrima", 10F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(420, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 19);
            this.label4.TabIndex = 63;
            this.label4.Text = "Condiciones:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Ebrima", 10F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(19, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 63;
            this.label2.Text = "Cliente:";
            // 
            // txtCliente
            // 
            this.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCliente.Font = new System.Drawing.Font("Ebrima", 10F);
            this.txtCliente.Location = new System.Drawing.Point(23, 37);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(386, 26);
            this.txtCliente.TabIndex = 1;
            this.txtCliente.TextChanged += new System.EventHandler(this.txtCliente_TextChanged);
            this.txtCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCliente_KeyDown);
            this.txtCliente.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCliente_KeyUp);
            this.txtCliente.Leave += new System.EventHandler(this.txtCliente_Leave);
            // 
            // txtClave
            // 
            this.txtClave.Location = new System.Drawing.Point(238, 6);
            this.txtClave.Name = "txtClave";
            this.txtClave.Size = new System.Drawing.Size(100, 22);
            this.txtClave.TabIndex = 60;
            this.txtClave.TabStop = false;
            this.txtClave.Visible = false;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(110, 6);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(100, 22);
            this.txtDescripcion.TabIndex = 60;
            this.txtDescripcion.TabStop = false;
            this.txtDescripcion.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Ebrima", 10F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(30, 415);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 19);
            this.label1.TabIndex = 61;
            this.label1.Text = "* Precios más I.V.A";
            // 
            // rtbComentarios
            // 
            this.rtbComentarios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbComentarios.Font = new System.Drawing.Font("Ebrima", 10F);
            this.rtbComentarios.Location = new System.Drawing.Point(420, 34);
            this.rtbComentarios.Name = "rtbComentarios";
            this.rtbComentarios.Size = new System.Drawing.Size(594, 26);
            this.rtbComentarios.TabIndex = 2;
            this.rtbComentarios.Text = "";
            this.rtbComentarios.Click += new System.EventHandler(this.rtbComentarios_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.DarkGray;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Ebrima", 10F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(746, 458);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 39);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.SeaGreen;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Ebrima", 10F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(884, 458);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 39);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.BackColor = System.Drawing.Color.Transparent;
            this.Label38.Font = new System.Drawing.Font("Ebrima", 10F);
            this.Label38.ForeColor = System.Drawing.Color.White;
            this.Label38.Location = new System.Drawing.Point(420, 66);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(67, 19);
            this.Label38.TabIndex = 57;
            this.Label38.Text = "Cantidad:";
            // 
            // btnAddDiesel
            // 
            this.btnAddDiesel.BackColor = System.Drawing.Color.SeaGreen;
            this.btnAddDiesel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddDiesel.BackgroundImage")));
            this.btnAddDiesel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddDiesel.FlatAppearance.BorderSize = 0;
            this.btnAddDiesel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddDiesel.ForeColor = System.Drawing.Color.White;
            this.btnAddDiesel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddDiesel.Location = new System.Drawing.Point(883, 75);
            this.btnAddDiesel.Name = "btnAddDiesel";
            this.btnAddDiesel.Size = new System.Drawing.Size(131, 39);
            this.btnAddDiesel.TabIndex = 8;
            this.btnAddDiesel.Text = "Agregar con precio Diesel";
            this.btnAddDiesel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddDiesel.UseVisualStyleBackColor = false;
            this.btnAddDiesel.Click += new System.EventHandler(this.btnAddDiesel_Click);
            // 
            // btnAddGasoline
            // 
            this.btnAddGasoline.BackColor = System.Drawing.Color.SeaGreen;
            this.btnAddGasoline.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddGasoline.BackgroundImage")));
            this.btnAddGasoline.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddGasoline.FlatAppearance.BorderSize = 0;
            this.btnAddGasoline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddGasoline.ForeColor = System.Drawing.Color.White;
            this.btnAddGasoline.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddGasoline.Location = new System.Drawing.Point(736, 75);
            this.btnAddGasoline.Name = "btnAddGasoline";
            this.btnAddGasoline.Size = new System.Drawing.Size(131, 39);
            this.btnAddGasoline.TabIndex = 7;
            this.btnAddGasoline.Text = "Agregar con precio Gasolina";
            this.btnAddGasoline.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddGasoline.UseVisualStyleBackColor = false;
            this.btnAddGasoline.Click += new System.EventHandler(this.btnAddGasoline_Click);
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGrandTotal.Enabled = false;
            this.txtGrandTotal.Font = new System.Drawing.Font("Ebrima", 13F, System.Drawing.FontStyle.Bold);
            this.txtGrandTotal.Location = new System.Drawing.Point(856, 408);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.Size = new System.Drawing.Size(158, 30);
            this.txtGrandTotal.TabIndex = 10;
            this.txtGrandTotal.Text = "0.00";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Font = new System.Drawing.Font("Ebrima", 10F);
            this.txtCantidad.Location = new System.Drawing.Point(420, 88);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(87, 26);
            this.txtCantidad.TabIndex = 4;
            this.txtCantidad.Text = "0.00";
            // 
            // Label39
            // 
            this.Label39.AutoSize = true;
            this.Label39.BackColor = System.Drawing.Color.Transparent;
            this.Label39.Font = new System.Drawing.Font("Ebrima", 10F);
            this.Label39.ForeColor = System.Drawing.Color.White;
            this.Label39.Location = new System.Drawing.Point(620, 66);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(89, 19);
            this.Label39.TabIndex = 58;
            this.Label39.Text = "Precio Diesel:";
            // 
            // Label43
            // 
            this.Label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Label43.AutoSize = true;
            this.Label43.BackColor = System.Drawing.Color.Transparent;
            this.Label43.Font = new System.Drawing.Font("Ebrima", 13F, System.Drawing.FontStyle.Bold);
            this.Label43.ForeColor = System.Drawing.Color.White;
            this.Label43.Location = new System.Drawing.Point(763, 411);
            this.Label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(80, 25);
            this.Label43.TabIndex = 52;
            this.Label43.Text = "TOTAL :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Ebrima", 8F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 40;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Concepto,
            this.Cantidad,
            this.Precio,
            this.Importe,
            this.PGasolina,
            this.PDiesel});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView1.Location = new System.Drawing.Point(23, 121);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 40;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Ebrima", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowTemplate.Height = 35;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(991, 281);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView1_UserDeletedRow);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // Concepto
            // 
            this.Concepto.FillWeight = 360F;
            this.Concepto.HeaderText = "Concepto";
            this.Concepto.MinimumWidth = 360;
            this.Concepto.Name = "Concepto";
            this.Concepto.ReadOnly = true;
            this.Concepto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Concepto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Cantidad
            // 
            this.Cantidad.FillWeight = 65.65144F;
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            // 
            // Precio
            // 
            this.Precio.FillWeight = 65.65144F;
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            // 
            // Importe
            // 
            this.Importe.FillWeight = 65.65144F;
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.ReadOnly = true;
            // 
            // PGasolina
            // 
            this.PGasolina.HeaderText = "PGasolina";
            this.PGasolina.Name = "PGasolina";
            this.PGasolina.Visible = false;
            // 
            // PDiesel
            // 
            this.PDiesel.HeaderText = "PDiesel";
            this.PDiesel.Name = "PDiesel";
            this.PDiesel.Visible = false;
            // 
            // txtPrecioDiesel
            // 
            this.txtPrecioDiesel.Font = new System.Drawing.Font("Ebrima", 10F);
            this.txtPrecioDiesel.Location = new System.Drawing.Point(620, 88);
            this.txtPrecioDiesel.Name = "txtPrecioDiesel";
            this.txtPrecioDiesel.Size = new System.Drawing.Size(101, 26);
            this.txtPrecioDiesel.TabIndex = 6;
            this.txtPrecioDiesel.Text = "0.00";
            // 
            // txtConcepto
            // 
            this.txtConcepto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtConcepto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConcepto.Font = new System.Drawing.Font("Ebrima", 10F);
            this.txtConcepto.Location = new System.Drawing.Point(23, 88);
            this.txtConcepto.Name = "txtConcepto";
            this.txtConcepto.Size = new System.Drawing.Size(386, 26);
            this.txtConcepto.TabIndex = 3;
            this.txtConcepto.TextChanged += new System.EventHandler(this.txtConcepto_TextChanged);
            this.txtConcepto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtConcepto_KeyDown);
            this.txtConcepto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtConcepto_KeyUp);
            this.txtConcepto.Leave += new System.EventHandler(this.txtConcepto_Leave);
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.BackColor = System.Drawing.Color.Transparent;
            this.Label37.Font = new System.Drawing.Font("Ebrima", 10F);
            this.Label37.ForeColor = System.Drawing.Color.White;
            this.Label37.Location = new System.Drawing.Point(509, 66);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(105, 19);
            this.Label37.TabIndex = 59;
            this.Label37.Text = "Precio Gasolina:";
            // 
            // txtPrecioGasolina
            // 
            this.txtPrecioGasolina.Font = new System.Drawing.Font("Ebrima", 10F);
            this.txtPrecioGasolina.Location = new System.Drawing.Point(513, 88);
            this.txtPrecioGasolina.Name = "txtPrecioGasolina";
            this.txtPrecioGasolina.Size = new System.Drawing.Size(101, 26);
            this.txtPrecioGasolina.TabIndex = 5;
            this.txtPrecioGasolina.Text = "0.00";
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.BackColor = System.Drawing.Color.Transparent;
            this.Label34.Font = new System.Drawing.Font("Ebrima", 10F);
            this.Label34.ForeColor = System.Drawing.Color.White;
            this.Label34.Location = new System.Drawing.Point(19, 66);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(71, 19);
            this.Label34.TabIndex = 22;
            this.Label34.Text = "Concepto:";
            // 
            // gridConceptos
            // 
            this.gridConceptos.AllowUserToAddRows = false;
            this.gridConceptos.AllowUserToDeleteRows = false;
            this.gridConceptos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridConceptos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gridConceptos.BackgroundColor = System.Drawing.Color.White;
            this.gridConceptos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridConceptos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridConceptos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Ebrima", 8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridConceptos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridConceptos.ColumnHeadersHeight = 40;
            this.gridConceptos.EnableHeadersVisualStyles = false;
            this.gridConceptos.GridColor = System.Drawing.Color.WhiteSmoke;
            this.gridConceptos.Location = new System.Drawing.Point(562, 21);
            this.gridConceptos.MultiSelect = false;
            this.gridConceptos.Name = "gridConceptos";
            this.gridConceptos.ReadOnly = true;
            this.gridConceptos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridConceptos.RowHeadersVisible = false;
            this.gridConceptos.RowHeadersWidth = 40;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Ebrima", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.gridConceptos.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.gridConceptos.RowTemplate.Height = 35;
            this.gridConceptos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridConceptos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridConceptos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridConceptos.Size = new System.Drawing.Size(386, 26);
            this.gridConceptos.TabIndex = 2;
            this.gridConceptos.TabStop = false;
            this.gridConceptos.Visible = false;
            this.gridConceptos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridConceptos_CellDoubleClick);
            this.gridConceptos.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridConceptos_CellMouseClick);
            this.gridConceptos.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridConceptos_CellMouseDoubleClick);
            this.gridConceptos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridConceptos_KeyDown);
            this.gridConceptos.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridConceptos_KeyUp);
            // 
            // gridCustomer
            // 
            this.gridCustomer.AllowUserToAddRows = false;
            this.gridCustomer.AllowUserToDeleteRows = false;
            this.gridCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridCustomer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gridCustomer.BackgroundColor = System.Drawing.Color.White;
            this.gridCustomer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridCustomer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridCustomer.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Ebrima", 8F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridCustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridCustomer.ColumnHeadersHeight = 40;
            this.gridCustomer.EnableHeadersVisualStyles = false;
            this.gridCustomer.GridColor = System.Drawing.Color.WhiteSmoke;
            this.gridCustomer.Location = new System.Drawing.Point(298, 21);
            this.gridCustomer.MultiSelect = false;
            this.gridCustomer.Name = "gridCustomer";
            this.gridCustomer.ReadOnly = true;
            this.gridCustomer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridCustomer.RowHeadersVisible = false;
            this.gridCustomer.RowHeadersWidth = 40;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Ebrima", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.gridCustomer.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.gridCustomer.RowTemplate.Height = 35;
            this.gridCustomer.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridCustomer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCustomer.Size = new System.Drawing.Size(236, 26);
            this.gridCustomer.TabIndex = 64;
            this.gridCustomer.TabStop = false;
            this.gridCustomer.Visible = false;
            this.gridCustomer.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridCustomer_CellContentDoubleClick);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.White;
            this.Label3.Location = new System.Drawing.Point(65, 12);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(127, 31);
            this.Label3.TabIndex = 68;
            this.Label3.Text = "Remisión";
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(81)))), ((int)(((byte)(120)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Ebrima", 12F);
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCerrar.Location = new System.Drawing.Point(24, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(35, 35);
            this.btnCerrar.TabIndex = 67;
            this.btnCerrar.Text = "X";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // Timer1
            // 
            this.Timer1.Interval = 10;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 7;
            this.bunifuElipse1.TargetControl = this.dataGridView1;
            // 
            // FormReferral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(81)))), ((int)(((byte)(120)))));
            this.ClientSize = new System.Drawing.Size(1066, 606);
            this.Controls.Add(this.gridCustomer);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.gridConceptos);
            this.Font = new System.Drawing.Font("Ebrima", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormReferral";
            this.Text = "FormReferral";
            this.Load += new System.EventHandler(this.FormReferral_Load);
            this.Click += new System.EventHandler(this.FormReferral_Click);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button btnCerrar;
        internal System.Windows.Forms.Label Label34;
        private System.Windows.Forms.DataGridView dataGridView1;
        internal System.Windows.Forms.Timer Timer1;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        internal System.Windows.Forms.TextBox txtGrandTotal;
        internal System.Windows.Forms.Label Label43;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Button btnAddGasoline;
        internal System.Windows.Forms.TextBox txtCantidad;
        internal System.Windows.Forms.Label Label39;
        internal System.Windows.Forms.TextBox txtPrecioDiesel;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.TextBox txtPrecioGasolina;
        public System.Windows.Forms.TextBox txtConcepto;
        private System.Windows.Forms.DataGridView gridConceptos;
        internal System.Windows.Forms.Button btnAddDiesel;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtClave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RichTextBox rtbComentarios;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCliente;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn PGasolina;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDiesel;
    }
}