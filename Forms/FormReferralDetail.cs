﻿using AppINTROIT.Entidades;
using AppINTROIT.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    public partial class FormReferralDetail : Form
    {
        public FormReferralDetail()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormReferralDetail_Load(object sender, EventArgs e)
        {
            lblFolio.Text = "RM" + ActiveReferral._Rm_Folio;
            txtNota.Text = ActiveReferral._Rm_Comentario;
            gridDetalle.DataSource = Referral.Detail();
            gridDetalle.ClearSelection();
            txtGrandTotal.Text = ActiveReferral._Rm_Total.ToString("##,###,##0.00");
        }

        private void gridDetalle_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

            gridDetalle.Columns["Cantidad"].DefaultCellStyle.Format = "##,###,##0.00";
            gridDetalle.Columns["Precio"].DefaultCellStyle.Format = "##,###,##0.00";
            gridDetalle.Columns["Importe"].DefaultCellStyle.Format = "##,###,##0.00";
        }
    }
}
