﻿using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    partial class FormMain : Form
    {
        //Form reemplaza a hide para limpiar la lista de componentes.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.PanelBarraTitulo = new System.Windows.Forms.Panel();
            this.lblHora = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.lblLogin = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.splitMenu = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panelForms = new System.Windows.Forms.Panel();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnFactory = new System.Windows.Forms.Button();
            this.btnUser = new System.Windows.Forms.Button();
            this.btnConcept = new System.Windows.Forms.Button();
            this.btnRemisiones = new System.Windows.Forms.Button();
            this.btnIssued = new System.Windows.Forms.Button();
            this.btnMenu = new System.Windows.Forms.Button();
            this.PanelBarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMenu)).BeginInit();
            this.splitMenu.Panel1.SuspendLayout();
            this.splitMenu.Panel2.SuspendLayout();
            this.splitMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelBarraTitulo
            // 
            this.PanelBarraTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.PanelBarraTitulo.Controls.Add(this.lblHora);
            this.PanelBarraTitulo.Controls.Add(this.btnCerrar);
            this.PanelBarraTitulo.Controls.Add(this.btnMenu);
            this.PanelBarraTitulo.Controls.Add(this.lblLogin);
            this.PanelBarraTitulo.Controls.Add(this.Label2);
            this.PanelBarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelBarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.PanelBarraTitulo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelBarraTitulo.Name = "PanelBarraTitulo";
            this.PanelBarraTitulo.Size = new System.Drawing.Size(824, 39);
            this.PanelBarraTitulo.TabIndex = 1;
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Font = new System.Drawing.Font("Ebrima", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.LightGray;
            this.lblHora.Location = new System.Drawing.Point(227, 11);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(71, 20);
            this.lblHora.TabIndex = 71;
            this.lblHora.Text = "00:00:00";
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.btnCerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Ebrima", 12F);
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCerrar.Location = new System.Drawing.Point(789, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(35, 39);
            this.btnCerrar.TabIndex = 70;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Text = "X";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Ebrima", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.ForeColor = System.Drawing.Color.LightGray;
            this.lblLogin.Location = new System.Drawing.Point(183, 10);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(0, 20);
            this.lblLogin.TabIndex = 4;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Ebrima", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.LightGray;
            this.Label2.Location = new System.Drawing.Point(52, 11);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(165, 20);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "Bienvenido al sistema:";
            // 
            // splitMenu
            // 
            this.splitMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMenu.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitMenu.Location = new System.Drawing.Point(0, 39);
            this.splitMenu.Name = "splitMenu";
            // 
            // splitMenu.Panel1
            // 
            this.splitMenu.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.splitMenu.Panel1.Controls.Add(this.label1);
            this.splitMenu.Panel1.Controls.Add(this.groupBox1);
            this.splitMenu.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitMenu_Panel1_Paint);
            this.splitMenu.Panel1.Leave += new System.EventHandler(this.splitMenu_Panel1_Leave);
            // 
            // splitMenu.Panel2
            // 
            this.splitMenu.Panel2.Controls.Add(this.panelForms);
            this.splitMenu.Panel2.MouseHover += new System.EventHandler(this.splitMenu_Panel2_MouseHover);
            this.splitMenu.Size = new System.Drawing.Size(824, 465);
            this.splitMenu.SplitterDistance = 190;
            this.splitMenu.SplitterWidth = 1;
            this.splitMenu.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Ebrima", 12F);
            this.label1.ForeColor = System.Drawing.Color.LightGray;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 38);
            this.label1.TabIndex = 5;
            this.label1.Text = "Menu";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.Font = new System.Drawing.Font("Ebrima", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(9, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(170, 270);
            this.groupBox1.TabIndex = 590;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generales";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnFactory);
            this.flowLayoutPanel1.Controls.Add(this.btnUser);
            this.flowLayoutPanel1.Controls.Add(this.btnConcept);
            this.flowLayoutPanel1.Controls.Add(this.btnRemisiones);
            this.flowLayoutPanel1.Controls.Add(this.btnIssued);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 25);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(164, 242);
            this.flowLayoutPanel1.TabIndex = 603;
            // 
            // panelForms
            // 
            this.panelForms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.panelForms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForms.Location = new System.Drawing.Point(0, 0);
            this.panelForms.Name = "panelForms";
            this.panelForms.Size = new System.Drawing.Size(633, 465);
            this.panelForms.TabIndex = 0;
            this.panelForms.Paint += new System.Windows.Forms.PaintEventHandler(this.panelForms_Paint);
            // 
            // Timer1
            // 
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // btnFactory
            // 
            this.btnFactory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFactory.AutoSize = true;
            this.btnFactory.BackColor = System.Drawing.Color.White;
            this.btnFactory.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFactory.BackgroundImage")));
            this.btnFactory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFactory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFactory.FlatAppearance.BorderSize = 0;
            this.btnFactory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFactory.Font = new System.Drawing.Font("Ebrima", 8F);
            this.btnFactory.ForeColor = System.Drawing.Color.Gray;
            this.btnFactory.Location = new System.Drawing.Point(3, 3);
            this.btnFactory.Name = "btnFactory";
            this.btnFactory.Size = new System.Drawing.Size(76, 64);
            this.btnFactory.TabIndex = 602;
            this.btnFactory.TabStop = false;
            this.btnFactory.Text = "Empresa";
            this.btnFactory.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFactory.UseVisualStyleBackColor = false;
            this.btnFactory.Click += new System.EventHandler(this.btnFactory_Click);
            // 
            // btnUser
            // 
            this.btnUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUser.AutoSize = true;
            this.btnUser.BackColor = System.Drawing.Color.White;
            this.btnUser.BackgroundImage = global::AppINTROIT.Properties.Resources.users32;
            this.btnUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUser.FlatAppearance.BorderSize = 0;
            this.btnUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUser.Font = new System.Drawing.Font("Ebrima", 8F);
            this.btnUser.ForeColor = System.Drawing.Color.Gray;
            this.btnUser.Location = new System.Drawing.Point(85, 3);
            this.btnUser.Name = "btnUser";
            this.btnUser.Size = new System.Drawing.Size(76, 64);
            this.btnUser.TabIndex = 603;
            this.btnUser.TabStop = false;
            this.btnUser.Text = "Usuarios";
            this.btnUser.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnUser.UseVisualStyleBackColor = false;
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            // 
            // btnConcept
            // 
            this.btnConcept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConcept.AutoSize = true;
            this.btnConcept.BackColor = System.Drawing.Color.White;
            this.btnConcept.BackgroundImage = global::AppINTROIT.Properties.Resources.concept;
            this.btnConcept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnConcept.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConcept.FlatAppearance.BorderSize = 0;
            this.btnConcept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConcept.Font = new System.Drawing.Font("Ebrima", 8F);
            this.btnConcept.ForeColor = System.Drawing.Color.Gray;
            this.btnConcept.Location = new System.Drawing.Point(3, 73);
            this.btnConcept.Name = "btnConcept";
            this.btnConcept.Size = new System.Drawing.Size(76, 64);
            this.btnConcept.TabIndex = 604;
            this.btnConcept.TabStop = false;
            this.btnConcept.Text = "Conceptos";
            this.btnConcept.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConcept.UseVisualStyleBackColor = false;
            this.btnConcept.Click += new System.EventHandler(this.btnConcept_Click);
            // 
            // btnRemisiones
            // 
            this.btnRemisiones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemisiones.BackColor = System.Drawing.Color.White;
            this.btnRemisiones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRemisiones.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemisiones.FlatAppearance.BorderSize = 0;
            this.btnRemisiones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemisiones.Font = new System.Drawing.Font("Ebrima", 8F);
            this.btnRemisiones.ForeColor = System.Drawing.Color.Gray;
            this.btnRemisiones.Image = global::AppINTROIT.Properties.Resources.remision;
            this.btnRemisiones.Location = new System.Drawing.Point(85, 73);
            this.btnRemisiones.Name = "btnRemisiones";
            this.btnRemisiones.Size = new System.Drawing.Size(76, 64);
            this.btnRemisiones.TabIndex = 605;
            this.btnRemisiones.TabStop = false;
            this.btnRemisiones.Text = "Remisiones";
            this.btnRemisiones.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRemisiones.UseVisualStyleBackColor = false;
            this.btnRemisiones.Click += new System.EventHandler(this.btnRemisiones_Click);
            // 
            // btnIssued
            // 
            this.btnIssued.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIssued.BackColor = System.Drawing.Color.White;
            this.btnIssued.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnIssued.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIssued.FlatAppearance.BorderSize = 0;
            this.btnIssued.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIssued.Font = new System.Drawing.Font("Ebrima", 8F);
            this.btnIssued.ForeColor = System.Drawing.Color.Gray;
            this.btnIssued.Image = global::AppINTROIT.Properties.Resources.remisiones32;
            this.btnIssued.Location = new System.Drawing.Point(3, 143);
            this.btnIssued.Name = "btnIssued";
            this.btnIssued.Size = new System.Drawing.Size(76, 64);
            this.btnIssued.TabIndex = 606;
            this.btnIssued.TabStop = false;
            this.btnIssued.Text = "Reportes";
            this.btnIssued.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnIssued.UseVisualStyleBackColor = false;
            this.btnIssued.Click += new System.EventHandler(this.btnIssued_Click);
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(52)))), ((int)(((byte)(93)))));
            this.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Font = new System.Drawing.Font("Ebrima", 14F);
            this.btnMenu.ForeColor = System.Drawing.Color.White;
            this.btnMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu.Image")));
            this.btnMenu.Location = new System.Drawing.Point(0, 0);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(42, 39);
            this.btnMenu.TabIndex = 5;
            this.btnMenu.TabStop = false;
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(151)))), ((int)(((byte)(213)))));
            this.ClientSize = new System.Drawing.Size(824, 504);
            this.Controls.Add(this.splitMenu);
            this.Controls.Add(this.PanelBarraTitulo);
            this.Font = new System.Drawing.Font("Ebrima", 8.25F);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            this.PanelBarraTitulo.ResumeLayout(false);
            this.PanelBarraTitulo.PerformLayout();
            this.splitMenu.Panel1.ResumeLayout(false);
            this.splitMenu.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMenu)).EndInit();
            this.splitMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Panel PanelBarraTitulo;
        internal Label Label2;
        private SplitContainer splitMenu;
        internal Button btnMenu;
        internal Button btnCerrar;
        private GroupBox groupBox1;
        internal Button btnFactory;
        internal Label label1;
        internal Button btnUser;
        internal Button btnConcept;
        internal Button btnRemisiones;
        private FlowLayoutPanel flowLayoutPanel1;
        private Panel panelForms;
        internal Timer Timer1;
        internal Label lblLogin;
        internal Label lblHora;
        internal Button btnIssued;
    }
}