﻿using AppINTROIT.Entidades;
using AppINTROIT.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppINTROIT.Forms
{
    public partial class FormMain : Form
    {
        private static FormMain _DefaultInstance;
        public bool _activeSplitPanel1 = true;

        public FormMain()
        {
            InitializeComponent();

            //Ocultamos el spliter panel 1
            splitMenu.Panel1Collapsed = false;
            //lblLogin.Text = ActiveOperadores._Login;
        }

        private void openFormOnPanel<Miform>() where Miform : Form, new()
        {

            Form Formulario;
            Formulario = panelForms.Controls.OfType<Miform>().FirstOrDefault(); //Busca el formulario en la coleccion
                                                                                      //Si form no fue econtrado
            if (Formulario == null)
            {
                //Cerrar formulario anterior , exepto form principal y form login/ 
                //Si desea que permita abrir varios formulario en el panel, simplemente elimine el codigo
                //My.Application.OpenForms.Cast<Form>().Except({ this, Form1}).ToList().ForEach(void(form) form.Close()) ;
                //Application.OpenForms.Cast<Form>().Except({ this, Form1}).ToList
                //Form[] formsList = Application.OpenForms.Cast<Form>().Where(x => x.Name == "Form1").Where(x => x.Name == "FormPrincipal").ToArray();
                //foreach (Form openForm in formsList)
                //{
                //    openForm.Close();
                //}
                if (this.panelForms.Controls.Count > 0)
                    this.panelForms.Controls.RemoveAt(0);
                //-----------------------------------------------
                //Abrir formulario en el panel
                Formulario = new Miform();
                Formulario.TopLevel = false;
                Formulario.FormBorderStyle = FormBorderStyle.None;
                Formulario.Dock = DockStyle.Fill;
                panelForms.Controls.Add(Formulario);
                panelForms.Tag = Formulario;
                //Formulario.FormClosed += new System.EventHandler(this.closedForm);
                //Formulario.Click += System.EventHandler(this.closedForm);
                //Formulario.Close();
                //_activeSplitPanel1 = true;
                Formulario.BringToFront();
                Formulario.Show();
                //splitMenu.Panel1Collapsed = true;
            }
            else
            { //Si Form Existe, traerlo al frente
                Formulario.BringToFront();
            }
        }

        private void closedForm(object sender, FormClosedEventArgs e)
        {
            if ((Application.OpenForms["FormUserProfile"] == null))
            {
                Timer1.Stop(); //We stop the timer once the user finishes editing his profile And closes the form
                               //Detenemos el temporizador una vez que el usuario termine de editar su perfil y cierre el formulario
                               //More Codes
            }
        }

        public static FormMain DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormMain();

                return _DefaultInstance;
            }
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            btnIssued_Click(null, null);
        }



        private void btnLogout_Click(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Esta apunto de cerrar el sistema, se perdera los datos no guardados, desea continuar ?", "Cerrar Sesión", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.OK)
            {
                System.Environment.Exit(1);
            }
            
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (!_activeSplitPanel1)
            {
                _activeSplitPanel1 = true;
                splitMenu.Panel1Collapsed = true;
            }
            else
            {
                _activeSplitPanel1 = false;
                splitMenu.Panel1Collapsed = false;
            }
        }

        private void splitMenu_Panel2_MouseHover(object sender, EventArgs e)
        {
            _activeSplitPanel1 = true;
            splitMenu.Panel1Collapsed = true;
        }

        private void splitMenu_Panel1_Leave(object sender, EventArgs e)
        {
            // splitMenu.Panel1Collapsed = true;
        }

        private void splitMenu_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToString("hh:mm:ss");
            //lblFecha.Text = DateTime.Now.ToLongDateString();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            openFormOnPanel<FormUsers>();           
        }

        private void btnFactory_Click(object sender, EventArgs e)
        {
           openFormOnPanel<FormFactory>();
        }

        private void btnBox_Click(object sender, EventArgs e)
        {
           // openFormOnPanel<FormBox>();
        }

        private void button5_Click(object sender, EventArgs e)
        {
           // openFormOnPanel<FormProduct>();
        }

        private void btnConcept_Click(object sender, EventArgs e)
        {
            openFormOnPanel<FormConcept>();
        }

        private void panelForms_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnRemisiones_Click(object sender, EventArgs e)
        {
            openFormOnPanel<FormReferral>();
        }

        private void btnIssued_Click(object sender, EventArgs e)
        {
            openFormOnPanel<FormIssued>();
        }
    }
}
