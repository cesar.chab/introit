﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppINTROIT.Configurations;
using AppINTROIT.Helpers;

namespace AppINTROIT
{
    public partial class FormConexion : Form
    {
        private static FormConexion _DefaultInstance;
        private EncryptAES AES = new EncryptAES();
        private System.Threading.Thread Procesar;
        delegate void DelMostrarProgreso(bool finalizar, string Texto);

        public static FormConexion DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormConexion();

                return _DefaultInstance;
            }
        }

        private void SubMostarProgreso(bool finalizar, string Texto = "")
        {
            if (this.InvokeRequired)
            {
                DelMostrarProgreso del = new DelMostrarProgreso(SubMostarProgreso);
                this.Invoke(del, new object[] { finalizar, Texto });
            }
            else if (finalizar)
            {
                Procesar.Abort();
                //this.Close();
            }
            else
                this.lblEstatus.Text = Texto;
        }

        public FormConexion()
        {
            InitializeComponent();
            pictureBox1.Location = new Point((splitContainer1.Panel1.Width / 2) - (pictureBox1.Width / 2),
              (splitContainer1.Panel1.Height / 2) - (pictureBox1.Height / 2));
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void FormConexion_Load(object sender, EventArgs e)
        {
            cboServer.Items.Add(Environment.MachineName);
            cboServer.SelectedIndex = 0;
            cboAutenticacion.SelectedIndex = 0;
            validPanelAuth();
        }

        private void cboAutenticacion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboAutenticacion_SelectedValueChanged(object sender, EventArgs e)
        {
            validPanelAuth();
        }

        private void validPanelAuth()
        {
            if (cboAutenticacion.SelectedItem.ToString() == "Windows Autenticación")
            {
                panelAuth.Visible = false;
                panelButtons.Location = new Point(28, 207);
            }
            else
            {
                panelAuth.Visible = true;
                panelButtons.Location = new Point(28, 320);
            }
        }

        private void getServer()
        {
            DataTable table = Splash.DTServer;
            //SqlDataSourceEnumerator.Instance.GetDataSources();

            foreach (DataRow server in table.Rows)
            {
                string servername = server[table.Columns["ServerName"]].ToString();

                // you can get that using the instanceName property 
                string instancename = server[table.Columns["InstanceName"]].ToString();

                //and version property tells you the version of sql server i.e 2000, 2005, 2008 r2 etc
                string sqlversion = server[table.Columns["Version"]].ToString();

                //to form the servername you can combine the server and instancenames as
                //string sqlserverfullname = String.Format("{0} {1}", servername, instancename);
                cboServer.Items.Add(instancename);
            }
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {

            panelButtons.Visible = false;
            PBLoading.Visible = true;
          
            if (Conexion.ValidaConexion(cboServer.Text, txtBase.Text, txtUsuario.Text, txtPassword.Text))
            {
                MessageBox.Show("Validación correcta, procede a guardar los parametros de la conexión", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                panelButtons.Visible = true;
                PBLoading.Visible = false;
            }
            else
            {
                MessageBox.Show("Error al validar, procede a capturar los parametros de la conexión", "Error al conectar" ,MessageBoxButtons.OK, MessageBoxIcon.Error);
                panelButtons.Visible = true;
                PBLoading.Visible = true;
            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var iniFile = new IniFile("Config.ini");
            iniFile.Write("Server", AES.Encrypt(cboServer.Text, DecryptAES.appPwdUnique, 256));
            iniFile.Write("Cnn", AES.Encrypt("Data Source=" + cboServer.Text + ";Initial Catalog=" + txtBase.Text + ";Integrated Security=True", DecryptAES.appPwdUnique, 256));
            iniFile.Write("Serial", AES.Encrypt(Helper.serialDD(), DecryptAES.appPwdUnique, 256));
            iniFile.Write("Sofware", AES.Encrypt("AppINTROIT", DecryptAES.appPwdUnique, 256));

            String Directory = System.Windows.Forms.Application.StartupPath;
            string file = Directory + "\\Config.ini";

            if (File.Exists(file))
            {
                if (Helper.existsCompany())
                {
                    MessageBox.Show("El archivo de configuración ha sido guardado correctamente.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                } else
                {
                    DialogResult result = MessageBox.Show("El archivo de configuración ha sido guardado correctamente, desea configurar los datos de la empresa ?", "Datos empresa", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    this.Hide();
                    if (result == DialogResult.OK)
                    {
                        RegisterCompany.DefaultInstance.ShowDialog();
                    } else
                    {
                        Login.DefaultInstance.ShowDialog();
                    }
                    
                }            
            } else
            {
                MessageBox.Show("Error al crear archivo de configuración, intenta nuevamente", "Error guardar configuración", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
