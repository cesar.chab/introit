﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using static AppINTROIT.Helpers.Helper;

namespace AppINTROIT.Helpers
{
    public class Conexion
    {
        private string _SQLServidor;
        private string _SQLBd;
        private string _SQLUser;
        private string _SQLPassword;
        private string _Cnn;

        private string _MsjError;
        private bool _TransManual;
        private int _TimeOutCnn = -1;
        private bool blnoCnnAbierta;
        private bool blnTransAbierta = false;
        private SqlConnection oCnn;
        private SqlTransaction Trans;
        public Collection<SQLpar> Parametros = new Collection<SQLpar>();
        public Collection<string> Comandos = new Collection<string>();

        public string SQLServidor { get { return _SQLServidor; } set { _SQLServidor = value; } }
        public string SQLBd { get { return _SQLBd; } set { _SQLBd = value; } }
        public string SQLUser { get { return _SQLUser; } set { _SQLUser = value; } }
        public string SQLPassword { get { return _SQLPassword; } set { _SQLPassword = value; } }
        public string MsjError { get { return _MsjError; } set { _MsjError = value; } }
        public bool TransManual { get { return _TransManual; } set { _TransManual = value; } }
        public int TimeOutCnn { get { return _TimeOutCnn; } set { _TimeOutCnn = value; } }
        public string Cnn { get { return _Cnn; } set { _Cnn = value; } }

        public Conexion()
        {
            //Load config ini
            EncryptAES AES = new EncryptAES();
            String Directory = System.Windows.Forms.Application.StartupPath;
            string file = Directory + "\\Config.ini";

            if (File.Exists(file))
            {
                IniFile iniFile = new IniFile("Config.ini");
                Cnn = AES.Decrypt(iniFile.Read("Cnn"), DecryptAES.appPwdUnique, 256);
            }
        }

        public bool Conecta(bool usingPassword = false)
        {
            try
            {
               
                if (usingPassword)
                {
                    oCnn = new SqlConnection();
                    String Tiempo = TimeOutCnn > -1 ? ";Connection Timeout=" + TimeOutCnn : "";
                    oCnn.ConnectionString = "Server=" + SQLServidor + "; User ID=" + SQLUser + "; Password=" + SQLPassword + Tiempo;
                }
                else
                {

                    //oCnn.ConnectionString = Cnn;
                    if (Cnn == null)
                    {
                        Cnn = "server=" + SQLServidor + ";database=" + SQLBd + "; integrated security=yes";
                    }
                    
                    oCnn = new SqlConnection(Cnn);
                }

                oCnn.Open();
                blnoCnnAbierta = true;
                MsjError = "";
                return true;

            }
            catch (SqlException ex)
            {
                blnoCnnAbierta = false;
                MsjError = "Error de Conexión [" + ex.Message + "]";
                return false;
            }
        }

        /// <summary>
        /// Metodo para llenar una DataTable pasandole la consulta el DataTable donde almacenara los resultados
        /// </summary>
        /// <param name="SQLQuery">Consulta SLQ</param>
        /// <param name="DT">DataTabla a almacenar resultados</param>
        /// <returns>Retorna el resulta de una consulta SQL a un DataTable</returns>
        public bool CargaTabla(String SQLQuery, ref DataTable DT)
        {
            try
            {
                if (blnoCnnAbierta == false)
                {
                    if (Conecta())
                    {
                        SqlDataAdapter da = new SqlDataAdapter(SQLQuery, oCnn);
                        if (TimeOutCnn > -1) da.SelectCommand.CommandTimeout = TimeOutCnn;
                        da.SelectCommand.Transaction = Trans;
                        DT.Rows.Clear();
                        da.Fill(DT);
                        if (!TransManual)
                        {
                            oCnn.Close();
                            blnoCnnAbierta = false;
                        }
                        MsjError = "";
                        return true;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    SqlDataAdapter da = new SqlDataAdapter(SQLQuery, oCnn);
                    da.SelectCommand.Transaction = Trans;
                    DT.Rows.Clear();
                    da.Fill(DT);
                    if (!TransManual)
                    {
                        oCnn.Close();
                        blnoCnnAbierta = false;
                    }
                    MsjError = "";
                    return true;
                }
            }
            catch (SqlException ex)
            {
                MsjError = "Error al Cargar Tabla [" + ex.Message + "]";
                return false;
            }
        }

        /// <summary>
        /// Cargamos una tabla medienta una string query y almacenarlo a un Dataset Indicando el nombre de la tabla
        /// contenida dentro del DataSet
        /// </summary>
        /// <param name="SQLQuery">Sentencia SQL</param>
        /// <param name="DS">Parametro de tipo DataSet</param>
        /// <param name="NomTabla">Nombre de la tabla dentro del DataSet</param>
        /// <returns>Devuelve el resultado de una sentencia SQL a un DataSet</returns>
        public bool CargaTabla(String SQLQuery, ref DataSet DS, String NomTabla)
        {
            try
            {

                if (blnoCnnAbierta == false)
                {
                    if (Conecta())
                    {
                        SqlDataAdapter DA = new SqlDataAdapter(SQLQuery, oCnn);
                        if (TimeOutCnn > -1) DA.SelectCommand.CommandTimeout = TimeOutCnn;
                        DA.SelectCommand.Transaction = Trans;
                        DA.Fill(DS, NomTabla);
                        if (!TransManual)
                        {
                            oCnn.Close();
                            blnoCnnAbierta = false;
                        }
                        MsjError = "";
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    SqlDataAdapter DA = new SqlDataAdapter(SQLQuery, oCnn);
                    DA.SelectCommand.Transaction = Trans;
                    DA.Fill(DS, NomTabla);
                    if (!TransManual)
                    {
                        oCnn.Close();
                        blnoCnnAbierta = false;
                    }
                    MsjError = "";
                    return true;
                }

            }
            catch (SqlException e)
            {
                MsjError = "Error al Cargar DataSet [" + e.Message + "]";
                return false;

            }
        }

        /// <summary>
        /// Metodo para obtener una lista de servidores disponibles 
        /// </summary>
        /// <param name="DT">Contenedor Resultado</param>
        /// <returns>Retorna el resultado de los servidores en linea a un DataTable</returns>
        public static string ServidoresEnLinea(ref DataTable DT)
        {
            try
            {
                System.Data.Sql.SqlDataSourceEnumerator Servidores = System.Data.Sql.SqlDataSourceEnumerator.Instance;
                DT = Servidores.GetDataSources();
                return "";
            }
            catch (Exception e)
            {

                return "Error al Localizar Servidores en linea [" + e.Message + "]";
            }
        }

        /// <summary>
        /// Validamos la conexion al servidor
        /// </summary>
        /// <param name="StrServidor">Nombre del servidor</param>
        /// <param name="StrBaseDatos">Nombre de la Base de Datos</param>
        /// <param name="StrUsuario">Nombre del Usuario de la Base de Datos</param>
        /// <param name="StrPwd">Contraseña de la Base de Datos</param>
        /// <returns>Devuelve True si la conexion es exitosa en caso contrario devuelve False</returns>
        public static bool ValidaConexion(string StrServidor, string StrBaseDatos, string StrUsuario, string StrPwd)
        {
            Conexion ClsDatos = new Conexion
            {
                SQLServidor = StrServidor,
                SQLBd = StrBaseDatos,
                SQLUser = StrUsuario,
                SQLPassword = StrPwd
            };
            if (ClsDatos.Conecta())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Function to validate the connection to the database without credentials
        /// </summary>
        /// <returns>Returns true or false depending on the state of the connection</returns>
        public static bool ValidaConexion()
        {
            Conexion conexion = new Conexion();
            return conexion.Conecta();
        }

        /// <summary>
        /// Metodo para iniciar una transaccion Manual
        /// </summary>
        /// <returns>Retorna un balor boleano que indica si se inicio o no la transaccion</returns>
        public bool BeginTrans()
        {
            if (TransManual)
            {
                if (blnoCnnAbierta == false)
                {
                    if (Conecta() == false)
                    {
                        return false;
                    }
                }
                try
                {
                    Trans = oCnn.BeginTransaction();
                    blnTransAbierta = true;
                    MsjError = "";
                    return true;
                }
                catch (SqlException e)
                {
                    MsjError = "Error al iniciar Transaccion[" + e.Message + "]";
                    blnTransAbierta = false;
                    return false;
                }


            }
            else
            {
                MsjError = "No Puede iniciar este modo la PropiedadTransManual es False";
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CommitTrans()
        {
            if (TransManual)
            {
                if (blnTransAbierta == false)
                {
                    MsjError = "No existe Begintrans";
                    return false;
                }
                if (blnoCnnAbierta)
                {
                    try
                    {
                        Trans.Commit();
                        Comandos.Clear();
                        Parametros.Clear();
                        blnTransAbierta = false;
                        MsjError = "";
                        return true;
                    }
                    catch (SqlException e)
                    {
                        MsjError = "Error al iniciar Transaccion [" + e.Message + "]";
                        return false;
                    }
                }
                else
                {
                    MsjError = "No se localiza Conexion Abierta";
                    return false;
                }
            }
            else
            {
                MsjError = "No puede iniciar este Metodo la propiedad TransManual es False";
                return false;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool RollBackTrans()
        {
            if (TransManual)
            {
                if (blnTransAbierta == false)
                {
                    MsjError = "No existe Begintrans";
                    return false;
                }
                if (blnoCnnAbierta)
                {
                    try
                    {
                        Trans.Rollback();
                        MsjError = "";
                        return true;
                    }
                    catch (SqlException e)
                    {
                        MsjError = "Error RollBackTrans nos e realizo Correctamente [" + e.Message + "]";
                        return false;
                    }
                }
                else
                {
                    MsjError = "No se localiza Conexion Abierta";
                    return false;
                }
            }
            else
            {
                MsjError = "No puede iniciar este Metodo la propiedad TransManual es False";
                return false;
            }
        }

        public void CargaComando(string StrSQL, Collection<SQLpar> ColeccionParametros = null)
        {
            Comandos.Add(StrSQL);

            if (ColeccionParametros != null)
            {
                foreach (SQLpar P in ColeccionParametros)
                {
                    Parametros.Add(P);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Execute()
        {
            string QueryEnCurso = "";
            if (blnoCnnAbierta == false)
            {
                if (Conecta() == false)
                {
                    return false;
                }
            }

            if (TransManual == true && blnTransAbierta == false)
            {
                MsjError = "No se puede ejecutar el metodo Execute sin iniciar una transaccion";
                return false;
            }
            if (TransManual == false) Trans = oCnn.BeginTransaction();
            try
            {
                int i = 0;
                for (i = 0; i < Comandos.Count; ++i)
                {

                    SqlCommand Comando = new SqlCommand
                    {
                        CommandText = Comandos[i]
                    };
                    QueryEnCurso = Comando.CommandText;
                    Comando.Connection = oCnn;
                    Comando.Transaction = Trans;
                    if (Parametros.Count > 0)
                    {
                        foreach (SQLpar Par in Parametros)
                        {
                            if (Par.Indice == i)
                            {
                                switch (Par.DataType)
                                {
                                    case SqlDbType.Image:
                                    case SqlDbType.VarBinary:
                                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                                        ms = (System.IO.MemoryStream)Par.obj;
                                        Comando.Parameters.Add(Par.StrNombreParametro, Par.DataType).Value = ms.GetBuffer();
                                        break;
                                    default:
                                        Comando.Parameters.Add(Par.StrNombreParametro, Par.DataType).Value = Par.obj;
                                        break;
                                }
                            }
                        }
                    }
                    Comando.ExecuteNonQuery();
                }
                if (TransManual == false)
                {
                    Trans.Commit();
                }

                Comandos.Clear();
                Parametros.Clear();
                return true;
            }
            catch (SqlException e)
            {
                if (TransManual == false)
                {
                    Trans.Rollback();
                }
                Comandos.Clear();
                Parametros.Clear();
                MsjError = "Error al Ejecutar Instruccion SQL[" + e.Message + "]\n\r" + QueryEnCurso;
                return false;
            }
        }
    }
}
