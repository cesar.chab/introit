﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AppINTROIT.Helpers
{
    public static class Cifrado
    {
        public static TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
        public static MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

        public static string Encriptar(string texto)
        {
            string _cifrado = "";

            if (!String.IsNullOrEmpty(texto.Trim()))
            {
                des.Key = hashmd5.ComputeHash((new UnicodeEncoding()).GetBytes(DecryptAES.appPwdUnique));
                des.Mode = CipherMode.ECB;
                ICryptoTransform encrypt = des.CreateEncryptor();
                byte[] buff = UnicodeEncoding.ASCII.GetBytes(texto);
                _cifrado = Convert.ToBase64String(encrypt.TransformFinalBlock(buff, 0, buff.Length));
            }

            return _cifrado;
        }

        public static string Desencriptar(string texto)
        {
            string _cifrado = "";
            if (!String.IsNullOrEmpty(texto.Trim()))
            {
                des.Key = hashmd5.ComputeHash((new UnicodeEncoding()).GetBytes(DecryptAES.appPwdUnique));
                des.Mode = CipherMode.ECB;
                ICryptoTransform desencrypta = des.CreateDecryptor();
                byte[] buff = Convert.FromBase64String(texto);
                _cifrado = UnicodeEncoding.ASCII.GetString(desencrypta.TransformFinalBlock(buff, 0, buff.Length));
            }

            return _cifrado;
        }
    }
}
