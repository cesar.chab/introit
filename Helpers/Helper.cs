﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Management;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;

namespace AppINTROIT.Helpers
{
    public class Helper
    {
        private const string Base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

        //El DES utiliza 8 bytes, el TripleDES utiliza 24 para el KEY. Y tambien maneja un Indice de Vector de 8 bytes los dos
        byte[] key = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
        byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        public Helper()
        {

        }

        /// <summary>
        /// Get Physical driver
        /// </summary>
        /// <returns></returns>
        public static string serialDD()
        {
            ManagementObject serialDD = new  ManagementObject(@"Win32_PhysicalMedia='\\.\PHYSICALDRIVE0'");
            return serialDD.Properties["SerialNumber"].ToString().Trim();
        }

        public struct SQLpar
        {
            public int Indice;
            public string StrNombreParametro;
            public System.Data.SqlDbType DataType;
            public object obj;
        }


        private static int mimedecode(char strIn)
        {
            if (strIn == '\0')
            {
                return -1;
            }
            else
            {
                return Base64Chars.IndexOf(strIn);

            }
        }
        private static char mimeencode(int intIn)
        {
            if (intIn >= 0)
            {
                return Base64Chars[intIn];

            }
            else
            {
                return '\0';
            }
        }
        public static string Encripta(string StrEncripta)
        {

            int c1, c2, c3, n;
            int w1, w2, w3, w4;
            string strOut = "";
            for (n = 1; n <= StrEncripta.Length; n = n + 3)
            {

                c1 = (int)StrEncripta[n - 1];
                c2 = (int)(StrEncripta.Length <= n ? '\n' : StrEncripta[n] + (char)0);
                c3 = (int)(StrEncripta.Length <= n + 1 ? '\n' : StrEncripta[n + 1] + (char)0);
                w1 = (int)(c1 / 4);
                w2 = (int)((c1 & 3) * 16 + (int)(c2 / 16));
                if (StrEncripta.Length >= n + 1)
                {
                    w3 = (c2 & 15) * 4 + (int)(c3 / 64);
                }
                else
                {
                    w3 = -1;
                }
                if (StrEncripta.Length >= n + 2)
                {
                    w4 = c3 & 63;
                }
                else
                {
                    w4 = -1;
                }
                strOut = strOut + mimeencode(w1) + mimeencode(w2) + mimeencode(w3) + mimeencode(w4);
            }
            return strOut;
        }

        public static string DeEncripta(string StrDeEncripta)
        {
            int n;
            int w1, w2, w3, w4;
            string strOut = "";
            for (n = 1; n <= StrDeEncripta.Length; n = n + 4)
            {
                w1 = mimedecode(StrDeEncripta[n - 1]);
                w2 = mimedecode(StrDeEncripta.Length <= n ? '\n' : StrDeEncripta[n]);
                w3 = mimedecode(StrDeEncripta.Length <= n + 1 ? '\n' : StrDeEncripta[n + 1]);
                w4 = mimedecode(StrDeEncripta.Length <= n + 2 ? '\n' : StrDeEncripta[n + 2]);
                if (w2 >= 0)
                {
                    strOut = strOut + (char)((w1 * 4 + (int)(w2 / 16)) & 255);
                }
                if (w3 >= 0)
                {
                    strOut = strOut + (char)((w2 * 16 + (int)(w3 / 4)) & 255);
                }
                if (w4 >= 0)
                {
                    strOut = strOut + (char)((w3 * 64 + (int)w4) & 255);
                }
            }
            return strOut;
        }

        public string EncryptToBase64String(string stringToEncrypt)
        {
            try
            {
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                // convierte la cadena de entrada en un arreglo byte
                byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                //ahora se encripta el array
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                // Se devuelve el arreglo de bytes como cadena
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string DecryptFromBase64String(string stringToDecrypt)
        {
            byte[] inputByteArray = new byte[(stringToDecrypt.Length + 1)];
            try
            {
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        

        public static bool Formato(System.Windows.Forms.TextBox TxtObj, string Format, char Tecla)
        {
            bool Retorno;
            Retorno = false;
            string Caracteres = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz+-_@#%&,<>.: /=";
            if (Tecla == 13 || Tecla == 8)
            {
                Retorno = true;
            }
            else
            {
                switch (Format.ToUpper())
                {
                    case "ENTERO":
                    case "INT":
                    case "INT16":
                    case "INT32":
                    case "SYSTEM.INT32":
                    case "SYSTEM.INT16":
                    case "SYSTEM.INT64":
                        {
                            Regex pattern = new Regex("[^0-9]");
                            if (!pattern.IsMatch(System.Convert.ToString(System.Convert.ToChar(Tecla))))
                            {
                                Retorno = true;

                            }
                            else
                            {
                                Retorno = false;

                            }

                        }
                        break;
                    case "DOUBLE":
                    case "DECIMAL":
                    case "FLOAT":
                    case "SYSTEM.DOUBLE":
                    case "SYSTEM.FLOAT":
                    case "SYSTEM.DECIMAL":
                        {
                            Regex pattern = new Regex("[^0-9.]");
                            if (!pattern.IsMatch(System.Convert.ToString(System.Convert.ToChar(Tecla))))
                            {
                                if (Tecla.ToString() == "." && TxtObj.Text.IndexOf(".") >= 0)
                                {
                                    Retorno = false;
                                }
                                else
                                {
                                    Retorno = true;
                                }
                            }
                            else
                            {
                                Retorno = false;

                            }
                            break;
                        }
                    case "CHAR":
                    case "SYSTEM.STRING":
                        {
                            if (Caracteres.IndexOf(Tecla) >= 0)
                            {
                                Retorno = true;
                            }
                            else
                            {
                                Retorno = false;
                            }

                        }
                        break;

                }

            }
            return Retorno;
        }



        /// <summary>
        /// Function to valid email
        /// </summary>
        /// <param name="sMail"></param>
        /// <returns></returns>
        public static bool validar_Mail(string sMail)
        {
            return Regex.IsMatch(sMail, @"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$");
        }

        public void NumerosyDecimal(System.Windows.Forms.TextBox CajaTexto, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

       
        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] byteArrayIn = ms.ToArray();
            return ms.ToArray();
        }


        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
            ms.Write(byteArrayIn, 0, byteArrayIn.Length);
            return Image.FromStream(ms, true);

        }

        /// <summary>
        /// Function to validate if there is registered company data
        /// </summary>
        /// <returns>returns true or false depending on the result of the validation</returns>
        public static bool existsCompany(string table = "Empresa")
        {
            var ClsDatos = new Conexion();
            var DT = new DataTable();
            bool _return = false;
            string _query = "SELECT COUNT(*) FROM dbo." + table + "";
            if (ClsDatos.CargaTabla(_query, ref DT))
            {
                if (DT.Rows.Count > 0)
                {
                    _return = ((int)DT.Rows[0][0] > 0) ? true : false;
                }
            }
            return _return;
        }

        public static string GetHTMLFromAddress(string Address)
        {
            System.Text.ASCIIEncoding ASCII = new System.Text.ASCIIEncoding();
            System.Net.WebClient netWeb = new System.Net.WebClient();
            string lsWeb = null;
            byte[] laWeb = null;

            try
            {
                laWeb = netWeb.DownloadData(Address);
                lsWeb = ASCII.GetString(laWeb);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString() + ex.ToString());
            }

            return lsWeb;
        }


    }

    /// <summary>
    /// Funciones con equivalencias de VB a C#
    /// </summary>
    static public class Equivalencias
    {
        /// <summary>
        /// Comprueba si el parámetro es de tipo DateTime
        /// </summary>
        /// <typeparam name="T">
        /// El tipo de datos a comprobar
        /// </typeparam>
        /// <param name="fecha">
        /// El valor a comprobar si es una fecha válida
        /// </param>
        /// <returns>
        /// Un valor verdadero o falso según el parámetro sea una fecha
        /// </returns>
        static public bool IsDate<T>(T fecha) //where T: IConvertible
        {
            // Si no queremos aceptar como válido un valor nulo
            // ya que Convert.ToDateTime devolverá DateTime.MinValue
            // cuando el parámetro es null
            if (fecha == null)
            {
                return false;
            }
#if DEBUG
            Console.WriteLine("    El tipo de fecha es: {0}", fecha.GetType().Name);
#endif

            // Aportación de Harvey Triana con fecha 11/Ago/2007
            // en el grupo de noticias microsoft.public.es.csharp
            if (fecha is DateTime)
            {
                return true;
            }

            try
            {
                DateTime fecha1 = Convert.ToDateTime(fecha);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Comprueba si el parámetro es un número
        /// </summary>
        /// <typeparam name="T">
        /// El tipo de datos a comprobar
        /// </typeparam>
        /// <param name="numero">
        /// El valor a comprobar si es un número
        /// </param>
        /// <returns>
        /// Un valor verdadero o falso según el parámetro sea un número,
        /// en realidad se comprueba si es convertible a Double.
        /// </returns>
        static public bool IsNumeric<T>(T numero) //where T : IConvertible
        {
            // Si es un valor nulo, devolver directamente true
            if (numero == null)
            {
                return true;
            }
#if DEBUG
            Console.WriteLine("    El tipo de numero es: {0}", numero.GetType().Name);
#endif
            // Salvo excepciones, todos los números se pueden convertir a Double
            try
            {
                double num = Convert.ToDouble(numero);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// El valor numérico ASCII del carácter indicado
        /// </summary>
        /// <param name="valor">
        /// El valor de tipo Char a convertir en número ASCII
        /// </param>
        /// <returns>
        /// El valor entero del carácter indicado
        /// </returns>
        static public int Asc(char valor)
        {
            return (int)valor;
        }

        /// <summary>
        /// El valor numérico ASCII de la cadena indicada
        /// (se comprobará el primer carácter de la cadena)
        /// </summary>
        /// <param name="valor">
        /// El valor de tipo String a convertir a número ASCII
        /// </param>
        /// <returns>
        /// El valor entero del primer carácter de la cadena indicada
        /// </returns>
        static public int Asc(string valor)
        {
            if (string.IsNullOrEmpty(valor) || valor.Length < 1)
                return 0;

            return (int)valor[0];
        }

        /// <summary>
        /// Convierte un número en un Char
        /// </summary>
        /// <param name="valor">
        /// El valor numérico a convertir
        /// </param>
        /// <returns>
        /// El valor de tipo Char del número indicado
        /// </returns>
        static public char ChrW(int valor)
        {
            return (char)valor;
        }

        /// <summary>
        /// Convierte un número en un Char
        /// </summary>
        /// <param name="valor">
        /// El valor numérico a convertir
        /// </param>
        /// <returns>
        /// El valor de tipo Char del número indicado
        /// </returns>
        static public char Chr(int valor)
        {
            return (char)valor;
        }

        //---------------------------------------------------------------------
        // Funciones de manipulación de cadenas
        //---------------------------------------------------------------------

        /// <summary>
        /// Devuelve la longitud de <paramref name="cadena"/>
        /// (si es un valor nulo devolverá cero)
        /// </summary>
        /// <param name="cadena">
        /// La cadena de la que se quiere averiguar el tamaño
        /// </param>
        /// <returns>
        /// La cantidad de caracteres de la cadena o cero si es nulo
        /// </returns>
        static public int Len(string cadena)
        {
            if (string.IsNullOrEmpty(cadena))
                return 0;
            return cadena.Length;
        }

        /// <summary>
        /// Devuelve la posición de la cadena2 en cadena1
        /// empezando por el carácter en la posición start
        /// (por compatibilidad con Visual Basic,
        /// la posición del primer carácter se considera que es 1)
        /// </summary>
        /// <param name="start">
        /// La posición del carácter (en base 1)
        /// a partir del que se comprobará
        /// </param>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <param name="binaria">
        /// True si se diferencian las mayúsculas de minúsculas
        /// </param>
        /// <returns>
        /// Devuelve la posición de cadena2 en cadena1.
        /// Si la cadena no existe devuelve cero
        /// </returns>
        static public int InStr(int start, string cadena1, string cadena2, bool binaria)
        {
            if (string.IsNullOrEmpty(cadena1) ||
                string.IsNullOrEmpty(cadena2) ||
                start > cadena1.Length)
            {
                return 0;
            }
            // Restar uno a la posición y usar cero si es menor de cero
            if (--start < 0)
                start = 0;

            if (binaria)
            {
                return cadena1.IndexOf(cadena2, start) + 1;
            }
            else
            {
                return cadena1.IndexOf(cadena2, start,
                    StringComparison.CurrentCultureIgnoreCase) + 1;
            }
        }

        /// <summary>
        /// Devuelve la posición de la cadena2 en cadena1
        /// empezando por el carácter en la posición start
        /// (por compatibilidad con Visual Basic,
        /// la posición del primer carácter se considera que es 1)
        /// </summary>
        /// <param name="start">
        /// La posición del carácter (en base 1)
        /// a partir del que se comprobará
        /// </param>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <returns>
        /// Devuelve la posición con índice 1 de cadena2 dentro de cadena1,
        /// comprobando desde el carácter de la posición start.
        /// Si no está, devolverá cero.
        /// </returns>
        static public int InStr(int start, string cadena1, string cadena2)
        {
            return InStr(start, cadena1, cadena2, true);
        }

        /// <summary>
        /// Devuelve la posición de la cadena2 en cadena1
        /// empezando por el primer carácter
        /// (por compatibilidad con Visual Basic,
        /// la posición del primer carácter se considera que es 1)
        /// </summary>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <param name="binaria">
        /// True si se diferencian las mayúsculas de minúsculas
        /// </param>
        /// <returns>
        /// Devuelve la posición de cadena2 en cadena1.
        /// Si la cadena no existe devuelve cero
        /// </returns>
        static public int InStr(string cadena1, string cadena2, bool binaria)
        {
            return InStr(1, cadena1, cadena2, binaria);
        }

        /// <summary>
        /// Devuelve la posición de la cadena2 en cadena1
        /// (por compatibilidad con Visual Basic,
        /// la posición del primer carácter se considera que es 1)
        /// </summary>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <returns>
        /// Devuelve la posición con índice 1 de cadena2 dentro de cadena1.
        /// Si no está, devolverá cero.
        /// </returns>
        static public int InStr(string cadena1, string cadena2)
        {
            return InStr(1, cadena1, cadena2);
        }

        /// <summary>
        /// Devuelve la posición de <paramref name="cadena2"/>
        /// dentro de <paramref name="cadena1"/>
        /// empezando por el caracter de la posición <paramref name="start"/>
        /// (se comprueba desde el final de la cadena)
        /// </summary>
        /// <param name="start">
        /// La posición del carácter (en base 1)
        /// a partir del que se comprobará
        /// </param>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <param name="binaria">
        /// True si se diferencian las mayúsculas de minúsculas
        /// </param>
        /// <returns>
        /// Devuelve la posición de cadena2 en cadena1.
        /// Si la cadena no existe devuelve cero
        /// </returns>
        static public int InStrRev(int start, string cadena1, string cadena2, bool binaria)
        {
            if (string.IsNullOrEmpty(cadena1) ||
                string.IsNullOrEmpty(cadena2) ||
                start > cadena1.Length)
            {
                return 0;
            }
            // Restar uno a la posición y usar cero si es menor de cero
            if (--start < 0)
                start = 0;
            // Si se hace comparación binaria o no
            if (binaria)
            {
                return cadena1.LastIndexOf(cadena2, start) + 1;
            }
            else
            {
                return cadena1.LastIndexOf(cadena2, start,
                    StringComparison.CurrentCultureIgnoreCase) + 1;
            }
        }

        /// <summary>
        /// Devuelve la posición de <paramref name="cadena2"/>
        /// dentro de <paramref name="cadena1"/>
        /// empezando por el caracter de la posición <paramref name="start"/>
        /// (se comprueba desde el final de la cadena)
        /// </summary>
        /// <param name="start">
        /// La posición del carácter (en base 1)
        /// a partir del que se comprobará
        /// </param>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <returns>
        /// Devuelve la posición de cadena2 en cadena1.
        /// Si la cadena no existe devuelve cero
        /// </returns>
        static public int InStrRev(int start, string cadena1, string cadena2)
        {
            return InStrRev(start, cadena1, cadena2, true);
        }

        /// <summary>
        /// Devuelve la posición de <paramref name="cadena2"/>
        /// dentro de <paramref name="cadena1"/>
        /// empezando por el último carácter
        /// (se comprueba desde el final de la cadena)
        /// </summary>
        /// <param name="cadena1">
        /// La cadena en la que se hará la búsqueda
        /// </param>
        /// <param name="cadena2">
        /// La cadena que queremos comprobar si está en la primera
        /// </param>
        /// <returns>
        /// Devuelve la posición de cadena2 en cadena1.
        /// Si la cadena no existe devuelve cero
        /// </returns>
        static public int InStrRev(string cadena1, string cadena2)
        {
            return InStrRev(1, cadena1, cadena2);
        }

        /// <summary>
        /// Devuelve los primeros caracteres de la cadena
        /// </summary>
        /// <param name="cadena">
        /// La cadena de la que se obtendrán los caracteres
        /// </param>
        /// <param name="length">
        /// El total de caracteres a devolver
        /// </param>
        /// <returns>
        /// Devuelve una cadena con los primeros length caracteres
        /// </returns>
        static public string Left(string cadena, int length)
        {
            if (string.IsNullOrEmpty(cadena) || length < 1)
                return "";

            // Comprobar que no nos pasamos
            if (length > cadena.Length)
            {
                length = cadena.Length;
            }
            return cadena.Substring(0, length);
        }

        /// <summary>
        /// Devuelve los últimos <paramref name="length"/> caracteres de la cadena
        /// </summary>
        /// <param name="cadena">
        /// La cadena de la que se obtendrán los caracteres
        /// </param>
        /// <param name="length">
        /// El total de caracteres a devolver
        /// </param>
        /// <returns>
        /// Devuelve una cadena con los últimos length caracteres
        /// </returns>
        static public string Right(string cadena, int length)
        {
            if (string.IsNullOrEmpty(cadena) || length < 1)
                return "";

            int n = cadena.Length;
            // Comprobar que no nos pasamos
            if (length > n)
            {
                length = n;
            }
            return cadena.Substring(n - length, length);
        }

        /// <summary>
        /// Devuelve <paramref name="length"/> caracteres de la cadena indicada 
        /// empezando por el carácter de la posición <paramref name="start"/>
        /// </summary>
        /// <param name="cadena">
        /// La cadena de la que se obtendrán los caracteres
        /// </param>
        /// <param name="start">
        /// Posición de inicio (en base 1) desde donde se tomará la cadena
        /// </param>
        /// <param name="length">
        /// Número de caracteres que se devolverán
        /// </param>
        /// <returns></returns>
        static public string Mid(string cadena, int start, int length)
        {
            if (string.IsNullOrEmpty(cadena) || length < 1 || start > cadena.Length)
                return "";
            // Comprobar que no nos pasamos
            if (length > cadena.Length - start)
            {
                length = cadena.Length - start + 1;
            }
            return cadena.Substring(start - 1, length);
        }

        /// <summary>
        /// Devuelve los caracteres desde la posición <paramref name="start"/>
        /// </summary>
        /// <param name="cadena">
        /// La cadena de la que se obtendrán los caracteres
        /// </param>
        /// <param name="start">
        /// Posición desde la que se devolverá la cadena
        /// </param>
        /// <returns></returns>
        static public string Mid(string cadena, int start)
        {
            if (string.IsNullOrEmpty(cadena))
                return "";

            return Mid(cadena, start, cadena.Length);
        }

        /// <summary>
        /// Sustituye en <paramref name="cadena"/> 
        /// los caracteres indicados desde <paramref name="start"/>
        /// con una longitud de <paramref name="length"/> y pone
        /// el contenido de <paramref name="cadena2"/>
        /// </summary>
        /// <param name="cadena">
        /// La cadena a la que se asignarán los caracteres
        /// </param>
        /// <param name="cadena2">
        /// La cadena a poner en cadena1
        /// </param>
        /// <param name="start">
        /// Posición desde la que se devolverá la cadena
        /// </param>
        /// <param name="length"></param>
        static public void Mid(ref string cadena, string cadena2, int start, int length)
        {
            if (string.IsNullOrEmpty(cadena))
            {
                throw new ArgumentNullException("cadena",
                        "La cadena de destino no puede tener un valor nulo.");
            }
            int n = cadena.Length;
            if (start >= n)
            {
                throw new ArgumentOutOfRangeException("start",
                        "La posición de inicio debe estar dentro de la cadena original.");
            }
            cadena = Left(Left(cadena, start - 1) +
                     Left(cadena2, length) + Mid(cadena, start + length), n);
        }

        /// <summary>
        /// Sustituye en <paramref name="cadena"/> 
        /// los caracteres indicados desde <paramref name="start"/>
        /// con una longitud de <paramref name="length"/> y pone
        /// el contenido de <paramref name="cadena2"/>
        /// </summary>
        /// <param name="cadena">
        /// La cadena a la que se asignarán los caracteres
        /// </param>
        /// <param name="start">
        /// Posición desde la que se devolverá la cadena
        /// </param>
        /// <param name="length">
        /// El número de caracteres que se sustituirán
        /// </param>
        /// <param name="cadena2">
        /// La cadena a poner en cadena1
        /// </param>
        static public void Mid(ref string cadena, int start, int length, string cadena2)
        {
            Mid(ref cadena, cadena2, start, length);
        }

        /// <summary>
        /// Sustituye en <paramref name="cadena"/> 
        /// los caracteres indicados desde <paramref name="start"/>
        /// y pone el contenido de <paramref name="cadena2"/>
        /// </summary>
        /// <param name="cadena">
        /// La cadena a la que se asignarán los caracteres
        /// </param>
        /// <param name="cadena2">
        /// La cadena a poner en cadena1
        /// </param>
        /// <param name="start">
        /// Posición desde la que se devolverá la cadena
        /// </param>
        static public void Mid(ref string cadena, string cadena2, int start)
        {
            Mid(ref cadena, start, cadena2);
        }

        /// <summary>
        /// Sustituye en <paramref name="cadena"/> 
        /// los caracteres indicados desde <paramref name="start"/>
        /// y pone el contenido de <paramref name="cadena2"/>
        /// </summary>
        /// <param name="cadena">
        /// La cadena a la que se asignarán los caracteres
        /// </param>
        /// <param name="start">
        /// Posición desde la que se devolverá la cadena
        /// </param>
        /// <param name="cadena2">
        /// La cadena a poner en cadena1
        /// </param>
        static public void Mid(ref string cadena, int start, string cadena2)
        {
            Mid(ref cadena, cadena2, start, cadena2.Length);
        }
    }
}
