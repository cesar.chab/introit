﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppINTROIT
{
    public partial class FormPrincipal : Form
    {
        private static FormPrincipal _DefaultInstance;
        public bool _activeSplitPanel1 = true;

        public static FormPrincipal DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormPrincipal();

                return _DefaultInstance;
            }
        }

        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
