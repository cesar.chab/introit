﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppINTROIT.Entidades
{
    public static class ActiveLicence
    {
        public static int _Id { get; set; }
        public static string _Descripcion { get; set; }
        public static string _FI { get; set; }
        public static string _FF { get; set; }
        public static string _FA { get; set; }
        public static string _Es_Cve_Estado { get; set; }
        public static string _SerialPC { get; set; }
    }
}
