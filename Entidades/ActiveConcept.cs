﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppINTROIT.Entidades
{
    public static class ActiveConcept
    {
        public static int _Id { get; set; }
        public static string _Descripcion { get; set; }
        public static decimal _PrecioGasolina { get; set; }
        public static decimal _PrecioDiesel { get; set; }
        public static string _Es_Cve_Estado { get; set; }
        public static DateTime _Fecha_Alta { get; set; }
        public static string _Oper_Alta { get; set; }
        public static DateTime _Fecha_Ult_Modif { get; set; }
        public static string _Oper_Ult_Modif { get; set; }
        public static DateTime _Fecha_Baja { get; set; }
        public static string _Oper_Baja { get; set; }
        public static int _Action { get; set; }
        public static string _Message { get; set; }

             
    }
}
