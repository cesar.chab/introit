﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppINTROIT.Entidades
{
    public static class ActiveReferral
    {
        public static string _Rm_Folio { get; set; }
        public static decimal _Rm_Total { get; set; }
        public static string _Rm_Comentario { get; set; }
        public static DateTime _Rm_Fecha { get; set; }
        public static string _Cl_Cve_Cliente { get; set; }
        public static string _Cl_Nombre { get; set; }
        public static DateTime _Fecha_Inicio { get; set; }
        public static DateTime _Fecha_Final { get; set; }
        public static string _Estado { get; set; }
        public static string _Message { get; set; }
    }
}
