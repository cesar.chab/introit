﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AppINTROIT.Entidades
{
    public static class ActiveCompany
    {
        public static string _Em_Cve_Empresa { get; set; }
        public static string _Em_Nombre { get; set; }
        public static string _Em_Calle { get; set; }
        public static string _Em_Cruzamiento { get; set; }
        public static string _Em_Numext { get; set; }
        public static string _Em_Numint { get; set; }
        public static string _Em_Colonia { get; set; }
        public static string _Em_Localidad { get; set; }
        public static string _Em_Municipio { get; set; }
        public static string _Em_Ciudad { get; set; }
        public static string _Em_Estado { get; set; }
        public static string _Em_Pais { get; set; }
        public static string _Em_CodPost { get; set; }
        public static string _Em_Telefono { get; set; }
        public static string _Em_RFC { get; set; }
        public static string _Em_CURP { get; set; }
        public static string _Em_Email { get; set; }
        public static string _Em_Regimen_Fiscal { get; set; }
        public static string _Em_Cve_Regimen_Fiscal { get; set; }
        public static Image _Em_Logo { get; set; }
        public static string _Em_Maneja_Impuesto { get; set; }
        public static string _Em_Impuesto { get; set; }
        public static decimal _Em_Impuesto_porcentaje { get; set; }
        public static string _Em_Moneda { get; set; }
        public static string _Modo_Busqueda { get; set; }
        public static string _Ruta_Backup { get; set; }
        public static DateTime _Ultimo_backup { get; set; }
        public static int _Frecuencia_backup { get; set; }
        public static string _Estado_backup { get; set; }
        public static string _Redondeo_Total { get; set; }
        public static DateTime _Fecha_Alta { get; set; }
        public static string _Oper_Alta { get; set; }
        public static DateTime _Fecha_Ult_Modif { get; set; }
        public static string _Oper_Ult_Modif { get; set; }
        public static DateTime _Fecha_Baja { get; set; }
        public static string _Oper_Baja { get; set; }
        public static string _Es_Cve_Estado { get; set; }
    }
}
