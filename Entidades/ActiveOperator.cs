﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AppINTROIT.Entidades
{
    public static class ActiveOperator
    {
        public static int _Id { get; set; }
        public static string _Nombre { get; set; }
        public static string _Login { get; set; }
        public static string _Password { get; set; }
        public static Image _Icono { get; set; }
        public static string Correo { get; set; }
        public static string _Es_Cve_Estado { get; set; }

        public static string _Rol { get; set; }

        public static string _Message { get; set; }

        public static int _Action { get; set; }
    }
}
