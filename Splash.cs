﻿using AppINTROIT.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppINTROIT.Configurations;

namespace AppINTROIT
{
    public partial class Splash : Form
    {
        //Private aes As New AES()
        private EncryptAES encrypt = new EncryptAES();
        private int _contador = 0;
        public static DataTable DTServer = new DataTable();
        private System.Threading.Thread Procesar;
        delegate void DelMostrarProgreso(bool finalizar, string Texto, int instancia = 0);
        private string _EstatusInfo = "";

        public string EstatusInfo
        {
            set
            {
                _EstatusInfo = value;
                CambiarEstatusTexto();
            }
            get
            {
                return _EstatusInfo;
            }
        }

        public void CambiarEstatusTexto()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(this.CambiarEstatusTexto));
                    return;
                }
                this.lblIniciando.Text = _EstatusInfo;

            }
            catch (Exception) { }
        }
        public Splash()
        {
            InitializeComponent();
        }

        private void SubMostarProgreso(bool finalizar, string Texto = "", int instancia = 0)
        {
            if (this.InvokeRequired)
            {
                DelMostrarProgreso del = new DelMostrarProgreso(SubMostarProgreso);
                this.Invoke(del, new object[] { finalizar, Texto, instancia });
            }
            else if (finalizar)
            {
                Procesar.Abort();
                switch (instancia)
                {
                    case 1:
                        {
                            this.Hide();
                            Login.DefaultInstance.ShowDialog();
                            break;
                        }
                    case 2:
                        {
                            this.Hide();
                            RegisterCompany.DefaultInstance.ShowDialog();
                            break;
                        }
                    default:
                        {
                            this.Hide();
                            FormConexion.DefaultInstance.ShowDialog();
                            break;
                        }
                }

            }
            else
            {
                this.lblIniciando.Text = Texto;
            }

        }

        private void Splash_Load(object sender, EventArgs e)
        {
            Procesar = new System.Threading.Thread(validating_data);
            Procesar.IsBackground = true;
            Procesar.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        private void validating_data()
        {
            SubMostarProgreso(false, "Iniciando...");
            System.Threading.Thread.Sleep(2000);
            SubMostarProgreso(false, "Validando configuración...");
            System.Threading.Thread.Sleep(2000);

            if (!existsConfig())
            {
                SubMostarProgreso(false, "Configurar sevidor");
                System.Threading.Thread.Sleep(2000);
                SubMostarProgreso(true);
                this.Hide();
            }
            else
            {
                SubMostarProgreso(false, "Validando Conexión...");
                System.Threading.Thread.Sleep(2000);

                if (Conexion.ValidaConexion())
                {
                    //Verificando datos empresa
                    if (Helper.existsCompany())
                    {
                        SubMostarProgreso(false, "Abriendo autenticación...");
                        System.Threading.Thread.Sleep(2000);
                        SubMostarProgreso(true, "", 1);
                        this.Hide();
                    }
                    else
                    {
                        SubMostarProgreso(false, "Configurar datos de la empresa...");
                        System.Threading.Thread.Sleep(2000);
                        SubMostarProgreso(true, "", 2);
                        this.Hide();
                    }

                } else
                {
                    SubMostarProgreso(true, "", 3);
                    this.Hide();
                }
            }
        }

        /// <summary>
        /// Function to validate if there is registered company data
        /// </summary>
        /// <returns>returns true or false depending on the result of the validation</returns>
        //private bool existsCompany()
        //{
        //    var ClsDatos = new Conexion();
        //    var DT = new DataTable();
        //    bool _return = false;
        //    string _query = "SELECT COUNT(*) FROM Empresa";
        //    if (ClsDatos.CargaTabla(_query, ref DT))
        //    {
        //        if (DT.Rows.Count > 0)
        //        {
        //            _return = ((int)DT.Rows[0][0] > 0) ? true : false;
        //        }
        //    }
        //    return _return;
        //}


        /// <summary>
        /// Validamos si existe el archivo config
        /// </summary>
        /// <returns></returns>
        private bool existsConfig()
        {
            String Directory = System.Windows.Forms.Application.StartupPath;
            string file = Directory + "\\Config.ini";

            if (File.Exists(file))
            {
                return true;
            }
            return false;
        }

        private void getIniFile()
        {
            try
            {
                // Or specify a specific name in the current dir
                var MyIni = new IniFile("Config.ini");
                if (!MyIni.KeyExists("DefaultVolume", "Audio"))
                {
                    MyIni.Write("DefaultVolume", encrypt.Encrypt("100", DecryptAES.appPwdUnique, int.Parse("256")), "Audio");
                }
            }
            catch (Exception ex)
            {
                new Exception(ex.ToString());
            }

        }

        private void Splash_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                System.Environment.Exit(1);
            }
        }
    }
}
